using System;
using ClayTablet.CT.Utility;
using System.Reflection;
using System.Diagnostics;

namespace RunJobs
{
    public class RunJobs
    {
        public static void Main(string[] args)
        {
            String provider_Name = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.Name"].ToString();
            int sleepTime = int.Parse(System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.SleepTime"].ToString());

            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string assembly_version = fvi.ProductVersion;
            string assembly_name = fvi.ProductName;

            CLogger.WriteLog(ELogLevel.INFO, "Load Provider: [" + provider_Name + "(" + assembly_name + "-" + assembly_version + ") ] ...");
            ClayTablet.CT.Net.WordBee.ReceiverJob receiverJober = new ClayTablet.CT.Net.WordBee.ReceiverJob();
            ClayTablet.CT.Net.WordBee.ProviderSenderJob senderJober = new ClayTablet.CT.Net.WordBee.ProviderSenderJob();
             
            while (true)
            {
                CLogger.WriteLog(ELogLevel.INFO, "Start to polling [" + assembly_name + " - " + assembly_version + "]");
                try
                {
                    CLogger.WriteLog(ELogLevel.INFO, "Running Provider Receiver");
                    receiverJober.Process(); 
                }
                catch (Exception e)
                {
                    CLogger.WriteLog(ELogLevel.ERROR, "Receiver error", e);
                }

                try
                {

                    CLogger.WriteLog(ELogLevel.INFO, "Running Provider TMS Checking");
                    senderJober.Process();
                }
                catch (Exception e)
                {
                    CLogger.WriteLog(ELogLevel.ERROR, "Sender error", e);
                }
                CLogger.WriteLog(ELogLevel.INFO, "Polling done, next polling in " + (sleepTime / 1000) + " seconds");
                System.Threading.Thread.Sleep(sleepTime);
            } 
        } 
    }
}
