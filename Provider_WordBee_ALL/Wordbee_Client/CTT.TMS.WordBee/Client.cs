﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml; 
using System.IO;
using System.Reflection;
using System.Net;
using ClayTablet.CT.Utility;
using CTT.TMS.GeneralTMS;
using Newtonsoft.Json.Linq;
using System.Web;
using System.Xml.Linq;
using CTT.TMS.Wordbee; 

namespace CTT.TMS.WordBee
{


    public class Client
    {
        private string m_apiUrl; 
        private string m_accountID = String.Empty;
        private string m_loginPassword = String.Empty;
        private string m_token = null; 
        private Boolean isConnected = false;
        private string m_error = String.Empty;
        private string m_clientcompanyId = String.Empty;
        private string m_clientcompanyName = String.Empty; 
        private List<TMSLanguageMapping> m_TMSLanguageMappings = new List<TMSLanguageMapping>();
        private DateTime lastAccess;

        private string lastestAPIUrl = "";
        private string lastestAPIXML = "";
        private string lastestAPIMethod = "";
        private bool isGetResponse = false;

        private Boolean logAPIDetails = false;

        private class MyWebClient : WebClient
        {
            public MyWebClient()
            {

            }

            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest w = base.GetWebRequest(uri);
                w.Timeout = 60 * 60 * 1000;
                return w;
            }
        }

        public Client(string apiUrl, string accountID, string password)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["LogWoodBeeApiDetails"] == null)
            {
                logAPIDetails = false;
            }
            else
            {
                string logAPIDetails_setting = System.Configuration.ConfigurationManager.AppSettings["LogWoodBeeApiDetails"].ToString().ToLower();

                if (logAPIDetails_setting.Equals("true"))
                    logAPIDetails = true;
                else
                    logAPIDetails = false;
            }

            if (apiUrl.EndsWith("/"))
            {
                this.m_apiUrl = apiUrl.Substring(0, apiUrl.Length - 1);
            }
            else
                this.m_apiUrl = apiUrl;
 
            this.m_accountID = accountID;
            this.m_loginPassword = password;

            this.lastAccess = DateTime.Now.AddMinutes(-100);

            loadLanguageMappings();   
        }

        public Client(string apiUrl, string accountID, string password, string clientCompanyName)
        {

            if (System.Configuration.ConfigurationManager.AppSettings["LogWoodBeeApiDetails"] == null)
            {
                logAPIDetails = false;
            }
            else
            {
                string logAPIDetails_setting = System.Configuration.ConfigurationManager.AppSettings["LogWoodBeeApiDetails"].ToString().ToLower();

                if (logAPIDetails_setting.Equals("true"))
                    logAPIDetails = true;
                else
                    logAPIDetails = false;
            }

            if (apiUrl.EndsWith("/"))
            {
                this.m_apiUrl = apiUrl.Substring(0, apiUrl.Length - 1);
            }
            else
                this.m_apiUrl = apiUrl;

            this.m_accountID = accountID;
            this.m_loginPassword = password;

            this.lastAccess = DateTime.Now.AddMinutes(-100); 

            this.m_clientcompanyName = clientCompanyName;  
            loadLanguageMappings();


        }

        public bool Connect()
        {

            if (this.isConnected && lastAccess.AddMinutes(5) >= DateTime.Now)
            {
                lastAccess = DateTime.Now;
                return true;
            }
            else
            {

                if (this.isConnected)
                {
                    try
                    {
                        this.Close();
                    }
                    catch (Exception) { }
                } 

                bool isSuccess = false;
                try
                {
                    lastestAPIMethod = "Connect()";
                    lastestAPIUrl = "";
                    lastestAPIXML = "";
                    isGetResponse = false;

                    using (WebClient clientc = new MyWebClient())
                    {
                        string url = string.Format("{0}/api/connect?pwd={1}&account={2}", this.m_apiUrl, this.m_loginPassword, this.m_accountID);
                        lastestAPIUrl = url; 

                        string result = clientc.DownloadString(url); 
                        lastestAPIXML = result;
                        isGetResponse = true;

                        System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);
                        this.m_token = e.Value;
                        this.isConnected = true;
                        isSuccess = true;
                        lastAccess = DateTime.Now;
                    }
                }
                catch (Exception e)
                {
                    m_token = null;
                    this.isConnected = false;
                    m_error = getErrorFromMessage(e.Message);
                    isSuccess = false;
                    CLogger.WriteLog(ELogLevel.DEBUG, "Fail to connector to Wordbee", e);
                }
                return isSuccess;
            }
        } 

        public void Close()
        {
            if (isConnected && m_token != null)
            {
                lastestAPIMethod = "Close()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;

                using (WebClient clientc = new MyWebClient())
                {
                    string url = string.Format("{0}/api/disconnect?token={1}", this.m_apiUrl, this.m_token);
                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url);
                    isGetResponse = true;
                    
                    lastestAPIXML = result; 

                }
                isConnected = false;
                m_token = null;
            }
        } 

        public string Error
        {
            get { return this.m_error; }
            set { this.m_error = value; }
        }

        public bool IsConnected
        {
            get { return this.isConnected; }
        }

        public string ClientCompanyName
        {
            get { return this.m_clientcompanyName; }
        }


        public String getCompanyId()
        {
            if (!String.IsNullOrEmpty(m_clientcompanyId))
                return m_clientcompanyId;
            else if (String.IsNullOrEmpty(m_clientcompanyName))
                return getMasterCompanyId();
            else
            {

                string defaultComponayID = getDefaultCompanyId();
                if (String.IsNullOrEmpty(defaultComponayID))
                    return getMasterCompanyId();
                else
                    return defaultComponayID;
            }
        }


        private String getMasterCompanyId()
        {

            String m_companyId = String.Empty;
            Connect(); 

            try
            {
                lastestAPIMethod = "getMasterCompanyId()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;

                using (WebClient clientc = new MyWebClient())
                {
                    string url = string.Format("{0}/master/company?token={1}", this.m_apiUrl, this.m_token);
                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url); 
                    lastestAPIXML = result;
                    isGetResponse = true;

                    /*
                     <ApiCompany xmlns="http://schemas.datacontract.org/2004/07/Wordbee.Database" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                        <CompanyId>1</CompanyId>
                        <IsClient>true</IsClient>
                        <IsSupplier>true</IsSupplier>
                        <Profile_>0</Profile_>
                        <Comments i:nil="true"/>
                        <IsActive>true</IsActive>
                        <Name>Nikon Test platform</Name>
                        <Code/><CountryCode>USA</CountryCode>
                        <AddressLine1/>
                        <AddressLine2 i:nil="true"/>
                        <AddressLine3 i:nil="true"/>
                        <City>-</City><Zip>-</Zip>
                        <ContactName>Andy Jones</ContactName>
                        <ContactEmail>wordbeesupport@nikon.com</ContactEmail>
                        <ContactPhone i:nil="true"/>
                        <ContactPhoneMobile i:nil="true"/>
                        <ContactFax i:nil="true"/>
                        <Expr1 i:nil="true"/>
                        <InvIsSame>true</InvIsSame>
                        <InvName>Nikon Test platform</InvName>
                        <InvCountryCode>USA</InvCountryCode><InvAddressLine1/>
                        <InvAddressLine2 i:nil="true"/><InvAddressLine3 i:nil="true"/>
                        <InvCity>-</InvCity><InvZip>-</InvZip><InvComments i:nil="true"/>
                        <TimeZone>Mountain Standard Time</TimeZone><VAT i:nil="true"/>
                        <DefaultPriceListId i:nil="true"/><PaymentTerms i:nil="true"/>
                        <CreditorNo/><DebtorNo/><VATNo/><InvoicingCompanyId i:nil="true"/>
                        <InvoiceTemplateId i:nil="true"/><DisableClientTerms>false</DisableClientTerms>
                        <ContactLocale>en</ContactLocale><ContactTitle/><AccountingCode/>
                        <InvDoPrint>true</InvDoPrint><InvDoEmail>true</InvDoEmail>
                        <InvDoAggregate>true</InvDoAggregate><InvEmail/><InvPublished_>1</InvPublished_>
                        <InvTemplateVariant>0</InvTemplateVariant>
                        <ConsolidationDocumentSetId>4</ConsolidationDocumentSetId>
                        <CustomStr1 i:nil="true"/><CustomStr2 i:nil="true"/>
                        <CustomStr3 i:nil="true"/><CustomStr4 i:nil="true"/>
                        <CustomStr5 i:nil="true"/><CustomStr6 i:nil="true"/>
                        <CustomStr7 i:nil="true"/><CustomStr8 i:nil="true"/>
                        <CustomStr9 i:nil="true"/><CustomStr10 i:nil="true"/>
                        <CustomStr11 i:nil="true"/><CustomStr12 i:nil="true"/>
                        <CustomStr13 i:nil="true"/><CustomStr14 i:nil="true"/>
                        <CustomStr15 i:nil="true"/><CustomStr16 i:nil="true"/>
                        <CustomStr17 i:nil="true"/><CustomStr18 i:nil="true"/>
                        <CustomStr19 i:nil="true"/><CustomStr20 i:nil="true"/>
                      </ApiCompany>
                     */
                    System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);
                    
                   XElement companyIdNode = e.Element(XName.Get("CompanyId", "http://schemas.datacontract.org/2004/07/Wordbee.Database"));
                        if (companyIdNode == null)
                            m_companyId = "";
                        else
                            m_companyId = companyIdNode.Value;
                    
                     
                }
            }
            catch (Exception e)
            { 
                m_error = getErrorFromMessage(e.Message);
                CLogger.WriteLog(ELogLevel.DEBUG, "Failure in getMasterCompanyId", e);
            }

            return m_companyId;
        }

        private String getDefaultCompanyId()
        {

            String m_companyId = String.Empty;

            Connect();

            try
            {
                lastestAPIMethod = "getDefaultCompanyId()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;

                using (WebClient clientc = new MyWebClient())
                {
                    string url = string.Format("{0}/companies?token={1}", this.m_apiUrl, this.m_token);
                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url);
                    lastestAPIXML = result;
                    isGetResponse = true;

                    /*
                     <ArrayOfApiCompany xmlns="http://schemas.datacontract.org/2004/07/Wordbee.Database" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                        <ApiCompany>
                        <OnlineUrl>
                        http://www.wordbee-translator.com/a/ansell/Company/CompanyView.aspx?x=sS%2bQ2diXwBwqcXiXVd9lgSzEg9O1fDX%2fI6AHP0dM1mI%3d
                        </OnlineUrl>
                        <CompanyId>1</CompanyId>
                        ...
                        <Name>Ansell</Name>
                        <...
                        </ApiCompany>
                        <ApiCompany>
                        <OnlineUrl>
                        http://www.wordbee-translator.com/a/ansell/Company/CompanyView.aspx?x=xhEQWWjsmpSvLW527gvMDGG1NovAKZRcHld8%2fpuJEE4%3d
                        </OnlineUrl>
                        <CompanyId>2</CompanyId>
                        ...
                        <Name>Acme</Name>
                        </ApiCompany>
                        <ApiCompany>
                        <OnlineUrl>
                        http://www.wordbee-translator.com/a/ansell/Company/CompanyView.aspx?x=nTcuiWN5S%2f8T2HQQ4vTFYXHz4jv2sEj4PF0BJ2m%2fj34%3d
                        </OnlineUrl>
                        <CompanyId>3</CompanyId>
                        ...
                        <Name>Google Freelance</Name>
                        ...
                        </ApiCompany>
                        <ApiCompany>
                        <OnlineUrl>
                        http://www.wordbee-translator.com/a/ansell/Company/CompanyView.aspx?x=cKYbWhSz9dFY%2bWTs8APhynTZQthSvgylUu5hrsmTHao%3d
                        </OnlineUrl>
                        <CompanyId>4</CompanyId>
                        ...
                        <Name>Ansell Healthcare</Name>
                        <Code>ANS</Code>
                        ...
                        </ApiCompany>
                        <ApiCompany>
                        ...
                        </ApiCompany>
                        </ArrayOfApiCompany>
                     */ 


                    System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);

                    XName apiCompany = XName.Get("ApiCompany", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName companyId = XName.Get("CompanyId", "http://schemas.datacontract.org/2004/07/Wordbee.Database"); 
                    XName name = XName.Get("Name", "http://schemas.datacontract.org/2004/07/Wordbee.Database");

                    foreach (XElement apiCompanyElement in e.Elements(apiCompany))
                    {
                        XElement companyIdElement = apiCompanyElement.Element(companyId);
                        XElement nameElement = apiCompanyElement.Element(name);

                        if (m_clientcompanyName.Equals(nameElement.Value))
                        {
                            m_companyId = companyIdElement.Value;
                            break;
                        }
                        
                    } 

                }
            }
            catch (Exception e)
            {
                m_error = getErrorFromMessage(e.Message);
                CLogger.WriteLog(ELogLevel.ERROR, "Faied to search compant ID for ClientCompanyName: [" + m_clientcompanyName + "].", e);
            }

            return m_companyId;
        }

        public int FindProjectId(string projectReference)
        {
            int foundProjectID = -1;
            Connect();

            lastestAPIMethod = "FindProjectId()";
            lastestAPIUrl = "";
            lastestAPIXML = "";
            isGetResponse = false;

            using (WebClient clientc = new MyWebClient())
            {
                string url = string.Format("{0}/projects?token={1}&reference={2}&count=1", this.m_apiUrl, this.m_token, HttpUtility.UrlEncode(projectReference));
                lastestAPIUrl = url;

                string result = clientc.DownloadString(url); 
                lastestAPIXML = result;
                isGetResponse = true; 

                System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);

                XElement projectNode = e.Element(XName.Get("ApiProject", "http://schemas.datacontract.org/2004/07/Wordbee.Database"));
                if (projectNode == null)
                    foundProjectID = -1;
                else
                {
                    XElement projectIdNode = projectNode.Element(XName.Get("ProjectId", "http://schemas.datacontract.org/2004/07/Wordbee.Database"));
                    if (projectIdNode == null) 
                        foundProjectID = -1;
                    else
                        foundProjectID = int.Parse(projectIdNode.Value);
                }
            }

            return foundProjectID;

        }

        public bool isProjectExist(int ProjectId)
        {
            return isProjectExist(Convert.ToString(ProjectId));
        }

        public bool isProjectExist(String ProjectId)
        {
             
            String projectName = FindProjectName(ProjectId);
            if (String.IsNullOrEmpty(projectName))
                return false;
            else
                return true;
        }

        public String FindProjectName(int ProjectId)
        {
             
            return FindProjectName(Convert.ToString(ProjectId) );
        }

        public String FindProjectName(string ProjectId)
        {
            String foundProjectName = null;
            Connect();

            try
            {
                lastestAPIMethod = "FindProjectName()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;

                using (WebClient clientc = new MyWebClient())
                {
                    string url = string.Format("{0}/projects/{1}?token={2}",
                        this.m_apiUrl, ProjectId, this.m_token);
                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url); 
                     lastestAPIXML = result;
                     isGetResponse = true;

                    if (String.IsNullOrEmpty(result))
                        foundProjectName = null;
                    else
                    {
                        try
                        {
                            System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);
                            XElement projectNameNode = e.Element(XName.Get("Reference", "http://schemas.datacontract.org/2004/07/Wordbee.Database"));
                            if (projectNameNode == null)
                                foundProjectName = null;
                            else
                                foundProjectName = projectNameNode.Value;
                        }
                        catch (Exception ee)
                        {
                            CLogger.WriteLog(ELogLevel.ERROR, "Failed to find project details with ID:[" + ProjectId + "]. Error:" + ee.Message);
                            CLogger.WriteLog(ELogLevel.ERROR, "Request URL:\n" + url + "\n");
                            CLogger.WriteLog(ELogLevel.ERROR, "Response text:\n" + result + "\n");
                            CLogger.WriteLog(ELogLevel.DEBUG, "Error detail: ", ee);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Failed to find project details with ID:[" + ProjectId + "]. Error:" + e.Message);
                CLogger.WriteLog(ELogLevel.DEBUG, "Error detail: ", e);
            }
            return foundProjectName;
        }

        /*
         * If the project does not exist, create a new one: 
            (POST) projects?token={TOKENID}
            &projecttype=2
            &clientcompanyid={CLIENT COMPANY ID}
            &sourcelocale={SOURCELOCALE}
            &targetlocales={TARGETLOCALES}
            &deadline={DEADLINE} 
            &reference={ REFERENCE}
            &status={STATUS}
            &receiveddate={RECEIVEDDATE}
            &templateprojectid={PROJECT ID}

            For In Progress set STATUS to value 0.

            The client company id (Nikon) can be hardcoded, it will never change but it may be different in the test and the production platform. 
            Use API call (GET) master/company?token={TOKENID} which returns the platform “master” company id.

            To get the ID of the template project, use the “projects” method with the “reference” parameter set.

            The “receiveddate” is optional. If not specified it will be set to the current date/time. You might want to set the Sharepoint date here. Do not forget to convert to UTC and ISO format (see API doc).
        */
        /*
         * rerult:
           <int xmlns="http://schemas.microsoft.com/2003/10/Serialization/">2147483647</int>
         */ 
        public int CreateProject(string projectReference, string templateprojectid,
            string sourceCTLanguage, List<string> targetCTLanguages, String projecttype,
            string UtcIso8601Deadline, string comments, string instructions)
         {
            int createdProjectId = -1;
            Connect();

            string sourcelocale = this.getMapppingTMSLanguage(sourceCTLanguage);
            string targetlocales = null;
            foreach (string targerCTLng in targetCTLanguages)
            {
                if (targetlocales == null)
                    targetlocales = this.getMapppingTMSLanguage(targerCTLng);
                else
                    targetlocales += "," + this.getMapppingTMSLanguage(targerCTLng);
            }
            targetlocales += "," + sourcelocale;

            lastestAPIMethod = "CreateProject()";
            lastestAPIUrl = "";
            lastestAPIXML = "";
            isGetResponse = false;
            using (WebClient webClient = new WebClient())
            {
                string url = string.Format("{0}/projects", this.m_apiUrl);
                lastestAPIUrl = url;

                /* 
                System.Collections.Specialized.NameValueCollection formData = new System.Collections.Specialized.NameValueCollection();
                formData.Add("token", this.m_token); 
                if (String.IsNullOrEmpty(projecttype ))
                    formData.Add("projecttype", "1"); 
                else
                    formData.Add("projecttype", "1");
                   // formData.Add("projecttype", projecttype); 

                formData.Add("clientcompanyid", getCompanyId());
                formData.Add("sourcelocale", HttpUtility.UrlEncode(sourcelocale));
                formData.Add("targetlocales", HttpUtility.UrlEncode(targetlocales));
                formData.Add("reference", HttpUtility.UrlEncode(projectReference));
                formData.Add("status", "0");
                formData.Add("receiveddate", HttpUtility.UrlEncode(toUTCISO8601(DateTime.UtcNow)));
                formData.Add("templateprojectid", templateprojectid);
               


                if (!String.IsNullOrEmpty(UtcIso8601Deadline) ) 
                    formData.Add("deadline", HttpUtility.UrlEncode(UtcIso8601Deadline));

                if (!String.IsNullOrEmpty(comments)) 
                    formData.Add("comments", HttpUtility.UrlEncode(comments));

                if (!String.IsNullOrEmpty(instructions)) 
                   formData.Add("instructions", HttpUtility.UrlEncode(instructions));

                byte[] responseBytes = webClient.UploadValues(url, "POST", formData);
                string result = Encoding.UTF8.GetString(responseBytes);
                System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);
                createdProjectId = int.Parse(e.Value);
                */

                
                String urlData = "?token=" + this.m_token;
                if (String.IsNullOrEmpty(projecttype))
                    urlData += "&projecttype=" + "1";
                else
                    urlData += "&projecttype=" + projecttype;

                urlData += "&clientcompanyid=" + getCompanyId();
                //urlData += "&sourcelocale=" + HttpUtility.UrlEncode(sourcelocale);
                urlData += "&targetlocales=" + HttpUtility.UrlEncode(targetlocales);
                if (!String.IsNullOrEmpty(projectReference))
                     urlData += "&reference=" + HttpUtility.UrlEncode(projectReference);

                urlData += "&status=0";
                urlData += "&receiveddate=" + HttpUtility.UrlEncode(toUTCISO8601(DateTime.UtcNow));
                urlData += "&templateprojectid=" + templateprojectid;

                if (!String.IsNullOrEmpty(UtcIso8601Deadline))
                    urlData += "&deadline=" + HttpUtility.UrlEncode(UtcIso8601Deadline);

                if (!String.IsNullOrEmpty(comments))
                    urlData += "&comments=" + HttpUtility.UrlEncode(comments);

                if (!String.IsNullOrEmpty(instructions))
                    urlData += "&instructions=" + HttpUtility.UrlEncode(instructions);

               

                if (!String.IsNullOrEmpty(comments) )
                {
                     /*
                     customfields	A json array containing a list of custom field ids and values. See below for details.
                    Example:
                 [ { “Id”: “CustomStr1”, “Value”: “My field” }, { “Id”: “CustomStr5”, “Value”: “A-2233” } ]
                   */
                    String getCategory = "";
                    String getDepartment = "";

                    String getCategory_SearchKey = comments.ToLower() + "_category";
                    String getDepartment_SearchKey = comments.ToLower() + "_department";

                    if (System.Configuration.ConfigurationManager.AppSettings[getCategory_SearchKey] != null)
                    {
                        getCategory = System.Configuration.ConfigurationManager.AppSettings[getCategory_SearchKey].ToString();
                    }

                    if (System.Configuration.ConfigurationManager.AppSettings[getDepartment_SearchKey] != null)
                    {
                        getDepartment = System.Configuration.ConfigurationManager.AppSettings[getDepartment_SearchKey].ToString();
                    }

                    String jsonString = "[{\"Id\": \"CustomStr2\", \"Value\":\"" + getCategory + "\"},";
                    jsonString += "{\"Id\": \"CustomStr5\", \"Value\":\"" + getDepartment + "\"}]";

                    urlData += "&customfields=" + HttpUtility.UrlEncode(jsonString);

                    CLogger.WriteLog(ELogLevel.INFO, "new project will have \"customfields\" as: " + jsonString);

                }
                CLogger.WriteLog(ELogLevel.INFO, "full URL: " + url + urlData);
                byte[] responseBytes = webClient.UploadData(url + urlData, "POST", System.Text.Encoding.ASCII.GetBytes(""));
                string result = Encoding.UTF8.GetString(responseBytes); 
                
                lastestAPIXML = result;
                isGetResponse = true;

                /*
                 //create the constructor with post type and few data
                String fullURL = url + urlData;
                MyWebRequest myRequest = new MyWebRequest(fullURL, "POST", "test=OK");
                
                string result = myRequest.GetResponse();
                 */
                System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);
                createdProjectId = int.Parse(e.Value);

                CLogger.WriteLog(ELogLevel.INFO, "created new project with ID: " + createdProjectId);
            }

            return createdProjectId;
        }
        public int CreateProject(string projectReference, string templateprojectid,
            string sourceCTLanguage, List<string> targetCTLanguages, String projecttype,
            string UtcIso8601Deadline)
        {
            return CreateProject(projectReference, templateprojectid, sourceCTLanguage, targetCTLanguages, projecttype, UtcIso8601Deadline, null, null);
        } 

        public int CreateProject(string projectReference, string templateprojectid,
            string sourceCTLanguage, List<string> targetCTLanguages)
        {
            return CreateProject(projectReference, templateprojectid,  sourceCTLanguage, targetCTLanguages, null, null, null, null);
        }

         
        public int CreateProject(string projectReference, string templateprojectid,
            string sourceCTLanguage, List<string> targetCTLanguages, string comments)
        {
            return CreateProject(projectReference, templateprojectid, sourceCTLanguage, targetCTLanguages, null, null, comments, null);
        }

        public bool UploadFileToProject(int projectId, string filePath , string fileName, string sourceCTLanguage)
        {
            /*
                    string fname = "folder1/folder2/myfile.doc"; // The file name including folders to avoid having 1000s of files in the project library root (IMPORTANT!!!)
                    string sourceLocale = "en";
                    string targetLocales = "fr,de,es";
            */

            bool isSuccess = false;
            if (!File.Exists(filePath) )
            {
                m_error = "Can't find passed file: " + filePath;
                return false;
            }

            Connect();

            lastestAPIMethod = "UploadFileToProject()";
            lastestAPIUrl = "";
            lastestAPIXML = "";
            isGetResponse = false;
            try
            {
                 string sourcelocale = this.getMapppingTMSLanguage(sourceCTLanguage); 
                 string url = string.Format(
                           "{0}/projects/{1}/files/{2}/file?token={3}&name={4}", this.m_apiUrl, projectId, HttpUtility.UrlEncode(sourcelocale), this.m_token, HttpUtility.UrlEncode(fileName));

                  lastestAPIUrl = url;
                  lastestAPIXML = ""; 

                 byte[] contents = File.ReadAllBytes(filePath);     // Put your UTF-8 encoded XLIFF here. The byte sequence MUST BE UTF-8!

                 WebClient client = new MyWebClient();
                    client.UploadData(url, "POST", contents);

                    isGetResponse = true;
                    isSuccess = true;
            }
            catch (Exception e)
            {
                isSuccess = false;
                m_error = e.Message;
                CLogger.WriteLog(ELogLevel.DEBUG, 
                    string.Format("Error in UploadFileToProject({0},{1},{2},{3})", projectId, filePath, fileName, sourceCTLanguage), e);
            }

            return isSuccess;
       }

        public string getLatestAPIURL()
        {
             return lastestAPIUrl ; 

        }

        public string getLatestAPIXML()
        {
             return lastestAPIXML ; 

        } 
        
         public string getLatestAPIMethod()
        {
             return lastestAPIMethod ; 

        }

         public bool IsLatestResponseGot()
         {
             return isGetResponse;
         }

        /*This operation will prepare the file for a workflow. It extracts the texts, stores them into the database and kicks off workflows. The workflow specified in the project will be assigned (e.g. this may attach a translation job to the file).
          Use this method: 
            (PUT) projects/{PROJECTID}/documents/codytdocument2?token={1}
            &name={2}
            &version={3}
            &sourcelocale={4}
            &targetlocales={5}
            &parserdomain={6}
            &parserconfig={7}
            &workflow={8}
            &workflowstart={9}
            &docreference={10}
            &nosegmentation={11}

            See the API doc “Projects and Jobs” / “Codyt” for the parameters.

            2: The previously uploaded document (e.g. “folder1/folder2/myfile.doc”)
            3: Optional doc version (free text string). Need to check back with Nikon if they use this today
            6: See documentation for the available codes or use (GET) settings/parserdomains?token={TOKENID} for available options.
            7: The name of the text extraction settings (in Wordbee go to “Settings”, choose file format and view the list of settings.
            8: Set “ProjectDefault”
            9: Drop this parameter so that workflow starts immediately
            10: Drop. Not used by Nikon.
            11: Drop. Segmentation should always be done.

            Return values:

            If the method returns -1 you are done. Otherwise you must poll the system status in a loop until the operation is done (or fails). Check the API documentation.
            */
        public bool StartTranslatingProjectFile(int projectId, string fileName, string sourceCTLanguage, List<string> targetCTLanguages, 
                                              string parserdomain, string parserconfig, string workflow)
        {
            CLogger.WriteLog(ELogLevel.DEBUG, "StartTranslatingProjectFile fileName:" + fileName);

            bool isSuccess = false;
            Connect();

            string sourcelocale = this.getMapppingTMSLanguage(sourceCTLanguage);
            string targetlocales = null;
            foreach (string targerCTLng in targetCTLanguages)
            {
                if (targetlocales == null)
                    targetlocales = this.getMapppingTMSLanguage(targerCTLng);
                else
                    targetlocales += "," + this.getMapppingTMSLanguage(targerCTLng);
            }
            String final_workflow = "Default";
            if (!String.IsNullOrEmpty(workflow))
                final_workflow = workflow;

            lastestAPIMethod = "StartTranslatingProjectFile()";
            lastestAPIUrl = "";
            lastestAPIXML = "";
            isGetResponse = false;
            using (MyWebClient clientc = new MyWebClient())
            {
                if (String.IsNullOrEmpty(parserconfig))
                {
                    parserconfig = "Default Fallback";
                }

                string fullURL = string.Format("{0}/projects/{1}/documents/codytdocument2?token={2}&name={3}&sourcelocale={4}&targetlocales={5}&parserdomain={6}&parserconfig={7}&workflow={8}",
              this.m_apiUrl, projectId, this.m_token, HttpUtility.UrlEncode(fileName),
              HttpUtility.UrlEncode(sourcelocale), HttpUtility.UrlEncode(targetlocales),
              HttpUtility.UrlEncode(parserdomain), HttpUtility.UrlEncode(parserconfig),
              HttpUtility.UrlEncode(final_workflow));

                lastestAPIUrl = fullURL;
                
                byte[] responseBytes = clientc.UploadData(fullURL, "PUT", System.Text.Encoding.ASCII.GetBytes(""));
                string results = Encoding.UTF8.GetString(responseBytes);

                CLogger.WriteLog(ELogLevel.DEBUG, "API response:\n" + results);

                lastestAPIXML = results;
                isGetResponse = true;

                XElement elem = System.Xml.Linq.XElement.Parse(results);
                int status = int.Parse(elem.Value);
                CLogger.WriteLog(ELogLevel.DEBUG, "Response Status code: " + status);

                if (status == -1)
                {
                    // SUCCESS
                    isSuccess = true;
                }
                else
                {
                    //loop to get status
                    int operationId = status;
                    int totalWaitSeconds = 30 * 60 ;  //30 minutes;
                    int sleep_mseconds = 10; 
                    /*
                     Status	Status of operation:
                    /// <summary>
                    /// Result error during request execution
                    /// </summary>
                    Failed = 10,
                    /// <summary>
                    /// Request is waiting in queue for execution
                    /// </summary>
                    Waiting = 0,
                    /// <summary>
                    /// Request is executing mode
                    /// </summary>
                    Executing = 1,
                    /// <summary>
                    /// Execution of request is finished
                    /// </summary>
                    Finished = 2
                   */
                    if (status == 10)
                    {
                        isSuccess = false;
                    }
                    else if (status == 2)
                        isSuccess = true;
                    else
                    {
                        int checkStatusId = 1; //Executing = 1
                        int passedSeconds = 0;
                        while (checkStatusId != 10 && checkStatusId != 2 && (passedSeconds < totalWaitSeconds))
                        {
                            System.Threading.Thread.Sleep(sleep_mseconds * 1000);
                            passedSeconds += sleep_mseconds;
                            checkStatusId = getOperationStatus(operationId);
                        }

                        if (checkStatusId == 10)
                        {
                            isSuccess = false;
                        }
                        else if (checkStatusId == 2)
                            isSuccess = true;
                        else if ((checkStatusId == 0 || checkStatusId == 1) && passedSeconds >= totalWaitSeconds)
                        {
                            // time passed and still not done, treated as success
                            isSuccess = true;
                        }
                        else
                            isSuccess = false;
                    }
                }

            }

            return isSuccess;

        }


        // http://api.wordbee-translator.com:32490/help/operations/AsyncOperationStatus
        /*
         Gets the status of an asynchronous operation. Used to test if an operation has terminated. 
            Url: http://api.wordbee-translator.com:32490/asyncoperations/{OPERATIONID}/status?token={TOKENID}

            HTTP Method: GET
            Message direction 	Format 	Body
            Request 	N/A 	The Request body is empty.
            Response 	Xml 	Example
            Response 	Json 	Example

            The following is an example response Xml body:

            <int xmlns="http://schemas.microsoft.com/2003/10/Serialization/">2147483647</int> 
         * */
        public int getOperationStatus(int operationId)
        {
            CLogger.WriteLog(ELogLevel.DEBUG, "getOperationStatus API call");

            int statusId = 1 ;// Executing = 1
            Connect();

            try
            {
                lastestAPIMethod = "getOperationStatus()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;
                using (WebClient clientc = new MyWebClient())
                {
                    string url = string.Format("{0}/asyncoperations/{1}/status?token={2}", this.m_apiUrl, operationId, this.m_token);
                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url);
                    CLogger.WriteLog(ELogLevel.DEBUG, "getOperationStatus API response:\n" + result);

                    lastestAPIXML = result;
                    isGetResponse = true;

                    System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);
                    statusId = int.Parse(e.Value);
                    CLogger.WriteLog(ELogLevel.DEBUG, "getOperationStatus response Status code:\n" + statusId);
                }
            }
            catch (Exception e)
            {                 
                m_error = getErrorFromMessage(e.Message);
                CLogger.WriteLog(ELogLevel.ERROR, "getOperationStatus error:" + m_error);
                CLogger.WriteLog(ELogLevel.DEBUG, "Error details: ", e);
            }

            return statusId;
        }

        public List<TaskInfo> FindProjectDocInfo(string projectId)
        {
            List<TaskInfo> docInfos = null;
            Connect();

            try
            {
                lastestAPIMethod = "FindProjectDocInfo()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;
                using (WebClient clientc = new MyWebClient())
                {
                    string url = string.Format("{0}/projects/{1}/tasks?token={2}", this.m_apiUrl, projectId, this.m_token);
                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url); 
                    lastestAPIXML = result;
                    isGetResponse = true;

                    System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);

                    XName apiCodytTask = XName.Get("ApiCodytTask", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName taskId = XName.Get("RevisionSetTaskId", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName beeDocumentId = XName.Get("BeeDocumentId", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName documentName = XName.Get("DocumentName", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName taskCode = XName.Get("TaskCode", "http://schemas.datacontract.org/2004/07/Wordbee.Database");

                    docInfos = new List<TaskInfo>();
                    foreach (XElement apiCodytTaskElement in e.Elements(apiCodytTask))
                    {
                        XElement taskIdElement = apiCodytTaskElement.Element(taskId);
                        XElement beeDocumentIdElement = apiCodytTaskElement.Element(beeDocumentId);
                        XElement documentNameElement = apiCodytTaskElement.Element(documentName);
                        XElement taskCodeElement = apiCodytTaskElement.Element(taskCode);

                        TaskInfo taskInfo = new TaskInfo();
                        taskInfo.TaskId = taskIdElement.Value;
                        taskInfo.BeeDocumentId = beeDocumentIdElement.Value;
                        taskInfo.DocumentName = documentNameElement.Value;
                        taskInfo.TaskCode = taskCodeElement.Value;

                        docInfos.Add(taskInfo);
                    }
                }
            }
            catch (Exception e)
            { 
                m_error = getErrorFromMessage(e.Message);
                docInfos = null;
                CLogger.WriteLog(ELogLevel.DEBUG, "Error in FindProjectDocInfo(" + projectId + ")", e);
            }
            return docInfos;
        }

        /*
         * Find yet unassigned jobs	Use method:

            (GET) projects/{PROJECTID}/tasks?token={TOKENID}
            &sourcelocale={SOURCELOCALE}
            &targetlocale={TARGETLOCALE}
            &status={STATUS}
            &filter={FILTER}

            The language parameters can be dropped.

            Now set the “filter” to enumerate unassigned jobs for a file:
            &filter=(Status_= 0) AND (DocumentName = ”{name of file}”)
            See doc “API Objects” > “APICodytTask” for other fields you can filter

            For each job returned, note down these properties:

            -	BeeDocumentId (the unique ID of the previously uploaded file)
            -	TaskId (the unique job identifier)
            -	TaskCode (to know if it is a translation, revision, etc.)

            You will need them to subsequently assign suppliers. 
         * 
         * 
         * Status	The task's status which is a numeric value of:
                /// <summary>
                /// Work in the active step did not yet start, i.e. the worker did not yet open the work
                /// </summary>
                NotAssigned = 0,
                /// <summary>
                /// Work in the active step did not yet start, i.e. the worker did not yet open the work
                /// </summary>
                NotStarted = 1,
                /// <summary>
                /// The step is in progress.
                /// </summary>
                InProgress = 2,
                /// <summary>
                /// The step is completed.
                /// </summary>
                Completed = 3,
                /// <summary>
                /// The task is not yet active because another step in the workflow is not yet ready
                /// </summary>
                Inactive = 10,
                /// <summary>
                /// The step is cancelled.
                /// </summary>
                Cancelled = 20,
                TaskCode	Can be TR (Translation), RV (Revision) or APP (Approval/Proofreading)
                LocaleSource
                LocaleTarget	The languages of the task.
                Reference	The task reference
                AllocMode_	The type of allocation:
                /// <summary>
                /// A single user must be allocated to a work item task (translation, revision and approval).
                /// The user who allocates must have proper rights for it.
                /// </summary>
                SingleUser = 0,
                /// <summary>
                /// Any user can pick up a work item and put his name on it to reserve exclusive work.
                /// The user can remove his name if he wants to release item for other people.
                /// </summary>
                SingleUserSelfService = 1,
                /// <summary>
                /// Multiple users can work at the same time. The work item is then simply not allocated to
                /// a specific person.
                /// </summary>
                MultiUser = 2

         * 
         * 9.7.2	Project tasks
            To list or find revision sets of a Codyt project. A revision set represents a specific version of a source document.
            Method
            Name	projects/{projectid}/tasks
            Url	http://api.wordbee-translator.com/projects/{PROJECTID}/tasks?token={TOKENID}&sourcelocale={SOURCELOCALE}&targetlocale={TARGETLOCALE}
            &task={TASK}&status={STATUS}&filter={FILTER}&from={FROM}&count={COUNT} 
            Method	GET
            Parameters
            This method returns tasks for current document versions only.
            token	        The token obtained on connection	required
            projectid	    The project.	required
            sourcelocale	Optional parameter. To filter tasks by source language.	optional
            targetlocale	Optional parameter. To filter tasks by target language.	optional
            task	        Optional parameter. To filter tasks by their task code.	optional
            status	        Optional parameter. To filter tasks by the task status. 
                            See ApiCodytTask for possible values. optional
            from	The first record to show starting with index 0. Default is 0	optional
            count	The number of records to show, up to 100 at a time. Default is 100.	optional
            filter	Free filter on any project property. Please see Filtering results for details.
            optional 
         */
        // return a list unassigned documentID

        public List<TaskInfo> FindUnAssignedTasks(string projectId)
        {
            List<TaskInfo> UnassignedTasks = null;
            Connect();

            try
            {

                lastestAPIMethod = "FindUnAssignedTasks()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;
                using (WebClient clientc = new MyWebClient())
                {
                    string url = string.Format("{0}/projects/{1}/tasks?token={2}&status={3}", this.m_apiUrl, projectId, this.m_token, 0);
                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url); 
                    lastestAPIXML = result;
                    isGetResponse = true;

                    System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);

                    XName apiCodytTask = XName.Get("ApiCodytTask", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName taskId = XName.Get("RevisionSetTaskId", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName beeDocumentId = XName.Get("BeeDocumentId", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName documentName = XName.Get("DocumentName", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName taskCode = XName.Get("TaskCode", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    UnassignedTasks = new List<TaskInfo>();
                    foreach (XElement apiCodytTaskElement in e.Elements(apiCodytTask))
                    {
                        XElement taskIdElement = apiCodytTaskElement.Element(taskId);
                        XElement beeDocumentIdElement = apiCodytTaskElement.Element(beeDocumentId);
                        XElement documentNameElement = apiCodytTaskElement.Element(documentName);
                        XElement taskCodeElement = apiCodytTaskElement.Element(taskCode);

                        TaskInfo taskInfo = new TaskInfo();
                        taskInfo.TaskId = taskIdElement.Value;
                        taskInfo.BeeDocumentId = beeDocumentIdElement.Value;
                        taskInfo.DocumentName = documentNameElement.Value;
                        taskInfo.TaskCode = taskCodeElement.Value;

                        UnassignedTasks.Add(taskInfo);
                    } 
                }
            }
            catch (Exception e)
            { 
                m_error = getErrorFromMessage(e.Message);
                UnassignedTasks = null;
                CLogger.WriteLog(ELogLevel.DEBUG, "Error in FindUnAssignedTasks(" + projectId + ")", e);
            }
            return UnassignedTasks;
        }

        /*
         To get just the internal and external suppliers that are qualified to be assigned to a given job:

        (GET) projects/{PROJECTID}/documents/{DOCUMENTID}/tasks/{TASKID}/suppliers?token={TOKENID}
        */
        /*
         <ArrayOfApiSupplierItem xmlns="http://schemas.datacontract.org/2004/07/Wordbee.Database">
          <ApiSupplierItem>
            <Comments>String content</Comments>
            <CompanyId>2147483647</CompanyId>
            <CompanyName>String content</CompanyName>
            <DelayLeadHours>32767</DelayLeadHours>
            <DelayUnitsPerDay>32767</DelayUnitsPerDay>
            <HasDomains>true</HasDomains>
            <IsActive>true</IsActive>
            <IsClient>true</IsClient>
            <IsInternal>true</IsInternal>
            <LocaleSource>String content</LocaleSource>
            <LocaleTarget>String content</LocaleTarget>
            <LocalesPerfectMatch>true</LocalesPerfectMatch>
            <MatchingDomains>2147483647</MatchingDomains>
            <OkTaskCode>true</OkTaskCode>
            <PersonId>2147483647</PersonId>
            <PersonName>String content</PersonName>
            <PriceAmount>12678967.543233</PriceAmount>
            <PriceCurrency>String content</PriceCurrency>
            <PriceListCode>String content</PriceListCode>
            <PriceListId>2147483647</PriceListId>
            <PriceUnitCode>String content</PriceUnitCode>
            <PriceUnits>32767</PriceUnits>
            <Profile_>255</Profile_>
            <Rating>255</Rating>
            <ServiceId>2147483647</ServiceId>
            <SupplierName>String content</SupplierName>
            <TaskCode>String content</TaskCode>
            <WorkedForClientCount>2147483647</WorkedForClientCount>
          </ApiSupplierItem>
         */
        public List<SupplierInfo> getQualifiedSupplier(string projectId, string documentId, string taskId)
        {
            List<SupplierInfo> supplierInfoList = null;
            Connect();

            try
            {
                lastestAPIMethod = "getQualifiedSupplier()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;
                using (WebClient clientc = new MyWebClient())
                {

                    string url = string.Format("{0}/projects/{1}/documents/{2}/tasks/{3}/suppliers?token={4}",
                       this.m_apiUrl, projectId, documentId, taskId, this.m_token);
                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url); 
                    lastestAPIXML = result;
                    isGetResponse = true;

                    System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);

                    XName apiSupplierItem_XName = XName.Get("ApiSupplierItem", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName CompanyId_XName = XName.Get("CompanyId", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName IsActive_XName = XName.Get("IsActive", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName PersonId_XName = XName.Get("PersonId", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName SupplierName_XName = XName.Get("SupplierName", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    supplierInfoList = new List<SupplierInfo>(); 
                    foreach (XElement apiElement in e.Elements(apiSupplierItem_XName))
                    {
                        XElement CompanyId_Element = apiElement.Element(CompanyId_XName);
                        XElement IsActive_Element = apiElement.Element(IsActive_XName);
                        XElement PersonId_Element = apiElement.Element(PersonId_XName);
                        XElement SupplierName_Element = apiElement.Element(SupplierName_XName);
                        if (IsActive_Element.Value != null && IsActive_Element.Value.ToLower().Equals("true"))
                        {
                            SupplierInfo supplierInfo = new SupplierInfo();
                            supplierInfo.CompanyId = CompanyId_Element.Value;
                            supplierInfo.CompanyId = CompanyId_Element.Value;
                            supplierInfo.CompanyId = CompanyId_Element.Value;

                            supplierInfoList.Add(supplierInfo);
                        }
                    }
                }
            }
            catch (Exception e)
            {

                m_error = getErrorFromMessage(e.Message);
                supplierInfoList = null;
                CLogger.WriteLog(ELogLevel.DEBUG, 
                    String.Format("Error in getQualifiedSupplier({0}, {1}, {2})", projectId, documentId, taskId), e);
            }

            return supplierInfoList;

        }

        /*
         Url	projects/{PROJECTID}/documents/{DOCUMENTID}/tasks/{TASKID}/suppliers?token={TOKENID}&companyid={COMPANYID}&personid={PERSONID} 
        Method	PUT
        Parameters
        token	The token obtained on connection	required
        projectid	The project.	required
        documentid	The unique document identifier.	required
        taskid	The unique task identifier	required
        companyid	Assign the task to a company with this id or this is the company id of external worker.	optional
        personid	Assign the task to the worker with this person id. Parameter is optional in case the task should be spread across a whole company.	optional

         */
        public void AssignTaskSupplier(string projectId, string documentId, string taskId, string supplierCompanyId, string personalId)
        {

            Connect();

            lastestAPIMethod = "AssignTaskSupplier()";
            lastestAPIUrl = "";
            lastestAPIXML = "";
            isGetResponse = false;
            using (WebClient webClient = new MyWebClient())
            {
                string url = string.Format("{0}/projects/{1}/documents/{2}/tasks/{3}/suppliers", this.m_apiUrl,
                    projectId, documentId, taskId);
                lastestAPIUrl = url;

                System.Collections.Specialized.NameValueCollection formData = new System.Collections.Specialized.NameValueCollection();
                formData["token"] = this.m_token;
                formData["companyid"] = HttpUtility.UrlEncode(supplierCompanyId);
                if (!String.IsNullOrEmpty(personalId))
                    formData["personid"] = HttpUtility.UrlEncode(personalId); 

                byte[] responseBytes = webClient.UploadValues(url, "POST", formData);
                isGetResponse = true;
                try
                {
                    string result = Encoding.UTF8.GetString(responseBytes);
                    lastestAPIXML = result;
                }
                catch (Exception e) {
                    CLogger.WriteLog(ELogLevel.DEBUG,
                            String.Format("Error in AssignTaskSupplier({0}, {1}, {2}, {3}, {4})", 
                                    projectId, documentId, taskId, supplierCompanyId, personalId), e);
                }
            } 
        } 
       
        /*Get all completed workflows:
        (GET) projects/workflows?token={TOKENID}
        &sourcelocale={SOURCELOCALE}
        &targetlocale={TARGETLOCALE}
        &status={STATUS}
        &statusdatefrom={DATEFROM}
        &filter={FILTER}
        &from=0
        &count=100

        Set {STATUS} to 1 for finished workflows only.
        See API doc “API Objects” > “ApiCodytRevisionSetLocale” for the various values.

        Use {DATEFROM} to check only workflows that were finished at or after this date. This avoids that you query all historical data.

        Set filter to &filter=ProjectCustomStrXX=”ClayTablet” to filter only on projects that are managed by ClayTablet. See Prerequisites chapter in this document.

        This method returns each finished workflow together with the file name and the project id. You will also get the document id if you need it for tracking purposes.

        If later, a user re-opens the workflow and closes it again, this method will return the workflow again because the status date will have been updated.
        */

        /*
         * <ArrayOfApiWorkflow xmlns="http://schemas.datacontract.org/2004/07/Wordbee.Database">
              <ApiWorkflow>
                <ProjectId>2147483647</ProjectId>
                <ProjectReference>String content</ProjectReference>
                <LocaleSource>String content</LocaleSource>
                <LocaleTarget>String content</LocaleTarget>
                <BeeDocumentId>2147483647</BeeDocumentId>
                <Name>String content</Name>
                <Version>String content</Version>
                <VersionDate>1999-05-31T11:20:00</VersionDate>
                <IsCurrent>true</IsCurrent>
                <Status_>255</Status_>
                <StatusUpdateDate>1999-05-31T11:20:00</StatusUpdateDate>
                <OpenTasks>32767</OpenTasks>
                <Comments>String content</Comments>
                <RevisionSetId>2147483647</RevisionSetId>
                <BeeDocumentSetId>2147483647</BeeDocumentSetId>
                <ProjectCustomStr1>String content</ProjectCustomStr1>
                <ProjectCustomStr2>String content</ProjectCustomStr2> 
                     *...
                <ProjectCustomStr20>String content</ProjectCustomStr20>
              </ApiWorkflow>
        */
        public List<DocumentInfo> getDocsWithCompletedWorkflows(string projectId, string sourceLanguage, string targetLanguage, DateTime datefrom, int totalRecords, string filter)
        {
            List<DocumentInfo> docInfoList = new List<DocumentInfo>();
            string sourcelocale = this.getMapppingTMSLanguage(sourceLanguage);
            string targetlocale = this.getMapppingTMSLanguage(targetLanguage);
            Connect();

            try
            {
                lastestAPIMethod = "getDocsWithCompletedWorkflows()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;
                using (WebClient clientc = new MyWebClient())
                {
                    string url = string.Format("{0}/projects/workflows?token={1}&sourcelocale={2}&targetlocale={3}&status={4}&statusdatefrom={5}&filter={6}&from=0&count={7}",
                       this.m_apiUrl, this.m_token, sourcelocale, targetlocale,
                       1, toUTCISO8601(datefrom.AddHours(-24)), HttpUtility.UrlEncode(filter),
                       totalRecords);

                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url);
                    lastestAPIXML = result;
                    isGetResponse = true;

                    if (logAPIDetails)
                    {
                        CLogger.WriteLog(ELogLevel.DEBUG, "getDocsWithCompletedWorkflows() URL: \n" + lastestAPIUrl);
                        CLogger.WriteLog(ELogLevel.DEBUG, "WB return: \n" + result);
                    }


                    System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);

                    XName apiWorkflow_XName = XName.Get("ApiWorkflow", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName beeDocumentId_XName = XName.Get("BeeDocumentId", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName beeDocumentName_XName = XName.Get("Name", "http://schemas.datacontract.org/2004/07/Wordbee.Database");

                    foreach (XElement apiWorkflowElement in e.Elements(apiWorkflow_XName))
                    {
                        XElement docIdElement = apiWorkflowElement.Element(beeDocumentId_XName);
                        XElement docNameElement = apiWorkflowElement.Element(beeDocumentName_XName);

                        DocumentInfo docinfo = new DocumentInfo();
                        docinfo.BeeDocumentId = docIdElement.Value;
                        docinfo.DocumentName = docNameElement.Value;
                        docInfoList.Add(docinfo);
                    }
                }
            }
            catch (Exception e)
            {
                m_error = getErrorFromMessage(e.Message);
                CLogger.WriteLog(ELogLevel.DEBUG,
                        String.Format("Error in getDocsWithCompletedWorkflows({0}, {1}, {2}, {3}, {4}, {5})",
                                projectId, sourceLanguage, targetLanguage, datefrom, totalRecords, filter), e);
            }

            return docInfoList;
        }

        public List<DocumentInfo> getDocsWithCompletedWorkflows(string projectId, string sourceLanguage, string targetLanguage, DateTime datefrom, int totalRecords)
        {
            List<DocumentInfo> docInfoList = new List<DocumentInfo>();
            string sourcelocale = this.getMapppingTMSLanguage(sourceLanguage);
            string targetlocale = this.getMapppingTMSLanguage(targetLanguage);
            Connect();

            try
            {
                lastestAPIMethod = "getDocsWithCompletedWorkflows()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;
                using (WebClient clientc = new MyWebClient())
                {
                    String url = "";
                    if (totalRecords == 0)
                      url = string.Format("{0}/projects/workflows?token={1}&sourcelocale={2}&targetlocale={3}&workflowstatus={4}&statusdatefrom={5}&projectid={6}",
                       this.m_apiUrl, this.m_token, sourcelocale, targetlocale,
                       1, toUTCISO8601(datefrom.AddHours(-24)), projectId);
                    else
                        url = string.Format("{0}/projects/workflows?token={1}&sourcelocale={2}&targetlocale={3}&workflowstatus={4}&statusdatefrom={5}&projectid={6}&from=0&count={7}",
                      this.m_apiUrl, this.m_token, sourcelocale, targetlocale,
                      1, toUTCISO8601(datefrom.AddHours(-24)), projectId,
                      totalRecords);

                    lastestAPIUrl = url; 

                    string result = clientc.DownloadString(url);
                    lastestAPIXML = result;
                    isGetResponse = true;


                    if (logAPIDetails)
                    {
                        CLogger.WriteLog(ELogLevel.DEBUG, "getDocsWithCompletedWorkflows() URL: \n" + lastestAPIUrl);
                        CLogger.WriteLog(ELogLevel.DEBUG, "WB return: \n" + result);
                    }

                    System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);

                    XName apiWorkflow_XName = XName.Get("ApiWorkflow", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName beeDocumentId_XName = XName.Get("BeeDocumentId", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName beeDocumentName_XName = XName.Get("Name", "http://schemas.datacontract.org/2004/07/Wordbee.Database");

                    foreach (XElement apiWorkflowElement in e.Elements(apiWorkflow_XName))
                    {
                        XElement docIdElement = apiWorkflowElement.Element(beeDocumentId_XName);
                        XElement docNameElement = apiWorkflowElement.Element(beeDocumentName_XName); 

                        DocumentInfo docinfo = new DocumentInfo();
                        docinfo.BeeDocumentId = docIdElement.Value;
                        docinfo.DocumentName = docNameElement.Value;
                        docInfoList.Add(docinfo);
                    }
                }
            }
            catch (Exception e)
            {
                m_error = getErrorFromMessage(e.Message);
                CLogger.WriteLog(ELogLevel.DEBUG,
                        String.Format("Error in getDocsWithCompletedWorkflows({0}, {1}, {2}, {3}, {4})",
                                projectId, sourceLanguage, targetLanguage, datefrom, totalRecords), e);
            }

            return docInfoList;
        }

        /*
          Find flagged files 

	   Use this method to find unfinished workflows that had been flagged by a user to transfer to Sharepoint. We discussed that the user would use a “Custom label” for flagging.

        (GET) projects/tasks?token={TOKENID}
        &sourcelocale={SOURCELOCALE}
        &targetlocale={TARGETLOCALE}
        &filter={FILTER}
        &from=0
        &count=100

        Use {DATEFROM} to check only workflows that were finished at or after this date. 
        This avoids that you query all historical data.

        Set filter to &filter=(ProjectCustomStrXX=”ClayTablet”) to filter only on projects that are managed by ClayTablet. See Prerequisites chapter in this document.

        Now set the “filter” to enumerate unassigned jobs for a file:
        &filter= … AND (Labels.Contains(”{label db code}”)

        The “label db code” is obtained from the API: Call (GET) settings/customlabels?token={TOKENID} and look up the label in the list. Navigate to the option you want to filter for. <DBCode> contains the db value.

        If required, set a “filter” on the workflow status. This example excludes completed workflows. To be seen if such filter is required for the use case.
        &filter= … AND (WorkflowStatus_ != 1)
        See API doc “API Objects” > “ApiCodytRevisionSetLocale” for the various values.

        Set filter on the workflow update date. To reduce the load on our database system, please indicate the UTC date of your last poll. The date time below is UTC.
        &filter= … AND (WorkflowStatusDate >= DateTime(2013, 5, 20)

        Alternatively you may want to filter on other properties such as the job status.

        This method returns file name and project id. You will also get the document id if you need it for tracking purposes.
       
         */


        public List<string> FindFlaggedFiles(string projectId, string sourceLanguage, string targetLanguage, DateTime datefrom, int totalRecords, string filter)
        {
            List<string> docIdList = null;
            string sourcelocale = this.getMapppingTMSLanguage(sourceLanguage);
            string targetlocale = this.getMapppingTMSLanguage(targetLanguage);
            Connect();

            try
            {
                lastestAPIMethod = "FindFlaggedFiles()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;

                using (WebClient clientc = new MyWebClient())
                {
                    string url = string.Format("{0}/projects/tasks?token={1}&sourcelocale={2}&targetlocale={3}&from=0&count={4}&filter={5}",
                       this.m_apiUrl, this.m_token, sourcelocale, targetlocale,
                       totalRecords, HttpUtility.UrlEncode(filter));
                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url);
                    lastestAPIXML = result;
                    isGetResponse = true;

                    System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);

                    XName apiWorkflow_XName = XName.Get("ApiCodytTask", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName beeDocumentId_XName = XName.Get("BeeDocumentId", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName ProjectId_XName = XName.Get("ProjectId", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    docIdList = new List<string>();
                    foreach (XElement apiWorkflowElement in e.Elements(apiWorkflow_XName))
                    {
                        XElement prjIdElement = apiWorkflowElement.Element(ProjectId_XName);
                        if (prjIdElement.Value != null && prjIdElement.Value.Equals(projectId))
                        {
                            XElement docIdElement = apiWorkflowElement.Element(beeDocumentId_XName);
                            docIdList.Add(docIdElement.Value);
                        }
                    }
                }
            }
            catch (Exception e)
            { 
                m_error = getErrorFromMessage(e.Message);
                docIdList = null;
                CLogger.WriteLog(ELogLevel.DEBUG,
                        String.Format("Error in FindFlaggedFiles({0}, {1}, {2}, {3}, {4}, {5})",
                                projectId, sourceLanguage, targetLanguage, datefrom, totalRecords, filter), e);
            }

            return docIdList;

        }

        /*
         * Automatically build translated file	This method constructs the translated file from the translated segments. It builds the file and stores it to the deliverable location.

            Please note that building the translated file may fail for various reasons: The most frequent is that a user forgot to fix markup. 
            Therefore, a mechanism must be put in place to notify the user of a “problem” and ask for action. 
            Before calling this method, you should check if the deliverable was not already uploaded by the user (or from the API).

            Use this method to download files:

            projects/{PROJECTID}/files/{LOCALE}/file/translation?token={TOKENID}
            &name={NAME}&targetlocale={TARGETLOCALE}&targetname={TARGETNAME}
            &targetEncoding={TARGETENCODING}&targetFormat={TARGETFORMAT} 

            tokenid	    The connection token	                required
            projectid	The project id	                        required
            locale	    The original file's source language	    required
            targetlocale	The language of the translated file	required
            targetname	If empty then the translated document will be saved with the same path/name as the original document (default behavior). 
                        Otherwise specify a target path/name. The file extension must match that of the original document.	optional
            targetEncoding	Do not set by default. Permits to customize the file encoding of the target file for certain document formats such as web pages.	optional
            targetFormat	Do not set by default. Permits to customize internal file format options depending on document format. Contact Wordbee for more information.	optional

            */

        public void BuildTranslatedFile(string projectid,
            string sourceCTLanguage, string targetCTLanguage, String fileName )
         {

             Connect();

            string sourcelocale = this.getMapppingTMSLanguage(sourceCTLanguage);
            string targetlocale = this.getMapppingTMSLanguage(targetCTLanguage);

            lastestAPIMethod = "BuildTranslatedFile()";
            lastestAPIUrl = "";
            lastestAPIXML = "";
            isGetResponse = false;
            using (WebClient webClient = new MyWebClient())
            {
                string url = string.Format("{0}/projects/{1}/files/{2}/file/translation?token={3}&name={4}&targetlocale={5}&targetname={6}", this.m_apiUrl, projectid, sourcelocale, this.m_token,
                    HttpUtility.UrlEncode(fileName), HttpUtility.UrlEncode(targetlocale),HttpUtility.UrlEncode(fileName));
                lastestAPIUrl = url;

                byte[] responseBytes = webClient.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(""));
                string result = Encoding.UTF8.GetString(responseBytes);
                lastestAPIXML = result;
                isGetResponse = true;
            } 
        }


        /*
        * Create folder
            This method permits to create a new folder or sub folder in the library.
            Method
            Name	projects/{projectid}/folders/{locale}/folder
            Url	http://api.wordbee-translator.com/projects/{PROJECTID}/folders/{LOCALE}/folder?token={TOKENID}
            &name={NAME}
            Method	POST
            Parameters
            token	The connection token	required
            projectid	The project	required
            locale	The language.	required
            name	The path/name of the new folder. If the folder already exists an error is returned.
            Example: "name=folder1\folder2\folder3" creates all three folder levels if these don't yet exist.	required


           */

        public void CreateFolderStructure(string projectid,
            string ctLanguage, String folderStructure)
        {

            Connect();

            string locale = this.getMapppingTMSLanguage(ctLanguage);

            lastestAPIMethod = "CreateFolderStructure()";
            lastestAPIUrl = "";
            lastestAPIXML = "";
            isGetResponse = false;
            using (WebClient webClient = new MyWebClient())
            {
                string url = string.Format("{0}/projects/{1}/folders/{2}/file/folder?token={3}&name={4}", this.m_apiUrl, projectid, HttpUtility.UrlEncode(locale), this.m_token,
                    HttpUtility.UrlEncode(folderStructure));
                lastestAPIUrl = url;

                byte[] responseBytes = webClient.UploadData(url, "POST", System.Text.Encoding.ASCII.GetBytes(""));
                string result = Encoding.UTF8.GetString(responseBytes);
                lastestAPIXML = result;
                isGetResponse = true;
            }
        }

        /*
        To find the deliverable or an attachment in the target languages, use this method:

           (GET) projects/{projectid}/filegroup?token={tokenid}&locales={locales}&name={name}&documentid={documentid}

           Specify either {name} or {documentid}. No need to set both.

           Locales can be one or more languages such as “en”, “en,fr,es” to select a single or multiple languages. Include the source language to find attachments to the source document.

           The result is a list:

           -	It can contain either one or zero deliverables (IsAttachment = false)
           -	It can contain zero, one or more attachments (IsAttachment = true)

           You need to find out with Nikon what to return back to the CMS. The record with IsAttachment = false should be given preference.
           Example where the translation in English consists of a deliverable and one attached file.

           <ArrayOfApiFileGroupItem>

           <ApiFileGroupItem>
           <Date>2013-05-17T10:38:33.8096895Z</Date>
           <FileEncoding>utf-8</FileEncoding>
           <FileName>folder1\file.doc</FileName>
           <FormatInfo>.doc</FormatInfo>
           <IsAttachment>false</IsAttachment>
           <IsSource>false</IsSource>
           <Locale>en</Locale>
           </ApiFileGroupItem>

           <ApiFileGroupItem>
           <Date>2013-05-17T10:39:09.4047254Z</Date>
           <FileEncoding i:nil="true"/>
           <FileName>folder1\file.doc_\mydeliverable.doc</FileName>
           <FormatInfo i:nil="true"/>
           <IsAttachment>true</IsAttachment>
           <IsSource>false</IsSource>
           <Locale>en</Locale>
           </ApiFileGroupItem>
           */
        public bool isFileTranslated(string projectId, string fileName, string targetLanguage)
        {
            bool isDone = false;
            string targetlocale = this.getMapppingTMSLanguage(targetLanguage);
            Connect();

            try
            {
                lastestAPIMethod = "isFileTranslated()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;
                using (WebClient clientc = new MyWebClient())
                {

                    string url = string.Format("{0}/projects/{1}/filegroup?token={2}&locales={3}&name={4}",
                        this.m_apiUrl, projectId, this.m_token, HttpUtility.UrlEncode(targetlocale), HttpUtility.UrlEncode(fileName));
                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url);
                    lastestAPIXML = result;
                    isGetResponse = true;

                    if (logAPIDetails)
                    {
                        CLogger.WriteLog(ELogLevel.DEBUG, "isFileTranslated() URL: \n" + lastestAPIUrl);
                        CLogger.WriteLog(ELogLevel.DEBUG, "WB return: \n" + result);
                    }


                    System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);

                    XName apiFileGroupItem_XName = XName.Get("ApiFileGroupItem", "http://schemas.datacontract.org/2004/07/Wordbee.Api.Rest.Library");
                    XName fileName_XName = XName.Get("FileName", "http://schemas.datacontract.org/2004/07/Wordbee.Api.Rest.Library");

                    foreach (XElement apiFileGroupItemElement in e.Elements(apiFileGroupItem_XName))
                    {
                        XElement fileNameElement = apiFileGroupItemElement.Element(fileName_XName);
                        String findFileName = fileNameElement.Value;

                        if (findFileName.Equals(fileName))
                        {
                            isDone = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                m_error = getErrorFromMessage(e.Message);
                CLogger.WriteLog(ELogLevel.DEBUG,
                        String.Format("Error in isFileTranslated({0}, {1}, {2})", projectId, fileName, targetLanguage), e);
            }
            return isDone;
        }

        public String getTranslatedFilePath(string projectId, string fileName, string targetLanguage)
        {
            String filePath = null;
            string targetlocale = this.getMapppingTMSLanguage(targetLanguage);
            Connect();

            try
            {
                lastestAPIMethod = "getTranslatedFilePath()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;
                using (WebClient clientc = new MyWebClient())
                {
                    string url = string.Format("{0}/projects/{1}/files/{2}?token={3}&namepattern={4}",
                        this.m_apiUrl, projectId, HttpUtility.UrlEncode(targetlocale), this.m_token, HttpUtility.UrlEncode( fileName ) );
                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url);
                    lastestAPIXML = result;
                    isGetResponse = true;

                    if (logAPIDetails)
                    {
                        CLogger.WriteLog(ELogLevel.DEBUG, "getTranslatedFilePath() URL: \n" + lastestAPIUrl);
                        CLogger.WriteLog(ELogLevel.DEBUG, "WB return: \n" + result);
                    }

                    System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);

                    XName apiFilePaths_XName = XName.Get("ApiFileInfo", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName filePath_XName = XName.Get("Name", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    // XName fileDate_XName = XName.Get("Date", "http://schemas.datacontract.org/2004/07/Wordbee.Database");

                    foreach (XElement apiFilePathsElement in e.Elements(apiFilePaths_XName))
                    {
                        XElement fileNameElement = apiFilePathsElement.Element(filePath_XName);
                        String findFilePath = fileNameElement.Value;
                        filePath = findFilePath.Replace(@"\\", @"\");
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                m_error = getErrorFromMessage(e.Message);
                CLogger.WriteLog(ELogLevel.DEBUG,
                        String.Format("Error in getTranslatedFilePath({0}, {1}, {2})", projectId, fileName, targetLanguage), e);
            }
            return filePath;
        }
        public String getTranslatedFilePath3(string projectId, string fileName, string targetLanguage)
        {
            String filePath = null;
            string targetlocale = this.getMapppingTMSLanguage(targetLanguage);
            Connect();

            try
            {
                lastestAPIMethod = "getTranslatedFilePath3()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;
                using (WebClient clientc = new MyWebClient())
                {
                    string url = string.Format("{0}/projects/{1}/files/{2}?token={3}&namepattern={4}",
                        this.m_apiUrl, projectId, HttpUtility.UrlEncode(targetlocale), this.m_token, HttpUtility.UrlEncode(fileName + "/*" + Path.GetExtension(fileName)));
                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url);
                    lastestAPIXML = result;
                    isGetResponse = true;


                    if (logAPIDetails)
                    {
                        CLogger.WriteLog(ELogLevel.DEBUG, "getTranslatedFilePath3() URL: \n" + lastestAPIUrl);
                        CLogger.WriteLog(ELogLevel.DEBUG, "WB return: \n" + result);
                    }

                    System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);

                    XName apiFilePaths_XName = XName.Get("ApiFileInfo", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName filePath_XName = XName.Get("Name", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                   // XName fileDate_XName = XName.Get("Date", "http://schemas.datacontract.org/2004/07/Wordbee.Database");

                    foreach (XElement apiFilePathsElement in e.Elements(apiFilePaths_XName))
                    {
                        XElement fileNameElement = apiFilePathsElement.Element(filePath_XName);
                        String findFilePath = fileNameElement.Value; 
                        filePath = findFilePath.Replace(@"\\", @"\"); 
                        break; 
                    }
                }
            }
            catch (Exception e)
            {
                m_error = getErrorFromMessage(e.Message);
                CLogger.WriteLog(ELogLevel.DEBUG,
                        String.Format("Error in getTranslatedFilePath3({0}, {1}, {2})", projectId, fileName, targetLanguage), e);
            }
            return filePath;
        }
        /*
         * <ArrayOfApiFileInfo xmlns="http://schemas.datacontract.org/2004/07/Wordbee.Database" 
         * xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
         * <ApiFileInfo><Date>2013-09-25T17:09:38.9942647Z</Date>
         * <Locale>en-US</Locale>
         * <Name>(test34)X8Dry-SD1-AF-Acceleration-INT-01.doc_\(test34)X8Dry-SD1-AF-Acceleration-INT-01_Eng.doc</Name>
         * <TranslationMode>TranslateNo</TranslationMode></ApiFileInfo>
         * </ArrayOfApiFileInfo>
         */
        public String getTranslatedFilePath2(string projectId, string fileName, string targetLanguage)
        {
            String filePath = null;
            string targetlocale = this.getMapppingTMSLanguage(targetLanguage);
            Connect();

            try
            {
                lastestAPIMethod = "getTranslatedFilePath2()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;
                using (WebClient clientc = new MyWebClient())
                {
                    string url = string.Format("{0}/projects/{1}/files/{2}?token={3}&namepattern={4}",
                        this.m_apiUrl, projectId, HttpUtility.UrlEncode(targetlocale), this.m_token, HttpUtility.UrlEncode(targetlocale + "/" + fileName));
                    lastestAPIUrl = url;

                    string result = clientc.DownloadString(url);
                    lastestAPIXML = result;
                    isGetResponse = true;

                    if (logAPIDetails)
                    {
                        CLogger.WriteLog(ELogLevel.DEBUG, "getTranslatedFilePath2() URL: \n" + lastestAPIUrl);
                        CLogger.WriteLog(ELogLevel.DEBUG, "WB return: \n" + result);
                    }

                    System.Xml.Linq.XElement e = System.Xml.Linq.XElement.Parse(result);

                    XName apiFilePaths_XName = XName.Get("ApiFileInfo", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    XName filePath_XName = XName.Get("Name", "http://schemas.datacontract.org/2004/07/Wordbee.Database");
                    // XName fileDate_XName = XName.Get("Date", "http://schemas.datacontract.org/2004/07/Wordbee.Database");

                    foreach (XElement apiFilePathsElement in e.Elements(apiFilePaths_XName))
                    {
                        XElement fileNameElement = apiFilePathsElement.Element(filePath_XName);
                        String findFilePath = fileNameElement.Value;
                        filePath = findFilePath.Replace(@"\\", @"\");
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                m_error = getErrorFromMessage(e.Message);
                CLogger.WriteLog(ELogLevel.DEBUG,
                        String.Format("Error in getTranslatedFilePath2({0}, {1}, {2})", projectId, fileName, targetLanguage), e);
            }
            return filePath;
        }

        /*
         Name	projects/{projectid}/files/{locale}/file
            Url	http://api.wordbee-translator.com/projects/{PROJECTID}/files/{LOCALE}/file?token={TOKENID}&names={NAMES}
            &zip={zip}
            Method	GET
            Parameters
            token	The connection token	required
            projectid	The project	required
            locale	The language.	required
            name	The path/name of the file or files to download.
            To download multiple files, separate the file names/paths by ":". The files will be zipped and the method returns a zip file.	required
            zip	If set to "true", the downloaded file will be zipped. If omitted, the file is not zipped.	optional
            */
        public bool SaveTranslatedFile(string projectId, string targetLanguage, string fileName, string saveAsFilePath)
        {
            bool isDone = false;
            string targetlocale = this.getMapppingTMSLanguage(targetLanguage);
            Connect();
            try
            {

                lastestAPIMethod = "SaveTranslatedFile()";
                lastestAPIUrl = "";
                lastestAPIXML = "";
                isGetResponse = false;
                if (File.Exists(saveAsFilePath))
                {
                    File.Delete(saveAsFilePath);
                }

                string url =
                           string.Format(
                           "{0}/projects/{1}/files/{2}/file?token={3}&names={4}",
                           this.m_apiUrl,
                           projectId,
                           HttpUtility.UrlEncode(targetlocale),
                           this.m_token,
                           HttpUtility.UrlEncode(fileName));

                lastestAPIUrl = url;

                if (logAPIDetails)
                {
                    CLogger.WriteLog(ELogLevel.DEBUG, "SaveTranslatedFile() URL: \n" + lastestAPIUrl); 
                }

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Timeout = 20 * 60 * 1000;
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                using (Stream s = response.GetResponseStream())
                {
                    using (FileStream st = new FileStream(saveAsFilePath, FileMode.Create))
                    {
                        CopyStream(s, st);
                    }
                    isDone = true;
                }
                isGetResponse = true;
            }
            catch (Exception e)
            { 
                m_error = getErrorFromMessage(e.Message);
                isDone = false;
                CLogger.WriteLog(ELogLevel.DEBUG,
                        String.Format("Error in SaveTranslatedFile({0}, {1}, {2}, {3})", 
                                projectId, targetLanguage, fileName, saveAsFilePath), e);
            }

            return isDone;
        }

        private void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }

        public string getMapppingCTLanguage(String tmsLanguage)
        {
            foreach (TMSLanguageMapping map in m_TMSLanguageMappings)
            {
                if (String.Compare(map.TMSLanguage, tmsLanguage, true) == 0)
                    return map.CTTLanguage;

            }
            return tmsLanguage;

        }

        public string getMapppingTMSLanguage(String ctLanguage)
        {
            foreach (TMSLanguageMapping map in m_TMSLanguageMappings)
            {
                if (String.Compare(map.CTTLanguage, ctLanguage, true) == 0)
                    return map.TMSLanguage;

            }
            return ctLanguage;

        }

        private String ReadStringFromFile(String fileNamePath)
        {
            String fileString;
            // create a writer and open the file
            TextReader textReader = new StreamReader(fileNamePath, Encoding.UTF8);

            // write a line of text to the file
            fileString = textReader.ReadToEnd();

            // close the stream
            textReader.Close();

            return fileString;
        } 

        public void loadLanguageMappings()
        {
            string curFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string xmlFile = curFolder + "LanguageMapping.xml";
            if (!(curFolder.EndsWith("\\")))
                xmlFile = curFolder + "\\" + "LanguageMapping.xml";


            if ( System.IO.File.Exists(xmlFile )  )
            {
                XmlTextReader txtReader = null;
                try
                {
                    txtReader = new XmlTextReader(xmlFile);
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(txtReader);

                    XmlNodeList xmlPGrpNodeList = xmldoc.GetElementsByTagName("LanguageMapping");
                    foreach (XmlNode xmlNode in xmlPGrpNodeList)
                    {
                        TMSLanguageMapping newMapping = new TMSLanguageMapping();

                        XmlNodeList fieldsNoteList = xmlNode.ChildNodes;
                        foreach (XmlNode fieldNode in fieldsNoteList)
                        {
                            if (String.Compare(fieldNode.Name, "CTLanguageCode", true) == 0)
                                newMapping.CTTLanguage = fieldNode.InnerText;
                            else if (String.Compare(fieldNode.Name, "TMSLanguageCode", true) == 0)
                                newMapping.TMSLanguage = fieldNode.InnerText;
                        }

                        m_TMSLanguageMappings.Add(newMapping);
                    }
                }
                catch (Exception e) {
                    CLogger.WriteLog(ELogLevel.DEBUG, "Error laoding language mapping: " + xmlFile, e);
                }
                finally
                {
                    if (txtReader != null)
                        txtReader.Close();
                }
            }
        }
   
        private string getUniqueJobName(String jobNamePrefix)
        {
            string dateString = DateTime.Now.ToUniversalTime().ToString("MM-dd-yyyy_HH.mm.ss");
            if (jobNamePrefix.EndsWith("_") || jobNamePrefix.EndsWith("-"))
                return jobNamePrefix + dateString + "_" + System.Guid.NewGuid().ToString().Substring(0, 8) ;
            else
                return jobNamePrefix + "_" + dateString + "_" + System.Guid.NewGuid().ToString().Substring(0, 8);
        }

        private string getErrorFromMessage(string message)
        {
            string error = String.Empty;
            if (String.IsNullOrEmpty(message))
                return error;
            else
            {
                int pos1 = message.IndexOf("<error>");
                int pos2 = message.IndexOf("</error>");

                if (pos2 > pos1 && pos1 > -1)
                {

                    error = message.Substring(pos1 + 7, pos2 - pos1 - 7);
                    return error;
                }
                else
                    return error;
            }

        }

        private string toUTCISO8601(DateTime dt)
        {
            DateTime dt_utc = dt.ToUniversalTime();
            return UtcIsoTimeUtil.toUTCISO8601Format(dt_utc);
        }


    }
}
