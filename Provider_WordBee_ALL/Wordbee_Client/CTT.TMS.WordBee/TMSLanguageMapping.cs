﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CTT.TMS.GeneralTMS
{
    public class TMSLanguageMapping
    {
        private string m_cttLanguage;
        private string m_tmsLanguage;

        public string CTTLanguage
        {
            get { return this.m_cttLanguage; }
            set { this.m_cttLanguage = value; }
        }

        public string TMSLanguage
        {
            get { return this.m_tmsLanguage; }
            set { this.m_tmsLanguage = value; }
        }

    }
}
