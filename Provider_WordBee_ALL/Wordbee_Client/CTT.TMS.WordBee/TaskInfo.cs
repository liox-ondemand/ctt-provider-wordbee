﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CTT.TMS.Wordbee
{
    public class TaskInfo
    {
        private string m_BeeDocumentId  = null;
        private string m_DocumentName = null;
        private string m_TaskId  = null;
        private string m_TaskCode = null;

        public string BeeDocumentId
        {
            get { return this.m_BeeDocumentId; }
            set { this.m_BeeDocumentId = value; }
        }

        public string DocumentName
        {
            get { return this.m_DocumentName; }
            set { this.m_DocumentName = value; }
        }

        public string TaskId
        {
            get { return this.m_TaskId; }
            set { this.m_TaskId = value; }
        }

        public string TaskCode
        {
            get { return this.m_TaskCode; }
            set { this.m_TaskCode = value; }
        }
    }
}
