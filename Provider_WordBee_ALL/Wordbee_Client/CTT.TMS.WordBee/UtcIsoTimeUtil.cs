﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CTT.TMS.Wordbee
{
    public class UtcIsoTimeUtil
    {
        public static string toUTCISO8601Format(DateTime dt)
        {
            string datePatt = @"yyyy-MM-ddTHH:mm:ssZ";
            DateTime dispDt = dt.ToUniversalTime();
            return dispDt.ToString(datePatt);
        }
    }
}
