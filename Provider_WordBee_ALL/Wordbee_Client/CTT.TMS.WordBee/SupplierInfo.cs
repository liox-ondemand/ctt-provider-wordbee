﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CTT.TMS.Wordbee
{
    public class SupplierInfo
    {
        private string m_companyId  = null;
        private string m_personId  = null;
        private string m_supplierName = null;

        public string CompanyId
        {
            get { return this.m_companyId; }
            set { this.m_companyId = value; }
        }

        public string PersonId
        {
            get { return this.m_personId; }
            set { this.m_personId = value; }
        }

        public string SupplierName
        {
            get { return this.m_supplierName; }
            set { this.m_supplierName = value; }
        }
    }
}
