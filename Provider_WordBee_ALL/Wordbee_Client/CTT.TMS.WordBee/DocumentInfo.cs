﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CTT.TMS.Wordbee
{
    public class DocumentInfo
    {
        private string m_BeeDocumentId  = null;
        private string m_DocumentName = null; 

        public string BeeDocumentId
        {
            get { return this.m_BeeDocumentId; }
            set { this.m_BeeDocumentId = value; }
        }

        public string DocumentName
        {
            get { return this.m_DocumentName; }
            set { this.m_DocumentName = value; }
        } 
        
    }
}
