﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace CTT.TMS.WordBee
{
    public class JobDownloadable
    {
        
        private string m_jobName;
        private bool m_jobDownloadable;

        public bool IsJobDownloadable
        {
            get { return this.m_jobDownloadable; }
            set { this.m_jobDownloadable = value; }
        }         

        public string JobName
        {
            get { return this.m_jobName; }
            set { this.m_jobName = value; }
        }
         

    }
}
