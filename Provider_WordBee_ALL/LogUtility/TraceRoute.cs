﻿using System;
using System.Collections.Generic;
using System.Diagnostics; 
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace ClayTablet.CT.Utility
{
    public class TraceRoute
    {
        public static string getTraceRouteInfo(string ipAddressOrHostName, int maxHops, int timeout)
        {
            StringBuilder sb = new StringBuilder();

            IPAddress ipAddress = Dns.GetHostEntry(ipAddressOrHostName).AddressList[0];
            sb.AppendLine("Tracing route to " + ipAddressOrHostName + " [" + ipAddress.ToString() + "]\n");

            foreach (var entry in TraceRoute.Tracert(ipAddressOrHostName, maxHops, timeout))
            {
                sb.AppendLine(entry.ToString());
            }


            return sb.ToString();

        }

        public static IEnumerable<TracertEntry> Tracert(string ipAddressOrHostName, int maxHops, int timeout)
        {
            
            IPAddress address = Dns.GetHostEntry(ipAddressOrHostName).AddressList[0];

            // Max hops should be at least one or else there won't be any data to return.
            if (maxHops < 1)
                throw new ArgumentException("Max hops can't be lower than 1.");

            // Ensure that the timeout is not set to 0 or a negative number.
            if (timeout < 1)
                throw new ArgumentException("Timeout value must be higher than 0.");


            Ping ping = new Ping();
            PingOptions pingOptions = new PingOptions(1, true);
            Stopwatch pingReplyTime = new Stopwatch();
            PingReply reply;

            do
            {
                pingReplyTime.Start();
                reply = ping.Send(address, timeout, new byte[] { 0 }, pingOptions);
                pingReplyTime.Stop();

                string hostname = string.Empty;
                if (reply.Address != null)
                {
                    try
                    {
                        hostname = Dns.GetHostByAddress(reply.Address).HostName;    // Retrieve the hostname for the replied address.
                    }
                    catch (SocketException e)
                    {
                        CLogger.WriteLog(ELogLevel.DEBUG, "Cannot get host name", e);
                    }
                }

                // Return out TracertEntry object with all the information about the hop.
                yield return new TracertEntry()
                {
                    HopID = pingOptions.Ttl,
                    Address = reply.Address == null ? "N/A" : reply.Address.ToString(),
                    Hostname = hostname,
                    ReplyTime = pingReplyTime.ElapsedMilliseconds,
                    ReplyStatus = reply.Status
                };

                pingOptions.Ttl++;
                pingReplyTime.Reset();
            }
            while (reply.Status != IPStatus.Success && pingOptions.Ttl <= maxHops);
        }


    }
}
