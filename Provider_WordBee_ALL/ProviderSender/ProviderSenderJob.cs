using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using com.claytablet.model;
using com.claytablet.provider;
using com.claytablet.model.enm;
using com.claytablet.model.Event;
using com.claytablet.model.Event.platform;
using com.claytablet.model.Event.provider;
using com.claytablet.service.Event;
using com.claytablet.util;
using com.claytablet.queue;
using com.claytablet.queue.service;
using com.claytablet.queue.service.sqs;
using com.claytablet.storage;
using com.claytablet.storage.service;
using com.claytablet.storage.service.s3;
using ClayTablet.CT.Utility;
using CTT.TMS.WordBee;
using CTT.TMS.Wordbee;
using System.IO;
using System.Text.RegularExpressions; 

namespace ClayTablet.CT.Net.WordBee
{
    public class ProviderSenderJob
    {

        public void Process()
        {

            SourceAccountProvider sap;
            TargetAccountProvider tap;

            QueueSubscriberService queueSubscriberService;
            QueuePublisherService queuePublisherService;
            StorageClientService storageClientService;

            ConnectionContext context;
            ProviderSender sender;

            bool needEmailNotification = false;
            String fromEmail = null;
            String toEmailsString = "";
            List<string> toEmails = new List<string>();
            String smtpServer = null;
            int smtpPort = 25;
            bool smtpEnableSSL = false;
            String smtpUser = null;
            String smtpPassword = null;
            bool allowPickupTranslationEvenWorkflowNotCompleted = false;

            /*
             *  <add key="ClayTablet.Notification.Required" value="False" />    

                <add key="ClayTablet.Notification.From.EmailAddress" value="" />
                <add key="ClayTablet.Notification.To.EmailAddresses" value="" />
    
                <add key="ClayTablet.SMTP.Server" value="your_stmp_server.your_domain" />
                <add key="ClayTablet.SMTP.Port" value="25" />
                <add key="ClayTablet.SMTP.EnableSSL" value="false" /> 
             
                <add key="ClayTablet.SMTP.UserName" value="" />
                <add key="ClayTablet.SMTP.Password" value="" />
             * 
             * 
             * <add key="AllowPickupTranslationEvenWorkflowNotCompleted" value="false" />
             * 
             */

            if (System.Configuration.ConfigurationManager.AppSettings["AllowPickupTranslationEvenWorkflowNotCompleted"] == null)
            {
                allowPickupTranslationEvenWorkflowNotCompleted = false;
            }
            else
            {
                string allowPickupTranslationEvenWorkflowNotCompleted_setting = System.Configuration.ConfigurationManager.AppSettings["AllowPickupTranslationEvenWorkflowNotCompleted"].ToString().ToLower();

                if (allowPickupTranslationEvenWorkflowNotCompleted_setting.Equals("true"))
                    allowPickupTranslationEvenWorkflowNotCompleted = true;
                else
                    allowPickupTranslationEvenWorkflowNotCompleted = false;
            }

            if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.Required"] == null)
            {
                needEmailNotification = false;
            }
            else
            {
                string needEmailNotification_setting = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.Required"].ToString().ToLower();

                if (needEmailNotification_setting.Equals("true"))
                    needEmailNotification = true;
                else
                    needEmailNotification = false;
            }

            if (needEmailNotification)
            {

                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.From.EmailAddress"] == null)
                {
                    CLogger.WriteLog(ELogLevel.ERROR, "Can't find Form Email config [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.Notification.From.EmailAddress] in AppSetting .");
                    return;
                }
                else
                {
                    fromEmail = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.From.EmailAddress"].ToString();
                }


                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.To.EmailAddresses"] == null)
                {
                    CLogger.WriteLog(ELogLevel.ERROR, "Can't find To Emails config [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.Notification.To.EmailAddresses] in AppSetting .");
                    return;
                }
                else
                {
                    toEmailsString = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Notification.To.EmailAddresses"].ToString();


                    char[] splitchar = { ',', ';' };
                    String[] notifyEmailList = toEmailsString.Split(splitchar);
                    for (int i = 0; i < notifyEmailList.Length; i++)
                    {
                        toEmails.Add(notifyEmailList[i]);
                    }

                }


                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.Server"] == null)
                {
                    CLogger.WriteLog(ELogLevel.ERROR, "Can't find SMTP server config [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.SMTP.Server] in AppSetting .");
                    return;
                }
                else
                {
                    smtpServer = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.Server"].ToString();
                }


                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.Port"] != null)
                {

                    try
                    {
                        smtpPort = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.Port"].ToString());
                    }
                    catch (Exception)
                    {
                        smtpPort = 25;
                    }

                }

                smtpEnableSSL = false;
                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.EnableSSL"] != null)
                {
                    string EnableSSL_setting = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.EnableSSL"].ToString().ToLower();

                    if (EnableSSL_setting.Equals("true"))
                        smtpEnableSSL = true;
                    else
                        smtpEnableSSL = false;
                }

                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.UserName"] != null)
                {
                    smtpUser = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.UserName"].ToString();
                }

                if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.Password"] != null)
                {
                    smtpPassword = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.SMTP.Password"].ToString();
                }

            }

            var sourceAccountConfig = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.SourceAccount"];
            String sourceAccountXML = null;
            if (sourceAccountConfig == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Source Account XML file [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.Provider.SourceAccount] in AppSetting to point a Account XML file and make sure system have read permission with the Account file.");
                return;
            }
            else
            {
                sourceAccountXML = PathUtil.getFullPath4RelatedPath(sourceAccountConfig.ToString());
            }

            String targetAccountXML = null;
            var targetAccountConfig = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.TargetAccount"];
            if (targetAccountConfig == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Target Account XML file [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.Provider.TargetAccount] in AppSetting to point a Account XML file and make sure system have read permission with the Account file.");
                return;
            }
            else
            {
                targetAccountXML = PathUtil.getFullPath4RelatedPath(targetAccountConfig.ToString());
            }

            String contextFolder = null;
            if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ContextFolder"] == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the ConnectionContext Folder.\n\nPlease configure [ClayTablet.Provider.ContextFolder] in AppSetting to point a folder and make sure system have Full permission with the folder.");
                return;
            }
            else
            {
                contextFolder = PathUtil.getFullPath4RelatedPath(System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ContextFolder"].ToString());
            }

            String wbApiUrl = null;
            if (String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_ApiUrl"]))
            {

                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Wordbee API URL [in Configuration.AppSettings].\n\nPlease configure [CTT_WordBee_ApiUrl] in AppSetting.");
                return;
            }
            else
                wbApiUrl = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_ApiUrl"].ToString();


            String wbAccountId = null;
            if (String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_AccountId"]))
            {

                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Wordbee account ID [in Configuration.AppSettings].\n\nPlease configure [CTT_WordBee_AccountId] in AppSetting.");
                return;
            }
            else
                wbAccountId = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_AccountId"].ToString();

            String wbApiPassword = null;
            if (String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Password"]))
            {

                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Wordbee API password [in Configuration.AppSettings].\n\nPlease configure [CTT_WordBee_Password] in AppSetting.");
                return;
            }
            else
                wbApiPassword = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Password"].ToString();



            Boolean needAddToExistingProject = false;

            if (!String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_AddToProject"]))
            {
                String value = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_AddToProject"].ToString();
                if (value.ToLower().Equals("yes"))
                    needAddToExistingProject = true;
            }

            String wbApi_AddTo_ExistingProjectNameName = null;
            String wbApiClientCompanyName = null;
            if (needAddToExistingProject)
            {
                if (String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_AddToProject_ProjectName"]))
                {

                    CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Wordbee project name which files will be added to [in Configuration.AppSettings].\n\nPlease configure [CTT_WordBee_AddToProject_ProjectName] in AppSetting.");
                    return;
                }
                else
                    wbApi_AddTo_ExistingProjectNameName = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_AddToProject_ProjectName"].ToString();

            }
            else
            {

                if (String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Project_ClientCompanyName"]))
                {
                    CLogger.WriteLog(ELogLevel.INFO, "Cannot locate the CTT_WordBee_Project_ClientCompanyName [in Configuration.AppSettings], will use the master company ID for API calls.\n\nIf you need to use a specific company ID, Please configure [CTT_WordBee_Project_ClientCompanyName] in AppSetting.");

                }
                else
                {
                    wbApiClientCompanyName = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Project_ClientCompanyName"].ToString();
                    CLogger.WriteLog(ELogLevel.INFO, "Found ClientCompanyName: [" + wbApiClientCompanyName + "]. Provider will start to create translation projects under this name.");
                }
            }

            String wbApi_FolderFormat = null;
            if (!String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Project_FolderStructureFormat"]))
            {
                wbApi_FolderFormat = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Project_FolderStructureFormat"].ToString();
            }

            //Load ConnectionContext, SDK will look for a file called "connectionContext.xml"
            //in the folder -- "CTT_ConnectionContext_Folder"  
            context = new ConnectionContext(false);
            context.setConnectionContextPath(contextFolder);

            context.load();

            String clientId = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ClientId"];
            //Initial a source Account,  SDK will look for the source account file which appsetting ("CTT_SourceAccount") pointed.
            string sourceKey = FileUtil.ReadStringFromFile(sourceAccountXML);
            sap = new SourceAccountProvider(clientId, sourceKey);

            //Initial a target Account,  SDK will look for the source account file which appsetting ("CTT_TargetAccount") pointed.         
            string targetKey = FileUtil.ReadStringFromFile(targetAccountXML);
            tap = new TargetAccountProvider(clientId, targetKey);

            Account sourceAccount = sap.get();
            Account targetAccount = sap.get();

            if (sourceAccount == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot load source account key from " + sourceAccountConfig);
            }

            if (targetAccount == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot load target account key from " + targetAccountConfig);
            }

            if (sourceAccount==null || targetAccount==null)
            {
                return;
            }

            storageClientService = new StorageClientServiceS3();
            storageClientService.setPublicKey(sourceAccount.getPublicKey());
            storageClientService.setPrivateKey(sourceAccount.getPrivateKey());
            storageClientService.setStorageBucket(sourceAccount.getStorageBucket());

            queuePublisherService = new QueuePublisherServiceSQS();
            queuePublisherService.setPublicKey(sourceAccount.getPublicKey());
            queuePublisherService.setPrivateKey(sourceAccount.getPrivateKey());
            queuePublisherService.setEndpoint(sourceAccount.getQueueEndpoint());

            queueSubscriberService = new QueueSubscriberServiceSQS();
            queueSubscriberService.setPublicKey(sourceAccount.getPublicKey());
            queueSubscriberService.setPrivateKey(sourceAccount.getPrivateKey());
            queueSubscriberService.setEndpoint(sourceAccount.getQueueEndpoint());
             

            //Initial a ProviderSender, may need to send Event back.
            sender = new ProviderSender(context, sap, tap, queuePublisherService, storageClientService);


            Client wsClient = null;
            if (String.IsNullOrEmpty(wbApiClientCompanyName))
                 wsClient = new Client(wbApiUrl, wbAccountId, wbApiPassword); 
            else
                wsClient = new Client(wbApiUrl, wbAccountId, wbApiPassword, wbApiClientCompanyName);              

            List<ProviderProject> allProviderProjectList = JobHelper.Instance.ListProjects();

            List<ProviderProject> PrjList_hasAllFilesNeedCreateWBPrj = new List<ProviderProject>();
            List<ProviderProject> PrjList_CreatedWBPrj = new List<ProviderProject>();
            foreach (ProviderProject prj in allProviderProjectList)
            {
                 
                if (prj.isWordbeeProjectCreated )
                {
                    PrjList_CreatedWBPrj.Add(prj);
                }
                else if (prj.CTTotalAssetTasks == prj.CTReceivedAssetTasks)  // got all files, but WB project has n't been created 
                {
                    PrjList_hasAllFilesNeedCreateWBPrj.Add(prj);
                }
                else
                {
                    // do nothing, wait for all files coming
                }
            } 

            CLogger.WriteLog(ELogLevel.INFO, "Found total: " + PrjList_hasAllFilesNeedCreateWBPrj.Count + " project(s) need to create project in wordbee.");
            if (PrjList_hasAllFilesNeedCreateWBPrj.Count > 0 ||
                PrjList_CreatedWBPrj.Count > 0
                )
            {

                if (!wsClient.Connect())
                {
                    String errorMSg = "Failed to login to Wordbee server.";

                    CLogger.WriteLog(ELogLevel.ERROR, errorMSg);
                    CLogger.WriteLog(ELogLevel.ERROR, wsClient.Error);

                    String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                    CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                    String lastError = JobHelper.Instance.getLastError();
                    if (errorMSg.Equals(lastError))
                    {
                        //do nothing, same error
                    }
                    else
                    {

                        JobHelper.Instance.setLastError(errorMSg);
                        if (needEmailNotification)
                        {
                            SendNotificationEmail(fromEmail, toEmails, "Failed to connect Wordbee.", errorMSg + "\n" + wsClient.Error + "\n\n\n" + traceMsg, false,
                            smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                        }
                    }
                    return;
                } 
            }

            #region create Project
            int addToProjectId = 0;
            String addtoProjectRealName = wbApi_AddTo_ExistingProjectNameName;
 
            if (needAddToExistingProject && PrjList_hasAllFilesNeedCreateWBPrj.Count > 0)
            {
                try
                {
                    addToProjectId = wsClient.FindProjectId(wbApi_AddTo_ExistingProjectNameName);
                    try
                    {
                        addtoProjectRealName = wsClient.FindProjectName(addToProjectId);
                    }
                    catch (Exception e)
                    {
                        CLogger.WriteLog(ELogLevel.WARN, "Cannot get project name for " + addToProjectId, e);
                    }
                }
                catch (Exception e)
                {
                    String errorMSg = "Can't find the existing project with name: [" + wbApi_AddTo_ExistingProjectNameName + "]";
                    CLogger.WriteLog(ELogLevel.ERROR, errorMSg);
                    CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                    CLogger.WriteLog(ELogLevel.ERROR, wsClient.Error);

                    String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                    CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                    String lastError = JobHelper.Instance.getLastError();
                    if (errorMSg.Equals(lastError))
                    {
                        //do nothing, same error
                    }
                    else
                    {

                        JobHelper.Instance.setLastError(errorMSg);
                        if (needEmailNotification)
                        {
                            SendNotificationEmail(fromEmail, toEmails, "Failed to connect Wordbee.", errorMSg + "\n" + wsClient.Error + "\n\n\n" + traceMsg, false,
                            smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                        }
                    }
                    return;
                }
 
            }

            foreach (ProviderProject newPrj in PrjList_hasAllFilesNeedCreateWBPrj)
            {
                // need to create woedbee project.
                CLogger.WriteLog(ELogLevel.INFO, "\n\n");
                CLogger.WriteLog(ELogLevel.INFO, "CT job ID:" + newPrj.CTProjectId);
                CLogger.WriteLog(ELogLevel.INFO, "CT job Name:" + newPrj.CTProjectName);
                CLogger.WriteLog(ELogLevel.INFO, "CT Total files:" + newPrj.CTReceivedAssetTasks);

                if (needAddToExistingProject)
                {
                    CLogger.WriteLog(ELogLevel.INFO, "Need to add job files to existing Wordbee project:" + addtoProjectRealName);
                    newPrj.isWordbeeProjectCreated = true;
                    newPrj.WordbeeProjectId = addToProjectId;
                    newPrj.WordbeeProjectCreationDate = DateTime.Now;
                    newPrj.WordbeeAutoGenProjectName = addtoProjectRealName;

                    JobHelper.Instance.UpdateProviderProject(newPrj);

                    PrjList_CreatedWBPrj.Add(newPrj); 
                }
                else
                {
                    CLogger.WriteLog(ELogLevel.INFO, "Need to create a new Wordbee project.");

                    try
                    {

                        int templateProjectID = -1;
                        try
                        {
                            templateProjectID = wsClient.FindProjectId(newPrj.WordbeeTemplateProjectName);

                        }
                        catch (Exception e)
                        {
                            String errorMsg = "Failed to get TemplateProjectID of referenced project:[" + newPrj.WordbeeTemplateProjectName  + "]";
                            CLogger.WriteLog(ELogLevel.ERROR, errorMsg, e);
                            CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);

                            String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                            CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                            String lastError = newPrj.LastError;
                            if (errorMsg.Equals(lastError))
                            {
                            }
                            else
                            {
                                newPrj.LastError = errorMsg;
                                JobHelper.Instance.UpdateProviderProject(newPrj);
                                if (needEmailNotification)
                                {
                                    SendNotificationEmail(fromEmail, toEmails, "Failed to get TemplateProjectID", errorMsg + "\n\n\n" + traceMsg, false,
                                     smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                    CLogger.WriteLog(ELogLevel.INFO, "Send error message to [" + toEmailsString + "]");
                                }
                            }
                        }

                        if (templateProjectID != -1)
                        {
                            newPrj.WordbeeTemplateProjectId = Convert.ToString(templateProjectID);

                            String newProjectName = newPrj.WordbeeProjectReference;
                            //newProjectName = null;
                            CLogger.WriteLog(ELogLevel.INFO, "Start to create new project:" + newProjectName);

                            int wbProjectID = wsClient.CreateProject(newProjectName,
                                 newPrj.WordbeeTemplateProjectId,
                            newPrj.CTSourceLanguage, newPrj.CTTargetLanguages, newPrj.WordbeeProjectType.ToString(),
                            newPrj.WordbeeProjectDeadline, newPrj.WordbeeInternalComments, newPrj.WordbeeInstructions);

                            newPrj.isWordbeeProjectCreated = true; 
                            newPrj.WordbeeProjectId = wbProjectID;
                            newPrj.WordbeeProjectCreationDate = DateTime.Now;

                            String newProjectName_real = wsClient.FindProjectName(wbProjectID);
                            if (newProjectName_real != null)
                                newPrj.WordbeeAutoGenProjectName = newProjectName_real;

                            JobHelper.Instance.UpdateProviderProject(newPrj);

                            PrjList_CreatedWBPrj.Add(newPrj);
                        }
                    }
                    catch (Exception e)
                    {

                        string sourcelocale = wsClient.getMapppingTMSLanguage(newPrj.CTSourceLanguage);
                        string targetlocales = null;
                        foreach (string targerCTLng in newPrj.CTTargetLanguages)
                        {
                            if (targetlocales == null)
                                targetlocales = wsClient.getMapppingTMSLanguage(targerCTLng);
                            else
                                targetlocales += "," + wsClient.getMapppingTMSLanguage(targerCTLng);
                        }
                        targetlocales += "," + sourcelocale;

                        String errorMsg = "Failed to create a new Wordbee project.";
                            errorMsg += "\nPassed project information ------- ";
                            errorMsg += "\n Project Reference: " + newPrj.WordbeeProjectReference;
                            errorMsg += "\n TemplateProjectId: " + newPrj.WordbeeTemplateProjectId;
                            errorMsg += "\n Target locales: " + targetlocales;
                            errorMsg += "\n Project type: " + newPrj.WordbeeProjectType.ToString();
                            errorMsg += "\n Project Deadline: " + newPrj.WordbeeProjectDeadline;
                            errorMsg += "\n Internal Comments: " + newPrj.WordbeeInternalComments;
                            errorMsg += "\n Instructions: " + newPrj.WordbeeInstructions;
                            errorMsg += "\n Error: " + e.Message;

                        CLogger.WriteLog(ELogLevel.ERROR, errorMsg, e);
                        CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);

                        String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                         CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                         String lastError = newPrj.LastError;
                         if (errorMsg.Equals(lastError))
                         {
                         }
                         else
                         {
                             newPrj.LastError = errorMsg;
                             JobHelper.Instance.UpdateProviderProject(newPrj);
                             if (needEmailNotification)
                             {
                                 SendNotificationEmail(fromEmail, toEmails, "Failed to create new Wordbee project.", errorMsg + "\n\n\n" + traceMsg, false,
                                  smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                 CLogger.WriteLog(ELogLevel.INFO, "Send error message to [" + toEmailsString + "]");
                             }
                         }
                    
                    }
                }
            }
           #endregion create Project


            CLogger.WriteLog(ELogLevel.INFO, "Found total: " + PrjList_CreatedWBPrj.Count + " project(s) in provider side.");
            CLogger.WriteLog(ELogLevel.INFO, "Check projects existing ...");

            List<ProviderProject> PrjList_ExistWBPrj = new List<ProviderProject>();
            foreach (ProviderProject oldPrj in PrjList_CreatedWBPrj)
            {
                //check if project exist
                Boolean isExist = true;
                try
                {
                    isExist = wsClient.isProjectExist(oldPrj.WordbeeProjectId);
                }
                catch (Exception e)
                {
                    isExist = true;
                    if (!String.IsNullOrEmpty(oldPrj.WordbeeAutoGenProjectName))
                       CLogger.WriteLog(ELogLevel.ERROR, "Failed to check project existing for project:" + oldPrj.WordbeeAutoGenProjectName);
                    else
                        CLogger.WriteLog(ELogLevel.ERROR, "Failed to check project existing for project:" + oldPrj.CTProjectName);
                    CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);

                }

                if (!isExist)
                {
                    int checkNum = oldPrj.CheckedProjectNotExistNum + 1;
                    if (checkNum > 9)
                    {
                        CLogger.WriteLog(ELogLevel.ERROR, "\n\n");
                        CLogger.WriteLog(ELogLevel.ERROR, "Can't find project [ID:" + oldPrj.WordbeeProjectId + " Name:" + oldPrj.CTProjectName + "]");
                        CLogger.WriteLog(ELogLevel.ERROR, "WB final prject Name:" + oldPrj.WordbeeAutoGenProjectName + "]");
                        CLogger.WriteLog(ELogLevel.ERROR, "Will move project to NotFound to backup.");
                        JobHelper.Instance.BackupNotFoundProviderProject(oldPrj);
                        CLogger.WriteLog(ELogLevel.ERROR, "Skip this one.\n\n");
                    }
                    else
                    {
                        oldPrj.CheckedProjectNotExistNum = checkNum;
                        JobHelper.Instance.UpdateProviderProject(oldPrj);
                    }

                }
                else
                {
                    oldPrj.CheckedProjectNotExistNum = 0;
                    JobHelper.Instance.UpdateProviderProject(oldPrj);
                    PrjList_ExistWBPrj.Add(oldPrj);
                }
            }

            CLogger.WriteLog(ELogLevel.DEBUG, "Found total: " + PrjList_ExistWBPrj.Count + " project(s) in Wordbee.");
            foreach (ProviderProject oldPrj in PrjList_ExistWBPrj)
            { 
                // need to create woedbee project.
                CLogger.WriteLog(ELogLevel.DEBUG, "\n\n");
                CLogger.WriteLog(ELogLevel.DEBUG, "Need to check created Wordbee project.");
                CLogger.WriteLog(ELogLevel.DEBUG, "CT project ID:" + oldPrj.CTProjectId);
                CLogger.WriteLog(ELogLevel.DEBUG, "CT project Name:" + oldPrj.CTProjectName);
                CLogger.WriteLog(ELogLevel.DEBUG, "CT Total files:" + oldPrj.CTReceivedAssetTasks);
                CLogger.WriteLog(ELogLevel.DEBUG, "WB project ID:" + oldPrj.WordbeeProjectId);
                CLogger.WriteLog(ELogLevel.DEBUG, "WB project ref:" + oldPrj.WordbeeProjectReference);

                List<ProviderProjectFile> allProjectFiles = JobHelper.Instance.ListProviderProjectFiles(oldPrj.CTProjectId);
                List<ProviderProjectFile> needUploadFiles = new List<ProviderProjectFile>();
                List<ProviderProjectFile> needStartTransFiles = new List<ProviderProjectFile>();  

                foreach (ProviderProjectFile  prjFile in allProjectFiles)
                {
                    if (!prjFile.IsUploaded)
                        needUploadFiles.Add(prjFile);
                    else if (!prjFile.IsTransStarted)
                        needStartTransFiles.Add(prjFile);
                    else
                    {
                    }
                }
                int totalUploadFiles = needUploadFiles.Count;
                CLogger.WriteLog(ELogLevel.DEBUG, "[" + oldPrj.CTProjectName + "]Total files need to upload:" + totalUploadFiles);
                foreach (ProviderProjectFile prjFile in needUploadFiles)
                {
                    try
                    {
                        String willpassFileName = prjFile.WordbeeFileName;
                        String cmsJobName = prjFile.CmsJobName;
                        if (String.IsNullOrEmpty(cmsJobName))
                            cmsJobName = oldPrj.CmsJobName;

                        if (!String.IsNullOrEmpty(wbApi_FolderFormat))
                        {
                            /*
                             supported format here:
                               "/yyyy/"                     ==== file in workbee looks like :  /project_A/2014/file1.xml 
                               "/mmm/"                      ==== file in workbee looks like :  /project_A/Oct/file1.xml
                               "/cms_job_name/"             ==== file in workbee looks like :  /project_A/job_market_report_translation_10_files/file1.xml
       
                               "/yyyy/cms_job_name/"        ==== file in workbee looks like :  /project_A/2014/job_market_report_translation_10_files/file1.xml
                               "/mmm/cms_job_name/"         ==== file in workbee looks like :  /project_A/Oct/job_market_report_translation_10_files/file1.xml
                               "/yyyy/mmm/"                 ==== file in workbee looks like :  /project_A/2014/Oct/file1.xml
       
                               "/yyyy/mmm/cms_job_name/"   ==== file in workbee looks like :  /project_A/2014/Oct/job_market_report_translation_10_files/file1.xml 
                             */
                            if (wbApi_FolderFormat.Equals("/yyyy/"))
                                willpassFileName = DateTime.Now.ToString(@"yyyy") + @"\" + prjFile.WordbeeFileName;
                            else if (wbApi_FolderFormat.Equals("/mmm/"))
                                willpassFileName = DateTime.Now.ToString(@"MMM") + @"\" + prjFile.WordbeeFileName;
                            else if (wbApi_FolderFormat.Equals("/cms_job_name/"))
                                willpassFileName = FormatUtil.MakeValidFolderName(cmsJobName) + @"\" + prjFile.WordbeeFileName;
                            else if (wbApi_FolderFormat.Equals("/yyyy/cms_job_name/"))
                                willpassFileName = DateTime.Now.ToString(@"yyyy") + @"\" + FormatUtil.MakeValidFolderName(cmsJobName) + @"\" + prjFile.WordbeeFileName;
                            else if (wbApi_FolderFormat.Equals("/mmm/cms_job_name/"))
                                willpassFileName = DateTime.Now.ToString(@"MMM") + @"\" + FormatUtil.MakeValidFolderName(cmsJobName) + @"\" + prjFile.WordbeeFileName;
                            else if (wbApi_FolderFormat.Equals("/yyyy/mmm/"))
                                willpassFileName = DateTime.Now.ToString(@"yyyy") + @"\" + DateTime.Now.ToString(@"MMM") + @"\" + prjFile.WordbeeFileName;
                            else if (wbApi_FolderFormat.Equals("/yyyy/mmm/cms_job_name/"))
                                willpassFileName = DateTime.Now.ToString(@"yyyy") + @"\" + DateTime.Now.ToString(@"MMM") + @"\" + FormatUtil.MakeValidFolderName(cmsJobName) + @"\" + prjFile.WordbeeFileName;
  
                        }


                        bool uploaded = wsClient.UploadFileToProject(oldPrj.WordbeeProjectId, prjFile.SourceFilePath,
                            willpassFileName, prjFile.SourceCttLanguageCode);
                        if (uploaded)
                        {
                            CLogger.WriteLog(ELogLevel.DEBUG, "Uploaded file:" + prjFile.SourceFilePath + "(" + prjFile.WordbeeFileName + ")");

                            totalUploadFiles--;
                            prjFile.WordbeeFullFileName = willpassFileName;
                            prjFile.IsUploaded = true;
                            prjFile.UploadedDate = DateTime.Now;
                            JobHelper.Instance.SaveProviderProjectFile(prjFile);
                            needStartTransFiles.Add(prjFile);

                            CLogger.WriteLog(ELogLevel.DEBUG, "Need to send out AcceptAssetTask event");

                            // send AcceptAssetTask event out to notify CT 2.0 platform 
                            AcceptAssetTask acceptEvent = new AcceptAssetTask();
                            acceptEvent.setAssetTaskId(prjFile.CTAssetTaskId);
                            acceptEvent.setAssetTaskNativeId("");
                            acceptEvent.setEventId(IdGenerator.createId());

                            //call ProviderSender to sent event
                            sender.sendEvent(acceptEvent);
                            CLogger.WriteLog(ELogLevel.DEBUG, "Sent out AcceptAssetTask event.");

                            try
                            {
                                com.claytablet.model.Event.provider.UpdateAssetTaskState stAsset = new UpdateAssetTaskState();
                                stAsset.setAssetTaskId(prjFile.CTAssetTaskId);
                                stAsset.setTranslationPercentage(10);
                                stAsset.setNativeState("In Translation");                                 
                                sender.sendEvent(stAsset);

                                CLogger.WriteLog(ELogLevel.DEBUG, "Sent out UpdateAssetTaskState event.");
                            }
                            catch (Exception e) {
                                CLogger.WriteLog(ELogLevel.ERROR, "Failed to sent out UpdateAssetTaskState event.\nError:" + e.Message);
                                CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                            }

                        }
                        else
                        {
                            String errorMsg = String.Format("Failed to upload file:{0} ({1}) to Wordbee project:{2}.\nError:{3}",
                                prjFile.SourceFilePath, prjFile.WordbeeFileName, oldPrj.WordbeeProjectReference, wsClient.Error);
                            CLogger.WriteLog(ELogLevel.ERROR, errorMsg);

                            String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                            CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                             String lastError = prjFile.LastError;
                             if (errorMsg.Equals(lastError))
                             {
                             }
                             else
                             {
                                 prjFile.LastError = errorMsg;
                                 JobHelper.Instance.SaveProviderProjectFile(prjFile);

                                 if (needEmailNotification)
                                 {
                                     SendNotificationEmail(fromEmail, toEmails, "Failed to upload file to Wordbee project.", errorMsg + "\n\n\n" + traceMsg, false,
                                      smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                     CLogger.WriteLog(ELogLevel.INFO, "Send error message to [" + toEmailsString + "]");
                                 }
                             }

                        }
                    }
                    catch (Exception e)
                    { 

                        String errorMsg = String.Format("Failed to upload file:{0} ({1}) to Wordbee project:{2}.\nError:{3}",
                            prjFile.SourceFilePath, prjFile.WordbeeFileName, oldPrj.WordbeeProjectReference, e.Message);
                        CLogger.WriteLog(ELogLevel.ERROR, errorMsg);
                        CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);

                        String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                        CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                        String lastError = prjFile.LastError;
                        if (errorMsg.Equals(lastError))
                        {
                        }
                        else
                        {
                            prjFile.LastError = errorMsg;
                            JobHelper.Instance.SaveProviderProjectFile(prjFile);
                            if (needEmailNotification)
                            {
                                SendNotificationEmail(fromEmail, toEmails, "Failed to upload file to Wordbee project.", errorMsg + "\n\n\n" + traceMsg, false,
                                 smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                CLogger.WriteLog(ELogLevel.INFO, "Send error message to [" + toEmailsString + "]");
                            }
                        }

                    }
                }
                CLogger.WriteLog(ELogLevel.DEBUG, "[" + oldPrj.CTProjectName + "]Now, Total files need to upload:" + totalUploadFiles);

                int totalStartTransFiles = needStartTransFiles.Count;
                CLogger.WriteLog(ELogLevel.DEBUG, "[" + oldPrj.CTProjectName + "] Total files need to start translation:" + totalStartTransFiles);
                foreach (ProviderProjectFile prjFile in needStartTransFiles)
                {
                    try
                    {
                        List<string> ctTargetLngCodes = new List<string>();
                        ctTargetLngCodes.Add(prjFile.TargetCttLanguageCode);

                        bool startTrans = wsClient.StartTranslatingProjectFile(oldPrj.WordbeeProjectId, prjFile.WordbeeFullFileName,
                               prjFile.SourceCttLanguageCode, ctTargetLngCodes,
                               prjFile.WordbeeParserDomain, prjFile.WordbeeParserConfig, prjFile.WordbeeWorkflow);


                        if (!startTrans)
                        {
                            String errorMsg = "Failed to Start translation for file:" + prjFile.SourceFilePath + "(" + prjFile.WordbeeFileName + "). \nError:" + wsClient.Error;
                            CLogger.WriteLog(ELogLevel.ERROR, errorMsg);

                            totalStartTransFiles--;
                            prjFile.LastError = errorMsg;
                            prjFile.IsTransStarted = true;
                            prjFile.StartTransDate = DateTime.Now;

                            JobHelper.Instance.SaveProviderProjectFile(prjFile);
                            CLogger.WriteLog(ELogLevel.DEBUG, "Failed but still treated as: Started translation for file:" + prjFile.SourceFilePath + "(" + prjFile.WordbeeFileName + ")");

                            String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                            CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                            try
                            {
                                if (needEmailNotification)
                                {
                                    SendNotificationEmail(fromEmail, toEmails, "Failed to start translation for file.", errorMsg + "\n\n\n" + traceMsg, false,
                                     smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                    CLogger.WriteLog(ELogLevel.DEBUG, "Send error message to [" + toEmailsString + "]");
                                }
                            }
                            catch (Exception e)
                            {
                                CLogger.WriteLog(ELogLevel.DEBUG, "Failed to send email notification", e);
                            }

                        }
                        else
                        {

                            totalStartTransFiles--;
                            prjFile.IsTransStarted = true;
                            prjFile.StartTransDate = DateTime.Now;

                            JobHelper.Instance.SaveProviderProjectFile(prjFile);
                            CLogger.WriteLog(ELogLevel.DEBUG, "Started translation for file:" + prjFile.SourceFilePath + "(" + prjFile.WordbeeFileName + ")");

                        }


                        /*
                        if (startTrans)
                        {
                            totalStartTransFiles--;
                            prjFile.IsTransStarted = true;
                            prjFile.StartTransDate = DateTime.Now;

                            JobHelper.Instance.SaveProviderProjectFile(prjFile);
                            CLogger.WriteLog(ELogLevel.INFO, "Started translation for file:" + prjFile.SourceFilePath + "(" + prjFile.WordbeeFileName + ")");
                         

                        }
                        else
                        {
                             String errorMsg = "Failed to Start translation for file:" + prjFile.SourceFilePath + "(" + prjFile.WordbeeFileName + "). \nError:" + wsClient.Error;
                            CLogger.WriteLog(ELogLevel.ERROR, errorMsg);
                           String lastError = prjFile.LastError;
                           if (errorMsg.Equals(lastError))
                           {
                           }
                           else
                           {
                               prjFile.LastError = errorMsg;
                               JobHelper.Instance.SaveProviderProjectFile(prjFile);
                               if (needEmailNotification)
                               {
                                   SendNotificationEmail(fromEmail, toEmails, "Failed to start translation for file.", errorMsg, false,
                                    smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                   CLogger.WriteLog(ELogLevel.INFO, "Send error message to [" + toEmailsString + "]");
                               }
                           }
                         }
                         */


                    }
                    catch (Exception e)
                    {
                        
                        String errorMsg = "Failed to start translation for file:" + prjFile.SourceFilePath + "(" + prjFile.WordbeeFileName + "). \nError:" + e.Message + "\nStackTrace:" + e.StackTrace;
                        CLogger.WriteLog(ELogLevel.ERROR, errorMsg);
                        CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                        CLogger.WriteLog(ELogLevel.ERROR, "API call method: " + wsClient.getLatestAPIMethod() );
                        CLogger.WriteLog(ELogLevel.ERROR, "API call URL: " + wsClient.getLatestAPIURL());
                        if (wsClient.IsLatestResponseGot())
                        CLogger.WriteLog(ELogLevel.ERROR, "API call response: " + wsClient.getLatestAPIXML());  

                        String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                        CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                        String lastError = prjFile.LastError;
                        if (errorMsg.Equals(lastError))
                        {
                        }
                        else
                        {
                            prjFile.LastError = errorMsg;
                            JobHelper.Instance.SaveProviderProjectFile(prjFile);
                            if (needEmailNotification)
                            {
                                SendNotificationEmail(fromEmail, toEmails, "Failed to start translation for file.", errorMsg + "\n\n\n" + traceMsg, false,
                                 smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                CLogger.WriteLog(ELogLevel.DEBUG, "Send error message to [" + toEmailsString + "]");
                            }
                        }
                    }
                }
                CLogger.WriteLog(ELogLevel.DEBUG, "[" + oldPrj.CTProjectName + "]Now, Total files need to start translation:" + totalStartTransFiles);

                // load back again since some file had been chnaged.
                allProjectFiles = JobHelper.Instance.ListProviderProjectFiles(oldPrj.CTProjectId);
                List<ProviderProjectFile> needDocIdProjectFiles = new List<ProviderProjectFile>(); 
                foreach (ProviderProjectFile pFile in allProjectFiles)
                {
                       // if (pFile.WordbeeDocumentId == null)
                      //  {
                            needDocIdProjectFiles.Add(pFile);
                       // }
                 }

                CLogger.WriteLog(ELogLevel.DEBUG, "[" + oldPrj.WordbeeProjectReference + "]Total files need to get doc ID:" + needDocIdProjectFiles.Count);
                if (needDocIdProjectFiles.Count > 0)
                {
                        try
                        {


                            List<TaskInfo> prjDocInfoList = wsClient.FindProjectDocInfo(Convert.ToString(oldPrj.WordbeeProjectId));
                            if (prjDocInfoList != null)
                            {
                                foreach (ProviderProjectFile pFile in needDocIdProjectFiles)
                                {
                                    foreach (TaskInfo docInfo in prjDocInfoList)
                                    {
                                        if (pFile.WordbeeFileName.Equals(docInfo.DocumentName) || pFile.WordbeeFullFileName.Equals(docInfo.DocumentName) ||
                                            docInfo.DocumentName.EndsWith(pFile.WordbeeFileName)
                                            )
                                        {
                                            pFile.WordbeeDocumentId = docInfo.BeeDocumentId;
                                            pFile.WordbeeTaskId = docInfo.TaskId;
                                            //set assigned as true if doesn't need to assign
                                            if (!pFile.WordbeeNeedAssignSupplier)
                                            {
                                                pFile.IsSupplierAssigned = true; 
                                                pFile.AssignSupplierDate = DateTime.Now;
                                            }
                                            JobHelper.Instance.SaveProviderProjectFile(pFile);
                                            CLogger.WriteLog(ELogLevel.DEBUG, "Yes. Find doc ID:" + docInfo.BeeDocumentId + " for file:" + pFile.WordbeeFileName);
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                

                                String errorMsg = String.Format("Failed to FindProjectDocInfo for Wordbee project:{0}.\nError:{1}",
                                                                 oldPrj.WordbeeProjectReference, wsClient.Error);
                                CLogger.WriteLog(ELogLevel.ERROR, errorMsg);

                                String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                                CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                                String lastError = oldPrj.LastError;
                                if (errorMsg.Equals(lastError))
                                {
                                }
                                else
                                {
                                    oldPrj.LastError = errorMsg;
                                    JobHelper.Instance.UpdateProviderProject(oldPrj);
                                    if (needEmailNotification)
                                    {
                                        SendNotificationEmail(fromEmail, toEmails, "Failed to FindProjectDocInfo for Wordbee project.", errorMsg + "\n\n\n" + traceMsg, false,
                                         smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                        CLogger.WriteLog(ELogLevel.DEBUG, "Send error message to [" + toEmailsString + "]");
                                    }
                                }
                            }

                        }
                        catch (Exception e)
                        {
                            

                            String errorMsg = String.Format("Failed to FindProjectDocInfo for Wordbee project:{0}.\nError:{1}",
                                 oldPrj.WordbeeProjectReference, e.Message);
                            CLogger.WriteLog(ELogLevel.ERROR, errorMsg);
                            CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);

                            String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                            CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                            String lastError = oldPrj.LastError;
                            if (errorMsg.Equals(lastError))
                            {
                            }
                            else
                            {
                                oldPrj.LastError = errorMsg;
                                JobHelper.Instance.UpdateProviderProject(oldPrj);
                                 if (needEmailNotification)
                                 {
                                     SendNotificationEmail(fromEmail, toEmails, "Failed to FindProjectDocInfo for Wordbee project.", errorMsg + "\n\n\n" + traceMsg, false,
                                      smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                     CLogger.WriteLog(ELogLevel.DEBUG, "Send error message to [" + toEmailsString + "]");
                                 }
                             }
                        }

                  }

                // load back again since some file had been changed.
                allProjectFiles = JobHelper.Instance.ListProviderProjectFiles(oldPrj.CTProjectId);
                List<ProviderProjectFile> needAssignSupplierFiles = new List<ProviderProjectFile>();
                CLogger.WriteLog(ELogLevel.DEBUG, "[" + oldPrj.WordbeeProjectReference + "]Check files to find is there any file translation need to assign supplier");
                foreach (ProviderProjectFile prjFile in allProjectFiles)
                {
                    if (prjFile.IsUploaded && prjFile.IsTransStarted && !prjFile.IsSupplierAssigned &&
                        prjFile.WordbeeNeedAssignSupplier && prjFile.WordbeeDocumentId != null &&
                        prjFile.WordbeeTaskId != null)
                    {
                        needAssignSupplierFiles.Add(prjFile);
                    }
 
                }
                CLogger.WriteLog(ELogLevel.DEBUG, "[" + oldPrj.WordbeeProjectReference + "]Total files need to Assign supplier:" + needDocIdProjectFiles.Count);
                if (needAssignSupplierFiles.Count > 0)
                {

                    try
                    {


                        List<TaskInfo> prjDocInfoList = wsClient.FindUnAssignedTasks(Convert.ToString(oldPrj.WordbeeProjectId));
                        if (prjDocInfoList != null)
                        {
                            CLogger.WriteLog(ELogLevel.DEBUG, "Wordbee reported there are " + prjDocInfoList.Count + "  files need to Assign supplier.  This number maybe not accurate since API call may fail.");
                            foreach (TaskInfo docInfo in prjDocInfoList)
                            {
                                CLogger.WriteLog(ELogLevel.DEBUG, "need to assign supplier for document: " + docInfo.DocumentName);
                            
                                ProviderProjectFile relatedFile = null;
                                foreach (ProviderProjectFile pFile in needDocIdProjectFiles)
                                {

                                    if (docInfo.BeeDocumentId.Equals(pFile.WordbeeDocumentId) &&
                                        docInfo.TaskId.Equals(pFile.WordbeeTaskId)
                                        )
                                    {
                                        relatedFile = pFile;
                                        break;
                                    } 
                                }

                                if (relatedFile != null)
                                {
                                    CLogger.WriteLog(ELogLevel.DEBUG, "find related file with Supplier Company ID:" + relatedFile.WordbeeSupplierCompanyID + " and PersonID:" + relatedFile.WordbeeSupplierPersonID);
                                    try
                                    {
                                        wsClient.AssignTaskSupplier(Convert.ToString(oldPrj.WordbeeProjectId), relatedFile.WordbeeDocumentId,
                                            relatedFile.WordbeeTaskId, relatedFile.WordbeeSupplierCompanyID,
                                            relatedFile.WordbeeSupplierPersonID);

                                        relatedFile.IsSupplierAssigned = true;
                                        relatedFile.AssignSupplierDate = DateTime.Now;
                                        JobHelper.Instance.SaveProviderProjectFile(relatedFile);
                                        CLogger.WriteLog(ELogLevel.ERROR, "Yes, Did assign supplier"); 

                                    }
                                    catch (Exception ase)
                                    {
                                        CLogger.WriteLog(ELogLevel.ERROR, "Failed to find assign supplier. Error:" + ase.Message);
                                        CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", ase);
                                    }
                                }
                                else
                                {
                                    CLogger.WriteLog(ELogLevel.ERROR, "Failed to find related file with Supplier Company ID:" + relatedFile.WordbeeSupplierCompanyID + " and PersonID:" + relatedFile.WordbeeSupplierPersonID);
                                }
                             }//each unassigntask
                            
                        }
                        else
                        {

                            String errorMsg = String.Format("Failed to find unAssignTasks for NK project:{0}, Wordbee project:{1}.\nError:{2}",
                             oldPrj.WordbeeProjectReference, oldPrj.WordbeeAutoGenProjectName, wsClient.Error);
                            CLogger.WriteLog(ELogLevel.ERROR, errorMsg);

                            String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                            CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                            if (needEmailNotification)
                                 {
                                     SendNotificationEmail(fromEmail, toEmails, "Failed to find unAssignTasks for Wordbee project.", errorMsg + "\n\n\n" + traceMsg, false,
                                      smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                     CLogger.WriteLog(ELogLevel.INFO, "Send error message to [" + toEmailsString + "]");
                                 }
                             
                        }

                    }
                    catch (Exception e)
                    {
                        CLogger.WriteLog(ELogLevel.ERROR, String.Format("Failed to find unAssignTasks for NK project:{0}, Wordbee project:{1}.\nError:{2}",
                             oldPrj.WordbeeProjectReference, oldPrj.WordbeeAutoGenProjectName, e.Message));
                        CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                    }


                }
               
                // load back again since some file had been changed.
                allProjectFiles = JobHelper.Instance.ListProviderProjectFiles(oldPrj.CTProjectId);
                List<ProviderProjectFile> needCheckProcessDoneFiles = new List<ProviderProjectFile>();
                List<ProviderProjectFile> needCheckProcessDoneFiles_workflowCompleted = new List<ProviderProjectFile>();
                List<ProviderProjectFile> needCheckProcessDoneFiles_flagTag = new List<ProviderProjectFile>();
                foreach (ProviderProjectFile prjFile in allProjectFiles)
                {
                    if (prjFile.IsUploaded &&
                        (prjFile.IsTransStarted || !prjFile.IsTransStarted) &&
                        (prjFile.IsSupplierAssigned || !prjFile.IsSupplierAssigned ) &&
                        !prjFile.IsTransProcessDone &&
                        !prjFile.IsSentback
                        )
                    {
                        needCheckProcessDoneFiles.Add(prjFile); 
                    }

                }


                CLogger.WriteLog(ELogLevel.DEBUG, "[" + oldPrj.WordbeeProjectReference + "]Total files need to check if translation process is done:" + needCheckProcessDoneFiles.Count);
                if (needCheckProcessDoneFiles.Count > 0)
                {

                    List<String> checkTars_LabelFlagged = new List<string>();
                    List<String> checkLabelFlagged_DbCodes = new List<string>();
                    List<String> checkTars_WorkflowCompleted = new List<string>();
                    bool  isInTars = false;
                    bool isInDBCodes = false;
                    int total_LabelFlagged = 0;
                    int total_WorkflowCompleted = 0;
                    foreach (ProviderProjectFile pFile in needCheckProcessDoneFiles)
                    {
                        if (pFile.WordbeePickupType.Equals("WorkflowCompleted"))
                        {

                            total_WorkflowCompleted++;
                            needCheckProcessDoneFiles_workflowCompleted.Add(pFile);
                            isInTars = false;
                            foreach (String hasTar in checkTars_WorkflowCompleted)
                            {
                                if (hasTar.Equals(pFile.TargetCttLanguageCode))
                                {
                                    isInTars = true;
                                    break;
                                }
                            }

                            if (!isInTars)
                                checkTars_WorkflowCompleted.Add(pFile.TargetCttLanguageCode);
                        }
                        else
                        {
                            total_LabelFlagged++;
                            needCheckProcessDoneFiles_flagTag.Add(pFile);
                            isInTars = false;
                            foreach (String hasTar in checkTars_LabelFlagged)
                            {
                                if (hasTar.Equals(pFile.TargetCttLanguageCode))
                                {
                                    isInTars = true;
                                    break;
                                }
                            }

                            if (!isInTars)
                            {
                                checkTars_LabelFlagged.Add(pFile.TargetCttLanguageCode);
                            }

                            if (!String.IsNullOrEmpty(pFile.WordbeeCustomLabelDBCode))
                            {
                                isInDBCodes = false;
                                foreach (String hasDBCode in checkLabelFlagged_DbCodes)
                                {
                                    if (hasDBCode.Equals(pFile.WordbeeCustomLabelDBCode))
                                    {
                                        isInDBCodes = true;
                                        break;
                                    }
                                }

                                if (!isInDBCodes)
                                {
                                    checkLabelFlagged_DbCodes.Add(pFile.WordbeeCustomLabelDBCode);
                                }
                            }

                        }

                    }
                    CLogger.WriteLog(ELogLevel.DEBUG, "[" + oldPrj.WordbeeProjectReference + "]Total " + total_WorkflowCompleted + " file(s) need to check for CompletedWorkflow");
                    String dbCodes_Check = "{";
                    int dbCnt = 0;
                    foreach (String dbcode in checkLabelFlagged_DbCodes)
                    {
                        dbCnt++;
                        if (dbCnt == 0)
                            dbCodes_Check += "'" + dbcode + "'";
                        else
                            dbCodes_Check += ",'" + dbcode + "'";
                    }
                    dbCodes_Check += "}";
                    //&filter= � AND (Labels.Contains(�{label db code}�)
                    String filter = "Labels.Contains(�" + dbCodes_Check + "�)";
                    foreach (String tarLngCode in checkTars_WorkflowCompleted)
                    {
                        try
                        {

                            CLogger.WriteLog(ELogLevel.DEBUG, "Check the workflow completed status for Target:[" + tarLngCode + "] for WB project:" + oldPrj.WordbeeProjectId); 

                          
                            //List<DocumentInfo> prjDocInfoList = wsClient.getDocsWithCompletedWorkflows(Convert.ToString(oldPrj.WordbeeProjectId), 
                            //    oldPrj.CTSourceLanguage, tarLngCode, oldPrj.WordbeeProjectCreationDate, oldPrj.CTReceivedAssetTasks);


                            List<DocumentInfo> prjDocInfoList = wsClient.getDocsWithCompletedWorkflows(Convert.ToString(oldPrj.WordbeeProjectId),
                                oldPrj.CTSourceLanguage, tarLngCode, oldPrj.WordbeeProjectCreationDate, 0);

                            if (prjDocInfoList != null)
                            {
                                foreach (ProviderProjectFile pFile in needCheckProcessDoneFiles_workflowCompleted)
                                {
                                    foreach (DocumentInfo docInfo in prjDocInfoList)
                                    {
                                        if (pFile.WordbeeDocumentId != null && pFile.WordbeeFileName != null)
                                        if (pFile.WordbeeDocumentId.Equals(docInfo.BeeDocumentId) ||
                                            pFile.WordbeeFileName.Equals(docInfo.DocumentName)
                                            )
                                        {
                                            pFile.IsTransStarted = true;
                                            pFile.IsSupplierAssigned = true;
                                            pFile.IsTransProcessDone = true;  
                                            pFile.IsTransFileBuild = true;
                                            JobHelper.Instance.SaveProviderProjectFile(pFile);
                                            CLogger.WriteLog(ELogLevel.DEBUG, "Yes. Find completed workflow for file:" + pFile.WordbeeFileName); 

                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                CLogger.WriteLog(ELogLevel.ERROR, String.Format("Failed to getDocsWithCompletedWorkflows (Target:{0}) for NK project:{1}, WB project:{2}.\nError:{3}",
                                                                 tarLngCode, oldPrj.WordbeeProjectReference, oldPrj.WordbeeAutoGenProjectName, wsClient.Error));
                            }

                            /*
                            foreach (ProviderProjectFile pFile in needCheckProcessDoneFiles_workflowCompleted)
                            {
                                try
                                {
                                    String wbfilter = "BeeDocumentId=" + pFile.WordbeeDocumentId;
                                    List<DocumentInfo> prjDocInfoList = wsClient.getDocsWithCompletedWorkflows(Convert.ToString(oldPrj.WordbeeProjectId),
                                    oldPrj.CTSourceLanguage, tarLngCode, oldPrj.WordbeeProjectCreationDate, oldPrj.CTReceivedAssetTasks, wbfilter);

                                    foreach (DocumentInfo docInfo in prjDocInfoList)
                                    {
                                        if (pFile.WordbeeDocumentId != null && pFile.WordbeeFileName != null)
                                            if (pFile.WordbeeDocumentId.Equals(docInfo.BeeDocumentId) ||
                                                pFile.WordbeeFileName.Equals(docInfo.DocumentName)
                                                )
                                            {
                                                pFile.IsTransStarted = true;
                                                pFile.IsSupplierAssigned = true;
                                                pFile.IsTransProcessDone = true;
                                                pFile.IsTransFileBuild = true;
                                                JobHelper.Instance.SaveProviderProjectFile(pFile);
                                                CLogger.WriteLog(ELogLevel.DEBUG, "Yes. Find completed workflow for file:" + pFile.WordbeeFileName);

                                                break;
                                            }
                                    }
                                catch (Exception e)
                                  { }
                            } 
                               
                            */

                        }
                        catch (Exception e)
                        {
                            String lastError = oldPrj.LastError;
                            if (lastError == null)
                                lastError = "";

                            String errorMsg = String.Format("Failed to getDocsWithCompletedWorkflows (Target:{0}) for NK project:{1}, WB project:{2}.\nError:{3}",
                                 tarLngCode, oldPrj.WordbeeProjectReference, oldPrj.WordbeeAutoGenProjectName, e.Message);
                            CLogger.WriteLog(ELogLevel.ERROR, errorMsg);
                            CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);

                            String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                            CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                            if (errorMsg.Equals(lastError))
                            {
                            }
                            else
                            {

                                oldPrj.LastError = errorMsg;
                                JobHelper.Instance.UpdateProviderProject(oldPrj);

                                if (needEmailNotification)
                                {
                                    SendNotificationEmail(fromEmail, toEmails, "Failed to getDocsWithCompletedWorkflows.", errorMsg + "\n\n\n" + traceMsg, false,
                                     smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                    CLogger.WriteLog(ELogLevel.DEBUG, "Send error message to [" + toEmailsString + "]");
                                }

                            }
                              
                        }

                    }

                    CLogger.WriteLog(ELogLevel.DEBUG, "[" + oldPrj.WordbeeProjectReference + "]Total " + total_LabelFlagged + " file(s) need to check for LabelFlagged");
                    foreach (String tarLngCode in checkTars_LabelFlagged)
                    {
                        try
                        { 

                            List<String> prjDocIDs = wsClient.FindFlaggedFiles(Convert.ToString(oldPrj.WordbeeProjectId),
                                oldPrj.CTSourceLanguage, tarLngCode, oldPrj.WordbeeProjectCreationDate,
                                oldPrj.CTReceivedAssetTasks, filter);

                            if (prjDocIDs != null)
                            {
                                foreach (ProviderProjectFile pFile in needCheckProcessDoneFiles_flagTag)
                                {
                                    foreach (String docId in prjDocIDs)
                                    {
                                        if ( pFile.WordbeeDocumentId.Equals(docId)  )
                                        {
                                            pFile.IsTransStarted = true;
                                            pFile.IsSupplierAssigned = true;
                                            pFile.IsTransProcessDone = true;
                                            //also set TransFileBuild as true since we need to skip the BuildTranstedFile step
                                            pFile.IsTransFileBuild = true;
                                            JobHelper.Instance.SaveProviderProjectFile(pFile);

                                            CLogger.WriteLog(ELogLevel.DEBUG, "Yes. Find Label Flagged File:" + pFile.WordbeeFileName);
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                CLogger.WriteLog(ELogLevel.ERROR, String.Format("Failed to FindFlaggedFiles (Target:{0}) for NK project:{1}, Wordbee project:{2}.\nError:{3}",
                                                                 tarLngCode, oldPrj.WordbeeProjectReference, oldPrj.WordbeeAutoGenProjectName, wsClient.Error));
                            }

                        }
                        catch (Exception e)
                        {
                            String errorMsg = String.Format("Failed to FindFlaggedFiles (Target:{0}) for NK project:{1}, Wordbee project:{2}.\nError:{3}",
                                 tarLngCode, oldPrj.WordbeeProjectReference, oldPrj.WordbeeAutoGenProjectName, e.Message);
                            CLogger.WriteLog(ELogLevel.ERROR, errorMsg);
                            CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);

                            String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                            CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                            if (needEmailNotification)
                            {
                                SendNotificationEmail(fromEmail, toEmails, "Failed to FindFlaggedFiles.", errorMsg + "\n\n\n" + traceMsg, false,
                                 smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                CLogger.WriteLog(ELogLevel.INFO, "Send error message to [" + toEmailsString + "]");
                            }
                        }

                    }
                }

                /*
                // need to call API to build translated file
                
                allProjectFiles = JobHelper.Instance.ListProviderProjectFiles(oldPrj.CTProjectId);
                List<ProviderProjectFile> needBuildTranslatedFiles = new List<ProviderProjectFile>();
                foreach (ProviderProjectFile prjFile in allProjectFiles)
                {
                    if (prjFile.IsUploaded &&
                        prjFile.IsTransStarted &&
                        prjFile.IsSupplierAssigned &&
                        prjFile.IsTransProcessDone &&
                        !prjFile.IsTransFileBuild
                        )
                    {
                        needBuildTranslatedFiles.Add(prjFile);
                    }

                }

                CLogger.WriteLog(ELogLevel.INFO, "[" + oldPrj.WordbeeProjectReference + "] Total " + needBuildTranslatedFiles.Count + " file(s) need to automatically build target file.");
                foreach (ProviderProjectFile pFile in needBuildTranslatedFiles)
                {
                    CLogger.WriteLog(ELogLevel.INFO, "Need to [Automatically build translated file] for file:" + pFile.WordbeeFileName);
                    try
                    {
                        wsClient.BuildTranslatedFile(Convert.ToString(oldPrj.WordbeeProjectId), pFile.SourceCttLanguageCode, pFile.TargetCttLanguageCode, pFile.WordbeeFileName);
                        pFile.IsTransFileBuild = true;
                        JobHelper.Instance.SaveProviderProjectFile(pFile);
                        CLogger.WriteLog(ELogLevel.INFO, "Success did [Automatically build translated file] for file:" + pFile.WordbeeFileName);    
                    }
                    catch (Exception bfe)
                    {
                        
                        String errorMsg = "Failed to [Automatically build translated file] for file:" + pFile.WordbeeFileName + "\nError:" + bfe.Message;
                        CLogger.WriteLog(ELogLevel.ERROR, errorMsg);

                         String lastError = pFile.LastError;
                         if (errorMsg.Equals(lastError))
                         {
                         }
                         else
                         {
                             pFile.LastError = errorMsg;
                             JobHelper.Instance.SaveProviderProjectFile(pFile);
                             if (needEmailNotification)
                             {
                                 SendNotificationEmail(fromEmail, toEmails, "Failed to [Automatically build translated file] for file.", errorMsg, false,
                                  smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                 CLogger.WriteLog(ELogLevel.INFO, "Send error message to [" + toEmailsString + "]");
                             }

                         }

                    }

                } 
                */

                // load back again since some file had been changed.
                allProjectFiles = JobHelper.Instance.ListProviderProjectFiles(oldPrj.CTProjectId); 
                List<ProviderProjectFile> needCheckDownloadFiles = new List<ProviderProjectFile>();

                //pFile.IsTransProcessDone = true;  
                //pFile.IsTransFileBuild = true;

                foreach (ProviderProjectFile prjFile in allProjectFiles)
                {
                    if (prjFile.IsUploaded &&
                        prjFile.IsTransStarted  &&
                        prjFile.IsSupplierAssigned  &&
                        (prjFile.IsTransProcessDone || allowPickupTranslationEvenWorkflowNotCompleted )&&
                        (prjFile.IsTransFileBuild || allowPickupTranslationEvenWorkflowNotCompleted ) &&
                        !prjFile.IsTargetFileDownloaded && 
                        !prjFile.IsSentback
                        )
                    {
                        needCheckDownloadFiles.Add(prjFile);
                    }

                }

                CLogger.WriteLog(ELogLevel.DEBUG, "[" + oldPrj.WordbeeProjectReference + "] Total " + needCheckDownloadFiles.Count + " file(s) need to download");
                foreach (ProviderProjectFile prjFile in needCheckDownloadFiles)
                {
                    String targetfilePathinWB = wsClient.getTranslatedFilePath(Convert.ToString(oldPrj.WordbeeProjectId),
                         prjFile.WordbeeFileName, prjFile.TargetCttLanguageCode);

                    if (targetfilePathinWB == null)
                        targetfilePathinWB = wsClient.getTranslatedFilePath2(Convert.ToString(oldPrj.WordbeeProjectId),
                         prjFile.WordbeeFileName, prjFile.TargetCttLanguageCode);

                    if (targetfilePathinWB == null)
                        targetfilePathinWB = wsClient.getTranslatedFilePath(Convert.ToString(oldPrj.WordbeeProjectId),
                         prjFile.WordbeeFullFileName, prjFile.TargetCttLanguageCode);

                    if (targetfilePathinWB == null)
                        CLogger.WriteLog(ELogLevel.DEBUG, "Can't find translated file:" + prjFile.WordbeeFileName);
                    else
                    {
                        CLogger.WriteLog(ELogLevel.DEBUG, "found translated file path:" + targetfilePathinWB);
                        String userTargetFileName = Path.GetFileName(targetfilePathinWB);
                        userTargetFileName = Regex.Replace(userTargetFileName.Trim(), "[^A-Za-z0-9_. ]+", "_");

                        string saveAsFilePath = JobHelper.Instance.GetProviderProjectTargetFolder(oldPrj.CTProjectId) + @"\" + userTargetFileName;
                        saveAsFilePath = saveAsFilePath.Replace(@"\\", @"\");

                        try
                        {
                            CLogger.WriteLog(ELogLevel.DEBUG, "Need download translated file:" + targetfilePathinWB + "\nAnd save as: " + saveAsFilePath);
                            wsClient.SaveTranslatedFile(Convert.ToString(oldPrj.WordbeeProjectId), prjFile.TargetCttLanguageCode, targetfilePathinWB, saveAsFilePath);
                            prjFile.IsSupplierAssigned = true;
                            prjFile.IsTransStarted = true;
                            prjFile.IsTransProcessDone = true;
                            prjFile.IsTransFileBuild = true;
                            prjFile.IsTargetFileDownloaded = true;
                            prjFile.TargetFilePath = saveAsFilePath;

                            if (File.Exists(saveAsFilePath))
                            {

                                JobHelper.Instance.SaveProviderProjectFile(prjFile);
                                CLogger.WriteLog(ELogLevel.DEBUG, "Success download file:" + prjFile.WordbeeFileName + "\nSaved as: " + saveAsFilePath);
                            }
                            else
                            {
                                prjFile.IsTargetFileDownloaded = false;
                                prjFile.TargetFilePath = "";
                                String errorMsg = "Failed to download translated file:" + targetfilePathinWB;
                                prjFile.LastError = errorMsg;
                                JobHelper.Instance.SaveProviderProjectFile(prjFile);
                            }
                        }
                        catch (Exception de)
                        {

                            String errorMsg = "Failed to download translated file:" + prjFile.WordbeeFileName + "\nError:" + de.Message;
                            CLogger.WriteLog(ELogLevel.ERROR, errorMsg);
                            CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", de);

                            String traceMsg = TraceRoute.getTraceRouteInfo("api.wordbee-translator.com", 20, 1000);
                            CLogger.WriteLog(ELogLevel.ERROR, traceMsg);

                            String lastError = prjFile.LastError;
                            if (errorMsg.Equals(lastError))
                            {
                            }
                            else
                            {
                                prjFile.LastError = errorMsg;
                                JobHelper.Instance.SaveProviderProjectFile(prjFile);
                                if (needEmailNotification)
                                {
                                    SendNotificationEmail(fromEmail, toEmails, "Failed to download translated file.", errorMsg + "\n\n\n" + traceMsg, false,
                                     smtpServer, smtpPort, smtpEnableSSL, smtpUser, smtpPassword);
                                    CLogger.WriteLog(ELogLevel.DEBUG, "Send error message to [" + toEmailsString + "]");
                                }
                            }

                        }
                    }
                }

                // load back again since some file had been changed.
                allProjectFiles = JobHelper.Instance.ListProviderProjectFiles(oldPrj.CTProjectId);
                List<ProviderProjectFile> needSendbackFiles = new List<ProviderProjectFile>();
                foreach (ProviderProjectFile prjFile in allProjectFiles)
                {
                    if (prjFile.IsUploaded &&
                        prjFile.IsTransStarted &&
                        prjFile.IsSupplierAssigned &&
                        prjFile.IsTransProcessDone &&
                        prjFile.IsTransFileBuild &&
                        prjFile.IsTargetFileDownloaded &&
                        !prjFile.IsSentback
                        )
                    {
                        needSendbackFiles.Add(prjFile);
                    }

                }

                CLogger.WriteLog(ELogLevel.DEBUG, "[" + oldPrj.WordbeeProjectReference + "] Total " + needSendbackFiles.Count + " file(s) need to send back.");
                foreach (ProviderProjectFile prjFile in needSendbackFiles)
                {

                    try
                    {

                        SubmitAssetTask submitAssetTask = new SubmitAssetTask();
                        submitAssetTask.setEventId(IdGenerator.createId());

                        //set original CT2 AssetTaskId
                        submitAssetTask.setAssetTaskId(prjFile.CTAssetTaskId);
                        submitAssetTask.setNativeState("Completed");
                        submitAssetTask.setFileExt(prjFile.FileExt);

                        sender.sendEvent(submitAssetTask, prjFile.TargetFilePath);

                        prjFile.IsSentback = true;
                        prjFile.SendbackDate = DateTime.Now;

                        JobHelper.Instance.SaveProviderProjectFile(prjFile);

                        CLogger.WriteLog(ELogLevel.DEBUG, "Send out translated file: " + prjFile.TargetFilePath);
                    }
                    catch (Exception e)
                    {
                        CLogger.WriteLog(ELogLevel.ERROR, "Failed to send out translated file: " + prjFile.TargetFilePath + "\nError:" + e.Message);
                        CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                    }

                }
 
            }

            if (wsClient != null && wsClient.IsConnected)
                wsClient.Close();
             
        }

        private void SendNotificationEmail(String fromEmail, List<string> toEmails, string subject, string body, bool isHtmlFormat,
            String smtpServer, int smtpPort, bool smtpEnableSSL, String smtpUser, String smtpPassword)
        {
            try
            {

                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                mail.From = new System.Net.Mail.MailAddress(fromEmail);

                //char[] splitchar = { ',' };
                //String[] notifyEmailList = toEmails.Split(splitchar);
                //if (notifyEmailList != null && notifyEmailList.Length > 0)
                if (toEmails != null && toEmails.Count > 0)
                {
                    foreach (String emailAddress in toEmails)
                    {
                        try
                        {
                            mail.To.Add(new System.Net.Mail.MailAddress(emailAddress));
                        }
                        catch (Exception e)
                        {
                            CLogger.WriteLog(ELogLevel.ERROR, "Cannot add recipient: " + emailAddress + " error: " + e.Message);
                            CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                        }
                    }

                    mail.Subject = subject;
                    mail.Body = body;
                    mail.IsBodyHtml = isHtmlFormat;
                    System.Net.Mail.SmtpClient smtpClient;

                    if (!string.IsNullOrEmpty(smtpServer))
                    {
                        smtpClient = new System.Net.Mail.SmtpClient(
                                smtpServer, smtpPort);

                        smtpClient.EnableSsl = smtpEnableSSL;

                        if (String.IsNullOrEmpty(smtpUser))
                        {
                            smtpClient.UseDefaultCredentials = true;
                        }
                        else
                        {
                            smtpClient.UseDefaultCredentials = false;
                            smtpClient.Credentials = new System.Net.NetworkCredential(
                                    smtpUser, smtpPassword);
                        }
                    }
                    else
                    {
                        smtpClient = new System.Net.Mail.SmtpClient();
                    }

                    smtpClient.Send(mail);
                    CLogger.WriteLog(ELogLevel.INFO, "Notification email sent successfully with smtp.");
                } 
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Failed to send notification email with smtp.\nError:" + e.Message);
                CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
            }
        }

         
    }
}
