using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using ClayTablet.CT.Utility;
using ClayTablet.CT.Net.WordBee;
using System.IO;
using System.Reflection;

namespace WordBee_Provider_Service
{
    public partial class Service1 : ServiceBase
    {
        private string assembly_version;
        private string assembly_name;
        protected System.Timers.Timer timer;
        protected bool IsRunning = false;

        public Service1()
        {
            String path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            path = System.IO.Path.GetDirectoryName(path);
            Directory.SetCurrentDirectory(path);

            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            assembly_version = fvi.ProductVersion;
            assembly_name = fvi.ProductName;

            InitializeComponent();
             
            String provider_service_name = "";
            if (!String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ServiceName"]))
            {
                provider_service_name = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ServiceName"].ToString();
            }

            this.ServiceName = provider_service_name;

        } 
        
        protected override void OnStart(string[] args)
        {
            EventLog.WriteEntry(this.ServiceName + "[" + assembly_name + "-" + assembly_version + "] started.");
            CLogger.WriteLog(ELogLevel.INFO, this.ServiceName + "[" + assembly_name + "-" + assembly_version + "] started.");
            InitializeTimer();
            timer.Enabled = true; 
        }

        protected override void OnStop()
        {
            timer.Enabled = false;
            EventLog.WriteEntry(this.ServiceName + "[" + assembly_name + "-" + assembly_version + "] stopped.");
            CLogger.WriteLog(ELogLevel.INFO, this.ServiceName + "[" + assembly_name + "-" + assembly_version + "] stopped.");
        }

        protected void InitializeTimer()
        {
            if (timer == null)
            {
                timer = new System.Timers.Timer();
                timer.AutoReset = true;
                timer.Interval = 1000;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
            }
        }
        private void timer_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            if (!IsRunning)
            { 
                try
                {
 			
                    IsRunning = true;
                    timer.Enabled = false;

                    CLogger.WriteLog(ELogLevel.INFO, "Start to polling [" + assembly_name + " - " + assembly_version + "]");

                    RunCommands();

                    timer.Interval = ReadAppSettingInterval();
                    CLogger.WriteLog(ELogLevel.INFO, "Polling done, next polling in " + timer.Interval / 1000 + " seconds");
                    timer.AutoReset = true;
                    IsRunning = false;
                    timer.Enabled = true; 

                }
                catch (Exception exp)
                {
                    EventLog.WriteEntry(exp.Message);
                    CLogger.WriteLog(ELogLevel.DEBUG, "Service start error", exp);
                }
                finally
                {
                     
                    timer.Enabled = true;
                    IsRunning = false;
                }


            }
        }

        protected void RunCommands()
        {
            _ReceivingMsg();

            _ReceivingTransaltion();
        }

        private void _ReceivingMsg()
        {
            CLogger.WriteLog(ELogLevel.INFO, "Start to ReceivingMsg");
            try
            {
                ReceiverJob receiverJober = new ReceiverJob();
                receiverJober.Process();
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "ReceivingMsg Error: " + e.Message);
                CLogger.WriteLog(ELogLevel.DEBUG, "Error details: ", e);
            }
            CLogger.WriteLog(ELogLevel.INFO, "ReceivingMsg --- Done!.");
        }

        private void _ReceivingTransaltion()
        {
            CLogger.WriteLog(ELogLevel.INFO, "Start to ReceivingTranslation");
            try
            {
                ProviderSenderJob receiverJober = new ProviderSenderJob();
                receiverJober.Process();
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "ReceivingTranslation Error: " + e.Message);
                CLogger.WriteLog(ELogLevel.DEBUG, "Error details: ", e);
            }
            CLogger.WriteLog(ELogLevel.INFO, "ReceivingTranslation --- Done!");
        }


        protected int ReadAppSettingInterval()
        {

            var sleepTimeConfig = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.SleepTime"];
            try
            {
                if (sleepTimeConfig != null)
                {
                    return Convert.ToInt32(sleepTimeConfig, 10);
                }
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.WARN, 
                        "Invalid ClayTablet.Provider.SleepTime setting: " + sleepTimeConfig + ", use 30 seconds as default", e);
            }


            return 30000;
        }

    }
}
