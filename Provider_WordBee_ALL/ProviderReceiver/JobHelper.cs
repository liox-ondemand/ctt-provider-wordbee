using System;
using System.IO;
using System.Collections.Generic; 
using System.Text;
using com.claytablet.model;
using com.claytablet.provider;
using com.claytablet.model.enm;
using com.claytablet.model.Event;
using com.claytablet.model.Event.platform;
using com.claytablet.model.Event.provider;
using com.claytablet.service.Event;
using com.claytablet.util;
using com.claytablet.queue;
using com.claytablet.queue.model;
using com.claytablet.queue.service;
using com.claytablet.queue.service.sqs;
using com.claytablet.storage;
using com.claytablet.storage.service;
using com.claytablet.storage.service.s3;
using ClayTablet.CT.Utility;

namespace ClayTablet.CT.Net.WordBee
{
    public class JobHelper
    {
        // sample Mapping handling,  used local file to save info
        private String DEFAULT_ERROR_DIR = null;
        private String DEFAULT_DATA_DIR = null;
	    private String DEFAULT_DATA_DIR_JOB = null;
        private String DEFAULT_DATA_DIR_JOB_BACKUP = null;
        private String DEFAULT_DATA_DIR_JOB_BACKUP_NOTFOUND = null;
        private String def_ProjectFileName = "Project.XML";
        private String def_ProjectSourceFolderName = "Source";
        private String def_ProjectTargetFolderName = "Target";


        public JobHelper()
        {

            DEFAULT_DATA_DIR = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ContextFolder"];
             if (DEFAULT_DATA_DIR.EndsWith("\\"))
                 DEFAULT_DATA_DIR_JOB = DEFAULT_DATA_DIR + "jobs\\";
             else
                 DEFAULT_DATA_DIR_JOB = DEFAULT_DATA_DIR + "\\jobs\\";
             
			 if (!System.IO.Directory.Exists(DEFAULT_DATA_DIR_JOB ) )
                        System.IO.Directory.CreateDirectory(DEFAULT_DATA_DIR_JOB);

             if (DEFAULT_DATA_DIR.EndsWith("\\"))
             {
                 DEFAULT_DATA_DIR_JOB_BACKUP = DEFAULT_DATA_DIR + "job_Backups\\";
                 DEFAULT_DATA_DIR_JOB_BACKUP_NOTFOUND = DEFAULT_DATA_DIR + "job_NotFound_Backups\\";
             }
             else
             {
                 DEFAULT_DATA_DIR_JOB_BACKUP = DEFAULT_DATA_DIR + "\\job_Backups\\";
                 DEFAULT_DATA_DIR_JOB_BACKUP_NOTFOUND = DEFAULT_DATA_DIR + "\\job_NotFound_Backups\\";
             }

             if (!System.IO.Directory.Exists(DEFAULT_DATA_DIR_JOB_BACKUP))
                 System.IO.Directory.CreateDirectory(DEFAULT_DATA_DIR_JOB_BACKUP);

             if (!System.IO.Directory.Exists(DEFAULT_DATA_DIR_JOB_BACKUP_NOTFOUND))
                 System.IO.Directory.CreateDirectory(DEFAULT_DATA_DIR_JOB_BACKUP_NOTFOUND);


             if (DEFAULT_DATA_DIR.EndsWith("\\"))
                 DEFAULT_ERROR_DIR = DEFAULT_DATA_DIR + "Errors\\";
             else
                 DEFAULT_ERROR_DIR = DEFAULT_DATA_DIR + "\\Errors\\";

             if (!System.IO.Directory.Exists(DEFAULT_ERROR_DIR))
                 System.IO.Directory.CreateDirectory(DEFAULT_ERROR_DIR);	
			 
	    }

        public static JobHelper Instance
        {
            get
            {
                return Nested.Instance;
            }
        }

        private class Nested
        {
            private static JobHelper instance;

            static Nested()
            {
                instance = new JobHelper();
            }

            public static JobHelper Instance
            {
                get
                {
                    return instance;
                }
            }

        }

        

        private bool existsProjectSourceFolder(DirectoryInfo directory)
        {

            String folderPath = directory.FullName;
            String folderXMLPath = folderPath + @"\" + def_ProjectSourceFolderName;

            return Directory.Exists(folderXMLPath);
        }

        private bool existsProjectTargetFolder(DirectoryInfo directory)
        {

            String folderPath = directory.FullName;
            String folderXMLPath = folderPath + @"\" + def_ProjectTargetFolderName;

            return Directory.Exists(folderXMLPath);
        }

        private bool existsProjectXML(DirectoryInfo directory)
        {

            String folderPath = directory.FullName;
            String prjXMLPath = folderPath + @"\" + def_ProjectFileName;

            return File.Exists(prjXMLPath);
        }

        private List<string> getProjectFilePaths(DirectoryInfo directory)
        {
            List<string> prjFilePaths = new List<string>();

            String folderPath = directory.FullName;
            String prjXMLPath = folderPath + @"\" + def_ProjectFileName;

            String[] exts = { "xml" };
            List<String> jobFiles = FileUtil.ListFiles(folderPath, exts);
            foreach (String file in jobFiles)
            {
                if (String.Compare(file, prjXMLPath, true) != 0)
                {
                    prjFilePaths.Add(file);
                } 
            }

            return prjFilePaths;
        }

        public String getLastError()
        {
            String errorFilePath = DEFAULT_ERROR_DIR + "last.error";
            if (File.Exists(errorFilePath))
            {

                return FileUtil.ReadStringFromFile(errorFilePath);
            }
            else
                return String.Empty;

        }

        public void setLastError(String errorMsg)
        {
            String errorFilePath = DEFAULT_ERROR_DIR + "last.error";
            try
            {
                FileUtil.WriteStringToFile(errorFilePath, errorMsg);

            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.DEBUG, "Cannot write to last.error", e);
            }
        }

        public List<ProviderProject> ListProjects() 
        {
            //CLogger.WriteLog(ELogLevel.DEBUG, "ListProjects() called...");             
            List<ProviderProject> projects = new List<ProviderProject>();

            DirectoryInfo directory = new DirectoryInfo(DEFAULT_DATA_DIR_JOB);
            DirectoryInfo[] directories = directory.GetDirectories();
            for (int i = 0; i < directories.Length; i++)
            {
                if (existsProjectXML(directories[i]) &&
                    existsProjectSourceFolder(directories[i]) &&
                    existsProjectTargetFolder(directories[i])
                    )
                {
                    String folderPath = directories[i].FullName;
                    String prjXMLPath = folderPath + @"\" + def_ProjectFileName;

                    String prjXMLString = FileUtil.ReadStringFromFile(prjXMLPath);
                    try
                    {
                        ProviderProject prj = ProviderProject.fromXml(prjXMLString);
                        if (prj != null)
                        {
                            projects.Add(prj);
                        }
                    }
                    catch (Exception e)
                    {
                        CLogger.WriteLog(ELogLevel.ERROR, "failed to load ProviderProject from XML:\n" + prjXMLString + "\n\n");
                        CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                    }
                }
            }
            //CLogger.WriteLog(ELogLevel.DEBUG, "ListProjects() found total: " + projects.Count + " ProviderProject(s)");
            return projects;
    		 
	    }

        public List<ProviderProject> ListBackupProjects()
        {
            //CLogger.WriteLog(ELogLevel.DEBUG, "ListBackupProjects() called...");
            List<ProviderProject> projects = new List<ProviderProject>();

            DirectoryInfo directory = new DirectoryInfo(DEFAULT_DATA_DIR_JOB_BACKUP);
            DirectoryInfo[] directories = directory.GetDirectories();
            for (int i = 0; i < directories.Length; i++)
            {
                if (existsProjectXML(directories[i]) &&
                    existsProjectSourceFolder(directories[i]) &&
                    existsProjectTargetFolder(directories[i])
                    )
                {
                    String folderPath = directories[i].FullName;
                    String prjXMLPath = folderPath + @"\" + def_ProjectFileName;

                    String prjXMLString = FileUtil.ReadStringFromFile(prjXMLPath);
                    try
                    {
                        ProviderProject prj = ProviderProject.fromXml(prjXMLString);
                        if (prj != null)
                        {
                            projects.Add(prj);
                        }
                    }
                    catch (Exception e)
                    {
                        CLogger.WriteLog(ELogLevel.ERROR, "failed to load ProviderProject from XML:\n" + prjXMLString + "\n\n");
                        CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                    }
                }
            }
            //CLogger.WriteLog(ELogLevel.DEBUG, "ListBackupProjects() found total: " + projects.Count + " ProviderProject(s)");
            return projects; 

        }

        public List<ProviderProjectFile> ListProviderProjectFiles(string ctProjectId)
        {
            //CLogger.WriteLog(ELogLevel.DEBUG, "getProviderProjectFiles('" + ctProjectId  + "') called...");
            List<ProviderProjectFile> files = new List<ProviderProjectFile>();

            String projectFolder = DEFAULT_DATA_DIR_JOB + ctProjectId;
            String projectFolder_bk = DEFAULT_DATA_DIR_JOB_BACKUP + ctProjectId;
            DirectoryInfo directory = null;
            if (System.IO.Directory.Exists(projectFolder))
            { 
                //CLogger.WriteLog(ELogLevel.DEBUG, "Search files under folder: " + projectFolder);
                directory = new DirectoryInfo(projectFolder);
            }
            else if (System.IO.Directory.Exists(projectFolder_bk))
            {
                //CLogger.WriteLog(ELogLevel.DEBUG, "Search files under folder: " + projectFolder_bk);
                directory = new DirectoryInfo(projectFolder_bk);
            }
            else
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Can't find project folder: " + projectFolder);
                CLogger.WriteLog(ELogLevel.ERROR, "Can't find backup project folder: " + projectFolder_bk);

                directory = null;
            }
            if (directory == null)
            {
                return files; 
            }
            else
            {
                List<String> filePaths = getProjectFilePaths(directory);

                foreach (String fielPath in filePaths)
                {
                    String prjFileXMLString = FileUtil.ReadStringFromFile(fielPath);
                    try
                    {
                        ProviderProjectFile prjFile = ProviderProjectFile.fromXml(prjFileXMLString);
                        if (prjFile != null)
                        {
                            files.Add(prjFile);
                        }
                    }
                    catch (Exception e)
                    {
                        CLogger.WriteLog(ELogLevel.ERROR, "failed to load ProviderProjectFile from XML:\n" + prjFileXMLString + "\n\n");
                        CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                    }

                }
            }
            return files;
        }

        public ProviderProject SearchAllProviderProjectByCTProjectId(String ctProjectId) 
        {
 
            List<ProviderProject> projects = this.ListProjects();

            foreach (ProviderProject prj in projects)
            {
                if (ctProjectId.Equals(prj.CTProjectId))
                {
                    return prj;
                }
            }
            List<ProviderProject> projects_bk = this.ListBackupProjects();
            foreach (ProviderProject prj in projects_bk)
            {
                if (ctProjectId.Equals(prj.CTProjectId))
                {
                    return prj;
                }
            }

            return null;
        }

        public ProviderProject SearchProviderProjectByCTProjectId(String ctProjectId)
        {

            List<ProviderProject> projects = this.ListProjects();

            foreach (ProviderProject prj in projects)
            {
                if (ctProjectId.Equals(prj.CTProjectId))
                {
                    return prj;
                }
            }
             

            return null;
        }

        public ProviderProject SearchBackupProviderProjectByCTProjectId(String ctProjectId)
        {
             
            List<ProviderProject> projects_bk = this.ListBackupProjects();
            foreach (ProviderProject prj in projects_bk)
            {
                if (ctProjectId.Equals(prj.CTProjectId))
                {
                    return prj;
                }
            }

            return null;
        }
         

        public ProviderProjectFile SearchProviderProjectFile(String ctProjectId, String assetTaskId)  
        {
            ProviderProject findProviderProject = this.SearchAllProviderProjectByCTProjectId(ctProjectId);
            if (findProviderProject == null)
                return null;
            else
            {
                List<ProviderProjectFile> files = this.ListProviderProjectFiles(ctProjectId);

                foreach (ProviderProjectFile file in files)
                {
                    if (assetTaskId.Equals(file.CTAssetTaskId))
                    {
                        return file;
                    }
                }

            }
		    return null;
    		 
	    }

        public ProviderProjectFile SearchProviderProjectFileByAssetTaskId(String assetTaskId)
        {
            List<ProviderProject> prjList = this.ListProjects();
            foreach (ProviderProject prj in prjList)
            {

                List<ProviderProjectFile> files = this.ListProviderProjectFiles(prj.CTProjectId);

                foreach (ProviderProjectFile file in files)
                    {
                        if (assetTaskId.Equals(file.CTAssetTaskId))
                        {
                            return file;
                        }
                    }
 
            }

            return null;

        }
         

        public String GetProviderProjectSourceFolder(String ctProjectId)
        {
            
                String projectFolder = DEFAULT_DATA_DIR_JOB + ctProjectId;
                if (!System.IO.Directory.Exists(projectFolder))
                    System.IO.Directory.CreateDirectory(projectFolder);

                String projectSourceFolder = projectFolder + @"\" + def_ProjectSourceFolderName;
                if (!System.IO.Directory.Exists(projectSourceFolder))
                    System.IO.Directory.CreateDirectory(projectSourceFolder);

                String projectTargetFolder = projectFolder + @"\" + def_ProjectTargetFolderName;
                if (!System.IO.Directory.Exists(projectTargetFolder))
                    System.IO.Directory.CreateDirectory(projectTargetFolder);


                return projectSourceFolder;
        }

        public String GetProviderProjectTargetFolder(String ctProjectId)
        {

            String projectFolder = DEFAULT_DATA_DIR_JOB + ctProjectId;
            if (!System.IO.Directory.Exists(projectFolder))
                System.IO.Directory.CreateDirectory(projectFolder);

            String projectSourceFolder = projectFolder + @"\" + def_ProjectSourceFolderName;
            if (!System.IO.Directory.Exists(projectSourceFolder))
                System.IO.Directory.CreateDirectory(projectSourceFolder);

            String projectTargetFolder = projectFolder + @"\" + def_ProjectTargetFolderName;
            if (!System.IO.Directory.Exists(projectTargetFolder))
                System.IO.Directory.CreateDirectory(projectTargetFolder);


            return projectTargetFolder;
        }

        public ProviderProject AddProviderProject(ProviderProject project)
        {
            ProviderProject result = null;
            try
            {
                String projectFolder = DEFAULT_DATA_DIR_JOB + project.CTProjectId;
                if (!System.IO.Directory.Exists(projectFolder))
                {
                    System.IO.Directory.CreateDirectory(projectFolder);
                }
                 
                String projectSourceFolder = projectFolder + @"\" + def_ProjectSourceFolderName;
                if (!System.IO.Directory.Exists(projectSourceFolder))
                        System.IO.Directory.CreateDirectory(projectSourceFolder);

                 String projectTargetFolder = projectFolder + @"\" + def_ProjectTargetFolderName;
                 if (!System.IO.Directory.Exists(projectTargetFolder))
                        System.IO.Directory.CreateDirectory(projectTargetFolder);

                String prjFile = projectFolder + @"\" + def_ProjectFileName;

                FileUtil.WriteStringToFile(prjFile, ProviderProject.toXml(project));
                result = project; 
                 

            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Failed to add new ProviderProject for project(ID:[" + project.CTProjectId + "] Name:[" + project .CTProjectName + "]). Error:" + e.Message);
                CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                result = null;
            }

            return result;
            
	    }


        public void UpdateProviderProject(ProviderProject project)
        {

            String projectFolder = DEFAULT_DATA_DIR_JOB + project.CTProjectId;
            String prjFile = projectFolder + @"\" + def_ProjectFileName;

            if (System.IO.File.Exists(prjFile))
                System.IO.File.Delete(prjFile);

            FileUtil.WriteStringToFile(prjFile, ProviderProject.toXml(project));
        }


        public void RemoveProviderProject(ProviderProject project)
        {
            String projectFolder = DEFAULT_DATA_DIR_JOB + project.CTProjectId;

            try
            {
                Directory.Delete(projectFolder, true);
            }
            catch (Exception e)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "failed to remove  ProviderProject folder:\n" + projectFolder + "\n\n");
                CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
            }
        }

        public ProviderProject BackupProviderProject(ProviderProject project)
        {
            String projectFolder = DEFAULT_DATA_DIR_JOB + project.CTProjectId;
            String projectFolder_bk = DEFAULT_DATA_DIR_JOB_BACKUP + project.CTProjectId;
            if (System.IO.Directory.Exists(projectFolder))
            {
                if (System.IO.Directory.Exists(projectFolder_bk))
                {
                    System.IO.Directory.Delete(projectFolder_bk, true);
                }

                System.IO.Directory.Move(projectFolder, projectFolder_bk);

                if (System.IO.Directory.Exists(projectFolder_bk))
                    return project;
                else
                    return null;
            }
            else
                return null;
        }

        public ProviderProject BackupNotFoundProviderProject(ProviderProject project)
        {
            String projectFolder = DEFAULT_DATA_DIR_JOB + project.CTProjectId;
            String projectFolder_bk = DEFAULT_DATA_DIR_JOB_BACKUP_NOTFOUND + project.CTProjectId;
            if (System.IO.Directory.Exists(projectFolder))
            {
                if (System.IO.Directory.Exists(projectFolder_bk))
                {
                    System.IO.Directory.Delete(projectFolder_bk, true);
                }

                System.IO.Directory.Move(projectFolder, projectFolder_bk);

                if (System.IO.Directory.Exists(projectFolder_bk))
                    return project;
                else
                    return null;
            }
            else
                return null;
        }

        public ProviderProject BackProviderProjectToLive(ProviderProject project)
        {

            String projectFolder = DEFAULT_DATA_DIR_JOB + project.CTProjectId;
            String projectFolder_bk = DEFAULT_DATA_DIR_JOB_BACKUP + project.CTProjectId;
            if (System.IO.Directory.Exists(projectFolder_bk))
            {
                if (System.IO.Directory.Exists(projectFolder))
                {
                    System.IO.Directory.Delete(projectFolder, true);
                }

                System.IO.Directory.Move(projectFolder_bk, projectFolder);

                if (System.IO.Directory.Exists(projectFolder))
                    return project;
                else
                    return null;
            }
            else
                return null;
        }

        public ProviderProjectFile SaveProviderProjectFile(ProviderProjectFile prjFile)
        {
            String projectFolder = DEFAULT_DATA_DIR_JOB + prjFile.CTProjectId;
            String prjFilePath = projectFolder + @"\" + prjFile.CTAssetTaskId + ".xml";
            if (Directory.Exists(projectFolder))
            {
                if (File.Exists(prjFilePath))
                    File.Delete(prjFilePath);

                String prjFileXML = ProviderProjectFile.toXml(prjFile);
                FileUtil.WriteStringToFile(prjFilePath, prjFileXML);

                return prjFile;

            }
            else
            {
                CLogger.WriteLog(ELogLevel.ERROR, "failed to save  ProviderProjectFile since can't find project folder:" + projectFolder );
                return null;
            }
        }

        public string getPureFileName(string fileName)
        {
            //    \ / : * ? " <> | 
            string passedFileName = fileName;
            passedFileName = passedFileName.Replace(@"\", "");
            passedFileName = passedFileName.Replace(@"/", "");
            passedFileName = passedFileName.Replace(@":", "_");
            passedFileName = passedFileName.Replace(@"*", "_");
            passedFileName = passedFileName.Replace(@"?", "_");
            passedFileName = passedFileName.Replace("\"", "_");
            passedFileName = passedFileName.Replace(@"<", "(");
            passedFileName = passedFileName.Replace(@">", ")");
            passedFileName = passedFileName.Replace(@"|", "_");

            return passedFileName;
        }

    }
}
