using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using com.claytablet.model;
using com.claytablet.provider;
using com.claytablet.model.enm;
using com.claytablet.model.Event;
using com.claytablet.model.Event.platform;
using com.claytablet.model.Event.provider;
using com.claytablet.service.Event;
using com.claytablet.util;
using com.claytablet.queue;
using com.claytablet.queue.service;
using com.claytablet.queue.service.sqs;
using com.claytablet.storage;
using com.claytablet.storage.service;
using com.claytablet.storage.service.s3;
using ClayTablet.CT.Utility;
using CTT.TMS.WordBee;

namespace ClayTablet.CT.Net.WordBee
{
    public class ReceiverJob
    {
        public void Process()
        {

            SourceAccountProvider sap;
            TargetAccountProvider tap;

            QueueSubscriberService queueSubscriberService; 
            QueuePublisherService queuePublisherService;
            StorageClientService storageClientService;
            MyProviderReceiver receiver;

            ConnectionContext context;
            ProviderSender sender;

            var sourceAccountConfig = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.SourceAccount"];
            String sourceAccountXML = null;
            if (sourceAccountConfig == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Source Account XML file [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.Provider.SourceAccount] in AppSetting to point a Account XML file and make sure system have read permission with the Account file.");
                return;
            }
            else
            {
                sourceAccountXML = PathUtil.getFullPath4RelatedPath(sourceAccountConfig.ToString());
            }

            String targetAccountXML = null;
            var targetAccountConfig = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.TargetAccount"];
            if (targetAccountConfig == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Target Account XML file [in Configuration.AppSettings].\n\nPlease configure [ClayTablet.Provider.TargetAccount] in AppSetting to point a Account XML file and make sure system have read permission with the Account file.");
                return;
            }
            else
            {
                targetAccountXML = PathUtil.getFullPath4RelatedPath(targetAccountConfig.ToString());
            }

            String contextFolder = null;
            if (System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ContextFolder"] == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the ConnectionContext Folder.\n\nPlease configure [ClayTablet.Provider.ContextFolder] in AppSetting to point a folder and make sure system have Full permission with the folder.");
                return;
            }
            else
            {
                contextFolder = PathUtil.getFullPath4RelatedPath(System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ContextFolder"].ToString());
            }

            String wbApiUrl = null;
            if (String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_ApiUrl"]))
            {

                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Wordbee API URL [in Configuration.AppSettings].\n\nPlease configure [CTT_WordBee_ApiUrl] in AppSetting.");
                return;
            }
            else
                wbApiUrl = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_ApiUrl"].ToString();


            String wbAccountId = null;
            if (String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_AccountId"]))
            {

                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Wordbee account ID [in Configuration.AppSettings].\n\nPlease configure [CTT_WordBee_AccountId] in AppSetting.");
                return;
            }
            else
                wbAccountId = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_AccountId"].ToString();

            String wbApiPassword = null;
            if (String.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Password"]))
            {

                CLogger.WriteLog(ELogLevel.ERROR, "Cannot locate the Wordbee API password [in Configuration.AppSettings].\n\nPlease configure [CTT_WordBee_Password] in AppSetting.");
                return;
            }
            else
                wbApiPassword = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Password"].ToString();
 
            //Load ConnectionContext, SDK will look for a file called "connectionContext.xml"
            //in the folder -- "CTT_ConnectionContext_Folder"  
            context = new ConnectionContext(false);
            context.setConnectionContextPath(contextFolder);
            context.load();

            String clientId = System.Configuration.ConfigurationManager.AppSettings["ClayTablet.Provider.ClientId"];
            //Initial a source Account,  SDK will look for the source account file which appsetting ("CTT_SourceAccount") pointed.
            string sourceKey = FileUtil.ReadStringFromFile(sourceAccountXML);
            sap = new SourceAccountProvider(clientId, sourceKey);

            //Initial a target Account,  SDK will look for the source account file which appsetting ("CTT_TargetAccount") pointed.         
            string targetKey = FileUtil.ReadStringFromFile(targetAccountXML);
            tap = new TargetAccountProvider(clientId, targetKey);

            Account sourceAccount = sap.get();
            Account targetAccount = sap.get();

            if (sourceAccount == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot load source account key from " + sourceAccountConfig);
            }

            if (targetAccount == null)
            {
                CLogger.WriteLog(ELogLevel.ERROR, "Cannot load target account key from " + targetAccountConfig);
            }

            if (sourceAccount == null || targetAccount == null)
            {
                return;
            }

            storageClientService = new StorageClientServiceS3();
            storageClientService.setPublicKey(sourceAccount.getPublicKey());
            storageClientService.setPrivateKey(sourceAccount.getPrivateKey());
            storageClientService.setStorageBucket(sourceAccount.getStorageBucket()); 

            queuePublisherService = new QueuePublisherServiceSQS();
            queuePublisherService.setPublicKey(sourceAccount.getPublicKey());
            queuePublisherService.setPrivateKey(sourceAccount.getPrivateKey());
            queuePublisherService.setEndpoint(sourceAccount.getQueueEndpoint());

            queueSubscriberService = new QueueSubscriberServiceSQS();
            queueSubscriberService.setPublicKey(sourceAccount.getPublicKey());
            queueSubscriberService.setPrivateKey(sourceAccount.getPrivateKey());
            queueSubscriberService.setEndpoint(sourceAccount.getQueueEndpoint());



            Client wsClient = new Client(wbApiUrl, wbAccountId, wbApiPassword); 
             

            //Initial a ProviderSender, receiver may need to send Event back to response.
            sender = new ProviderSender(context, sap, tap, queuePublisherService, storageClientService);
            receiver = new MyProviderReceiver(context, storageClientService, sender, wsClient);

            CLogger.WriteLog(ELogLevel.INFO, string.Format("Check SQS message for Item need translation. Start at {0}\t", System.DateTime.Now.ToString()));

            //Retrieve all avaiable messages
            int maxMessages = 9;
            int roundCount = 0;
            Boolean noMoreMsg = false;

            while ( !noMoreMsg && roundCount < 100 )
            {

                com.claytablet.queue.model.Message[] messages = queueSubscriberService.receiveMessages(maxMessages);
                CLogger.WriteLog(ELogLevel.INFO, "Found " + messages.Length + " messages.");

                if (messages.Length > 0)
                {
                    noMoreMsg = false;
                    roundCount++;
                    int msg_Count = 0;
                    foreach (com.claytablet.queue.model.Message message in messages)
                    {
                        msg_Count++;
                        CLogger.WriteLog(ELogLevel.INFO, msg_Count.ToString() + ")Message Body:\n" + message.getBody());
 
                        try
                        {
                            
                                //deserializing, from xml to AbsEvent,  
                                IEvent curEvent = AbsEvent.fromXml(message.getBody());

                                //call your ProducerReceiver to handle the event
                                HandleResult curHandleResult = receiver.ReceiveEvent(curEvent);

                                if (curHandleResult.CanDeleteMessage)
                                {
                                    //Event handled, delete from Queue
                                    queueSubscriberService.deleteMessage(message);
                                    CLogger.WriteLog(ELogLevel.INFO, "Message is processed and deleted from queue");
                            }

                            if (!curHandleResult.Success)
                            {
                                CLogger.WriteLog(ELogLevel.INFO, "Event handling error.\nError Message:" + curHandleResult.ErrorMessage);
                            } 
                        }
                        catch (Exception e)
                        {
                            CLogger.WriteLog(ELogLevel.INFO, "Message processing error: " + e.Message);
                            CLogger.WriteLog(ELogLevel.DEBUG, "Error details: ", e);
                        }
                    } 

                }
                else
                {
                    noMoreMsg = true;
                    roundCount = 0; 
                }
            }

            if (wsClient != null && wsClient.IsConnected)
                wsClient.Close();
 
          }

         
         
        }
    } 
