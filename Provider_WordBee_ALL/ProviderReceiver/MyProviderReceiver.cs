using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using com.claytablet.model;
using com.claytablet.provider;
using com.claytablet.model.enm;
using com.claytablet.model.Event;
using com.claytablet.model.Event.platform;
using com.claytablet.model.Event.provider;
using com.claytablet.service.Event; 
using com.claytablet.util;
using com.claytablet.queue;
using com.claytablet.queue.model;
using com.claytablet.queue.service;
using com.claytablet.queue.service.sqs;
using com.claytablet.storage;
using com.claytablet.storage.service;
using com.claytablet.storage.service.s3;
using ClayTablet.CT.Utility; 
using System.Web;
using com.claytablet.model.Event.metadata;
using com.claytablet.model.Event.metadata.custom.Wordbee;
using CTT.TMS.WordBee;
using CTT.TMS.Wordbee;

namespace ClayTablet.CT.Net.WordBee
{
    public class MyProviderReceiver 
    {
        // also connection config file
	    private  ConnectionContext context; 
	    private StorageClientService storageClientService;    	
	    private com.claytablet.service.Event.ProviderSender sender;
        private Client wsClient = null;

        public MyProviderReceiver(ConnectionContext context,
                                  StorageClientService storageClientService,
                                  ProviderSender sender, Client wsClient)
        {
            //Pass a ConnectionContext, you need it to retrieve the path where 
            // the translated files will be saved
            this.context = context;

            //pass a StorageClientService, you need it to download file for translation
            this.storageClientService = storageClientService; 
            
            //Pass a ProducerSender, so you can send Events and Files out if needed
            //Check Sample-Sender for how to send Events and Files.
            this.sender = sender;
            this.wsClient = wsClient;
		    
	    }


        public HandleResult ReceiveEvent(IEvent cttEvent)
        {

            if (cttEvent is ApprovedAssetTask)
                return receiveEvent((ApprovedAssetTask)cttEvent);
            else if (cttEvent is CanceledAssetTask)
                return receiveEvent((CanceledAssetTask)cttEvent);
            else if (cttEvent is CanceledSupportAsset)
                return receiveEvent((CanceledSupportAsset)cttEvent);
            else if (cttEvent is ProcessingError)
                return receiveEvent((ProcessingError)cttEvent);
            else if (cttEvent is RejectedAssetTask)
                return receiveEvent((RejectedAssetTask)cttEvent);
            else if (cttEvent is StartAssetTask)
                return receiveEvent((StartAssetTask)cttEvent);
            else if (cttEvent is StartNeedAssetTaskUpdate)
                return receiveEvent((StartNeedAssetTaskUpdate)cttEvent);
            else if (cttEvent is StartSupportAsset)
                return receiveEvent((StartSupportAsset)cttEvent);
            else if (cttEvent is StartUpdateTMAsset)
                return receiveEvent((StartUpdateTMAsset)cttEvent);
            else if (cttEvent is StartNeedTranslationCorrectionAssetTask)
                return receiveEvent((StartNeedTranslationCorrectionAssetTask)cttEvent);
            else
            {
                HandleResult handleResult = new HandleResult();
                handleResult.Success = false;
                handleResult.CanDeleteMessage = true;
                handleResult.ErrorMessage = "Can't identify the Event Type.";

                return handleResult;
            }


        }

        private HandleResult receiveEvent(StartNeedAssetTaskUpdate curEvent)
        {

            HandleResult handleResult = new HandleResult();
            //  call TMS to translate the AssetTask
           
                 
                // send AcceptAssetTask event out to notify CT 2.0 platform 
                AcceptNeedAssetTaskUpdate acceptEvent = new AcceptNeedAssetTaskUpdate();
                acceptEvent.setAssetTaskId(curEvent.getAssetTaskId());
                acceptEvent.setEventId(IdGenerator.createId());

                //call ProviderSender to sent event
                sender.sendEvent(acceptEvent);
                ProviderProject providerProject = JobHelper.Instance.SearchProviderProjectByCTProjectId(curEvent.getProjectId());
                ProviderProjectFile  providerProjectFile = null;
                if (providerProject == null)
                {
                    providerProject = JobHelper.Instance.SearchBackupProviderProjectByCTProjectId(curEvent.getProjectId());
                    if (providerProject == null)
                    {
                        handleResult.CanDeleteMessage = true;
                        handleResult.Success = true;
                        handleResult.ErrorMessage = "Can't find related  ProviderProject.  Will ignore this event.";
                        return handleResult;
                    }
                    else
                    {
                        providerProjectFile = JobHelper.Instance.SearchProviderProjectFile(curEvent.getProjectId(),curEvent.getAssetTaskId());
                        if (providerProjectFile != null)
                        {
                            // need to put project from backup back to live
                            providerProject = JobHelper.Instance.BackProviderProjectToLive(providerProject);
                            if (providerProject == null)
                            {
                                handleResult.CanDeleteMessage = true;
                                handleResult.Success = true;
                                handleResult.ErrorMessage = "Can't move related  ProviderProject from backup to live.  Will ignore this event.";
                                return handleResult;
                            }
                        }
                        else
                        {
                            handleResult.CanDeleteMessage = true;
                            handleResult.Success = true;
                            handleResult.ErrorMessage = "Can't fins related  ProviderProjectFile.  Will ignore this event.";
                            return handleResult;
                        }
                    }
                }
                else
                    providerProjectFile = JobHelper.Instance.SearchProviderProjectFile(curEvent.getProjectId(),curEvent.getAssetTaskId());


                if (providerProject != null && providerProjectFile != null)
                {

                    if (providerProjectFile.IsUploaded)
                    {
                        if (providerProjectFile.IsSentback)
                            providerProjectFile.ReceivedEventType = EventType.NeedTranslationUpdate;
                        else
                            providerProjectFile.ReceivedEventType = EventType.NewTranslation;

                        JobHelper.Instance.SaveProviderProjectFile(providerProjectFile);
                    }
                    else
                    {
                        // if not uploaded, then do nothing
                    }

                    //event handling is done, so Message can be removed from SQS queue.
                    handleResult.CanDeleteMessage = true;
                    handleResult.Success = true;
                    //return handleResult;  

                } 

            return handleResult;
        }

        private HandleResult receiveEvent(ApprovedAssetTask curEvent) 
        {
      
            HandleResult handleResult = new HandleResult();
            handleResult.Success = true;
            handleResult.CanDeleteMessage = true;

            CLogger.WriteLog(ELogLevel.INFO, "Get a ApprovedAssetTask  for AssetTask:" + curEvent.getAssetTaskId() );

            ProviderProjectFile providerProjectFile = JobHelper.Instance.SearchProviderProjectFileByAssetTaskId(curEvent.getAssetTaskId());
            ProviderProject    providerProject = null;
            if (providerProjectFile != null)
                providerProject = JobHelper.Instance.SearchProviderProjectByCTProjectId(providerProjectFile.CTProjectId);
             
            if (providerProject != null && providerProjectFile != null)
            {
                  CLogger.WriteLog(ELogLevel.INFO, "Found project:" + providerProject.CTProjectName );

                  providerProjectFile.IsApproved = true;
                  JobHelper.Instance.SaveProviderProjectFile(providerProjectFile);

                  //check if all ProviderProjectFile are approved, if so, then can backup the project.
                  bool isAllApproved = true;
                  List<ProviderProjectFile> allProviderProjectFiles = JobHelper.Instance.ListProviderProjectFiles(providerProjectFile.CTProjectId);
                  foreach (ProviderProjectFile eachFile in allProviderProjectFiles)
                  {
                      if (!eachFile.IsApproved)
                      {
                          isAllApproved = false;
                          break;
                      }

                  }

                  if (isAllApproved)
                      JobHelper.Instance.BackupProviderProject(providerProject);

                //event handling is done, so Message can be removed from SQS queue.
                handleResult.CanDeleteMessage = true;
                handleResult.Success = true;
                //return handleResult;  

            } 


            return handleResult;

	    }


        private HandleResult receiveEvent(CanceledAssetTask curEvent)
        {

            // ignore, since Wordbee doesn't support this.
            HandleResult handleResult = new HandleResult(); 
            handleResult.CanDeleteMessage = true;
            handleResult.Success = true;    				 
			        
            return handleResult;
	    }


        private HandleResult receiveEvent(CanceledSupportAsset curEvent)
        {

            HandleResult handleResult = new HandleResult();
            handleResult.Success = true;
            handleResult.CanDeleteMessage = true; 
            return handleResult;

	    }


        private HandleResult receiveEvent(ProcessingError curEvent)
        {

            HandleResult handleResult = new HandleResult();
            CLogger.WriteLog(ELogLevel.INFO, "Get a ProcessingError:" );
            CLogger.WriteLog(ELogLevel.INFO, "------------ Error Message --------------");
            CLogger.WriteLog(ELogLevel.INFO, curEvent.getErrorMessage() );
            CLogger.WriteLog(ELogLevel.INFO, "------------ Error Details --------------");
            CLogger.WriteLog(ELogLevel.INFO, curEvent.getErrorDetails() + "\n\n" );
 
            handleResult.Success = true;
            handleResult.CanDeleteMessage = true;
            return handleResult;

	    }


        private HandleResult receiveEvent(RejectedAssetTask curEvent)
        {

            HandleResult handleResult = new HandleResult();
            handleResult.Success = true;
            handleResult.CanDeleteMessage = true;

            ProviderProjectFile providerProjectFile = JobHelper.Instance.SearchProviderProjectFileByAssetTaskId(curEvent.getAssetTaskId());
            ProviderProject providerProject = null;
            if (providerProjectFile != null)
                providerProject = JobHelper.Instance.SearchProviderProjectByCTProjectId(providerProjectFile.CTProjectId);
           
            if (providerProject != null && providerProjectFile != null)
            {

                providerProjectFile.IsApproved = false;
                JobHelper.Instance.SaveProviderProjectFile(providerProjectFile); 

                //event handling is done, so Message can be removed from SQS queue.
                handleResult.CanDeleteMessage = true;
                handleResult.Success = true;
                //return handleResult;  

            } 

            return handleResult;

	    }


        private HandleResult receiveEvent(StartAssetTask curEvent)
        {
            
    		HandleResult handleResult = new HandleResult();      	 
		    try {

                        IJobMetadata jobMeta = curEvent.getJobMetadata();
                        if (jobMeta == null)
                        {
                            //can't save ProviderProject.
                            handleResult.CanDeleteMessage = true;
                            handleResult.ErrorMessage = "Can't get JobMetadata from event.\n----Event Description Field --(Start)---\n" + curEvent.getDescription() + "\n----Event Description Field --(End)---";

                            handleResult.Success = false;
                            return handleResult;
                        }

                        WordbeeMetadataGroup wordbeeMetadataGroup = null;
                        foreach (IMetadataGroup mGroup in jobMeta.getAdditionalMetadataGroups())
                        {
                            if (WordbeeMetadataGroup.isWordbeeMetadataGroup(mGroup))
                            {
                                wordbeeMetadataGroup = WordbeeMetadataGroup.convert2WordbeeMetadataGroup(mGroup);
                            }
                        }

                        if (wordbeeMetadataGroup == null)
                        { 


                        }
                     // Download the latest asset task revision file  
					    String downloadFilePath = storageClientService
                                .downloadLatestAssetTaskVersion(curEvent.getAssetTaskId(), JobHelper.Instance.GetProviderProjectSourceFolder(curEvent.getProjectId()));

                        String ctProjectId = curEvent.getProjectId();
                        ProviderProject findPproject = JobHelper.Instance.SearchProviderProjectByCTProjectId(ctProjectId);
                        if (findPproject == null)
                        {
                            findPproject = new ProviderProject();
                            findPproject.CTProjectId = ctProjectId;
                            findPproject.CTProjectName = curEvent.getProjectName();
                            findPproject.isWordbeeProjectCreated = false;
                            findPproject.ReceivedDate = DateTime.Now;

                            findPproject.ReceivedNewAssetTask();
                            findPproject.CTSourceLanguage = curEvent.getSourceLanguageCode();
                            findPproject.addCTTargetLanguage(curEvent.getTargetLanguageCode());

                            findPproject.CmsJobName = jobMeta.getJobName();
                            // get metadata part to fill
                            findPproject.CTProjectDecription = jobMeta.getJobName();
                            findPproject.CTProjectDeadline = jobMeta.getDueDate();
                            findPproject.CTTotalAssetTasks = jobMeta.getAssetTaskCount();
                            findPproject.CTProducerId = jobMeta.getProducerId();
                            findPproject.WordbeeProjectReference = jobMeta.getJobName();
                            findPproject.WordbeeProjectDeadline = null;
                            if (!String.IsNullOrEmpty(jobMeta.getDueDate()))
                            {
                                try
                                {
                                    DateTime dt = Convert.ToDateTime(jobMeta.getDueDate());
                                    DateTime dt_utc = dt.ToUniversalTime();
                                    findPproject.WordbeeProjectDeadline = UtcIsoTimeUtil.toUTCISO8601Format(dt_utc);
                                }
                                catch (Exception e)
                                {
                                    CLogger.WriteLog(ELogLevel.WARN, "Cannot convert job due date: " + jobMeta.getDueDate());
                                    CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                                }

                            }

                            if (wordbeeMetadataGroup != null)
                            {
                                findPproject.WordbeeInstructions = wordbeeMetadataGroup.getInstructions();
                                findPproject.WordbeeInternalComments = wordbeeMetadataGroup.getInternalComments();
                                findPproject.WordbeeProjectType = int.Parse(wordbeeMetadataGroup.getProjectType());
                                findPproject.WordbeeTemplateProjectName = wordbeeMetadataGroup.getTemplateProjectID(); 

                            }
                            else
                            { 
                                 // load from config
                                findPproject.WordbeeInstructions = "";
                                findPproject.WordbeeInternalComments = "";
                                findPproject.WordbeeProjectType = 2;
                                var prjTypeConfig = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Project_Projecttype"];
                                if (prjTypeConfig != null)
                                {
                                    try
                                    {
                                        findPproject.WordbeeProjectType = int.Parse(prjTypeConfig.ToString());
                                    }
                                    catch (Exception e)
                                    {
                                        CLogger.WriteLog(ELogLevel.WARN, "Invalid CTT_WordBee_Project_Projecttype configuration: " + prjTypeConfig);
                                        CLogger.WriteLog(ELogLevel.DEBUG, "Error detail:", e);
                                    }

                                }

                                if (System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_TemplateProjectName"] != null)
                                {
                                    try
                                    {
                                        findPproject.WordbeeTemplateProjectName = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_TemplateProjectName"].ToString();
                                    }
                                    catch (Exception) { }

                                }  
                                 

                            }

                            
                            /*
                            int templateProjectID = 6;
                            templateProjectID = wsClient.FindProjectId(wordbeeMetadataGroup.getTemplateProjectID());
                            findPproject.WordbeeTemplateProjectId = Convert.ToString(templateProjectID); //  wordbeeMetadataGroup.getTemplateProjectID();
                            */

                            findPproject = JobHelper.Instance.AddProviderProject(findPproject);
                            if (findPproject == null)
                            {
                                //can't save ProviderProject.
                                handleResult.CanDeleteMessage = false;
                                handleResult.ErrorMessage = "Failed to save new ProviderProject";
                                handleResult.Success = false;
                                return handleResult;
                            }
                             
                        }
                        else
                        { 
                            findPproject.ReceivedNewAssetTask(); 
                            findPproject.addCTTargetLanguage(curEvent.getTargetLanguageCode()); 
                        }
                        
                        //   Need metadata part to get total files, job description, producer ID etc
                        //to do here 

                        JobHelper.Instance.UpdateProviderProject(findPproject); 

                        String originalFileName = null;

                        String wbFileVersion = "";
                        String wbParserDomain = "";
                        String wbParserConfig = "";
                        String wbWorkflow = "";
                        bool wbNeedAssignSupplier = false;
                        String wbSupplierCompanyID = "";
                        String wbSupplierPersonID = "";
                        String wbPickupType = "WorkflowCompleted";
                        String wbCustomLabelDBCode = "";

                        if (wordbeeMetadataGroup != null)
                        {
                            

                            wbFileVersion = wordbeeMetadataGroup.getFileVersion();
                            wbParserDomain = wordbeeMetadataGroup.getParserDomain();
                            wbParserConfig = wordbeeMetadataGroup.getParserConfig();
                            wbWorkflow = wordbeeMetadataGroup.getWorkflow();
                            wbNeedAssignSupplier = wordbeeMetadataGroup.getNeedAssignSupplier();
                            wbSupplierCompanyID = wordbeeMetadataGroup.getSupplierCompanyID();
                            wbSupplierPersonID = wordbeeMetadataGroup.getSupplierPersonID();
                            wbPickupType = wordbeeMetadataGroup.getPickupType();
                            wbCustomLabelDBCode = wordbeeMetadataGroup.getCustomLabelDBCode();

                        }
                        else
                        {
                            // load from config 
                            originalFileName = Path.GetFileName(downloadFilePath); 
                            if (System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Project_ParserDomain"] != null)
                            {
                                try
                                {
                                    wbParserDomain = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Project_ParserDomain"].ToString();
                                }
                                catch (Exception) { }

                            }

                            if (System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Project_ParserConfig"] != null)
                            {
                                try
                                {
                                    wbParserConfig = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Project_ParserConfig"].ToString();
                                }
                                catch (Exception) { }

                            }

                            if (System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Project_Workflow"] != null)
                            {
                                try
                                {
                                    wbWorkflow = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_Project_Workflow"].ToString();
                                }
                                catch (Exception) { }

                            }

                            if (System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_NeedAssignSupplier"] != null)
                            {
                                try
                                {
                                    wbNeedAssignSupplier = bool.Parse(System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_NeedAssignSupplier"].ToString());
                                }
                                catch (Exception) { }

                            }

                            if (System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_SupplierCompanyId"] != null)
                            {
                                try
                                {
                                    wbSupplierCompanyID = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_SupplierCompanyId"].ToString();
                                }
                                catch (Exception) { }

                            }

                            if (System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_SupplierPersonId"] != null)
                            {
                                try
                                {
                                    wbSupplierPersonID = System.Configuration.ConfigurationManager.AppSettings["CTT_WordBee_SupplierPersonId"].ToString();
                                }
                                catch (Exception) { }

                            }


                        }


                        // Save info to ProviderProjectFile  
                        ProviderProjectFile providerProjectFile = new ProviderProjectFile();

                        providerProjectFile.CTAssetTaskId = curEvent.getAssetTaskId();
                        providerProjectFile.CTAssetId = curEvent.getAssetId();
                        providerProjectFile.CTProjectId = curEvent.getProjectId();
                        providerProjectFile.CmsJobName = jobMeta.getJobName();

                        providerProjectFile.IsUploaded = false;
                        providerProjectFile.IsApproved = false;
                        providerProjectFile.IsSentback = false;
                        providerProjectFile.IsTransStarted = false;
                        providerProjectFile.IsSupplierAssigned = false;

                        providerProjectFile.OriginalFilename = originalFileName;
                        providerProjectFile.ReceivedDate = DateTime.Now;
                        providerProjectFile.ReceivedEventType = EventType.NewTranslation;
                        providerProjectFile.WordbeeFileName = originalFileName; // curEvent.getAssetId().Substring(0, 8) + "_" + originalFileName;
                        providerProjectFile.WordbeeFileVersion = wbFileVersion;
                        providerProjectFile.SourceCttLanguageCode = curEvent.getSourceLanguageCode();
                        providerProjectFile.TargetCttLanguageCode = curEvent.getTargetLanguageCode();
                        providerProjectFile.FileExt = curEvent.getFileExt(); 
                        providerProjectFile.CTFileName = Path.GetFileName(downloadFilePath);
                        providerProjectFile.SourceFilePath = downloadFilePath; 
                        
                        providerProjectFile.WordbeeParserDomain = wbParserDomain;
                        providerProjectFile.WordbeeParserConfig = wbParserConfig;
                        providerProjectFile.WordbeeWorkflow = wbWorkflow;

                        providerProjectFile.WordbeeNeedAssignSupplier = wbNeedAssignSupplier;
                        providerProjectFile.WordbeeSupplierCompanyID = wbSupplierCompanyID;
                        providerProjectFile.WordbeeSupplierPersonID = wbSupplierPersonID;

                        providerProjectFile.WordbeePickupType = wbPickupType;
                        providerProjectFile.WordbeeCustomLabelDBCode = wbCustomLabelDBCode;


                        providerProjectFile = JobHelper.Instance.SaveProviderProjectFile(providerProjectFile);
                        if (providerProjectFile == null)
                        {
                            //can't save ProviderProject.
                            handleResult.CanDeleteMessage = false;
                            handleResult.ErrorMessage = "Failed to save new ProviderProject.";
                            handleResult.Success = false;
                            return handleResult;
                        }
                        else
                        {
                            //event handling is done, so Message can be removed from SQS queue.
                            handleResult.CanDeleteMessage = true;
                            handleResult.Success = true;
                            //return handleResult;  

                            try
                            {
                                storageClientService.deleteAssetTaskVersions(curEvent.getAssetTaskId());
                            }
                            catch (Exception e)
                            {
                                CLogger.WriteLog(ELogLevel.DEBUG, "Failed to delete asset task in S3: " + curEvent.getAssetTaskId(), e);
                            }
                }


            } catch (Exception e) {
                  
			        handleResult.CanDeleteMessage = false;
                    handleResult.Success = false;
                    handleResult.ErrorMessage = "[StartAssetTask] Error.";
                    handleResult.Message = e.Message;
                    CLogger.WriteLog(ELogLevel.DEBUG, "[StartAssetTask] Error (will retry).", e);
            }

            return handleResult;
	    } 

        private HandleResult receiveEvent(StartNeedTranslationCorrectionAssetTask curEvent)
        {

            HandleResult handleResult = new HandleResult();
            //  call TMS to translate the StartNeedTranslationCorrectionAssetTask
            try
            {
                 

                        // Download the latest asset task revision file  
                        String downloadFilePath = storageClientService
                                .downloadLatestAssetTaskVersion(curEvent.getAssetTaskId(), JobHelper.Instance.GetProviderProjectSourceFolder(curEvent.getProjectId()));
 
                        
                        // send AcceptAssetTask event out to notify CT 2.0 platform 
                        AcceptAssetTask acceptEvent = new AcceptAssetTask();
                        acceptEvent.setAssetTaskId(curEvent.getAssetTaskId());
                        acceptEvent.setAssetTaskNativeId("");
                        acceptEvent.setEventId(IdGenerator.createId());

                        //call ProviderSender to sent event
                        sender.sendEvent(acceptEvent);

                        //event handling is done, so Message can be removed from SQS queue.
                        handleResult.CanDeleteMessage = true;
                        handleResult.Success = true;
                        //return handleResult; 

                        try
                        {
                            storageClientService.deleteAssetTaskVersions(curEvent.getAssetTaskId());
                        }
                        catch (Exception e)
                        {
                            CLogger.WriteLog(ELogLevel.DEBUG, "Failed to delete asset task in S3: " + curEvent.getAssetTaskId(), e);
                        }

            }
            catch (Exception e)
            {
                handleResult.CanDeleteMessage = false;
                handleResult.Success = false;
                handleResult.ErrorMessage = "[StartNeedTranslationCorrectionAssetTask] Error.";
                handleResult.Message = e.Message;
                CLogger.WriteLog(ELogLevel.DEBUG, "[StartNeedTranslationCorrectionAssetTask] Error (will retry).", e);
            }

            return handleResult;
        }

        private HandleResult receiveEvent(StartUpdateTMAsset curEvent)
        {

            HandleResult handleResult = new HandleResult();
            //  call TMS to translate the AssetTask
            try
            {


               

                        CLogger.WriteLog(ELogLevel.INFO, "Start to downloaded UpdateTM file");
                        String downloadFilePath = storageClientService
                                .downloadUpdateTMAsset(curEvent.getUpdateTMAssetId(), curEvent.getFileExt(), context.getSourceDirectory());
                 
                        //event handling is done, so Message can be removed from SQS queue.
                        handleResult.CanDeleteMessage = true;
                        handleResult.Success = true;
                        //return handleResult; 

                        try
                        {
                            storageClientService.deleteUpdateTMAsset(curEvent.getUpdateTMAssetId(), curEvent.getFileExt());
                        }
                        catch (Exception e)
                        {
                            CLogger.WriteLog(ELogLevel.DEBUG, "Failed to delete UpdateTM asset in S3: " + curEvent.getUpdateTMAssetId(), e);
                        }

            }
            catch (Exception e)
            {

                handleResult.CanDeleteMessage = false;
                handleResult.Success = false;
                handleResult.ErrorMessage = "[StartUpdateTMAsset] Error.";
                handleResult.Message = e.Message;
                CLogger.WriteLog(ELogLevel.DEBUG, "[StartUpdateTMAsset] Error (will retry).", e);
            }

            return handleResult;
        }

        private HandleResult receiveEvent(StartSupportAsset curEvent)
        {

            HandleResult handleResult = new HandleResult();
            handleResult.Success = true;
            handleResult.CanDeleteMessage = true;  
            return handleResult;

	    }

        
         
    }
}
