﻿using System;
using System.Collections.Generic;
using System.Text;
using Xstream.Core;

namespace ClayTablet.CT3.Net.GlobalSight.SmartTec
{
    public class NotifyEmail
    {
        string guid = com.claytablet.util.IdGenerator.createId();
        string emailBody;
        string emailAddress;

        public string EmailBody
        {
            get { return this.emailBody; }
            set { this.emailBody = value; }
        }

        public string EmailAddress
        {
            get { return this.emailAddress; }
            set { this.emailAddress = value; }
        }

        public string Guid
        {
            get { return this.guid; }
        }

        private static string getBaseFolder()
        {
            String DEFAULT_DATA_DIR = System.Configuration.ConfigurationManager.AppSettings["CTT2_ConnectionContext_Folder"];
            if (DEFAULT_DATA_DIR.EndsWith("\\"))
                DEFAULT_DATA_DIR = DEFAULT_DATA_DIR + "NotifyEmails\\";
            else
                DEFAULT_DATA_DIR = DEFAULT_DATA_DIR + "\\NotifyEmails\\";

            if (!System.IO.Directory.Exists(DEFAULT_DATA_DIR))
                System.IO.Directory.CreateDirectory(DEFAULT_DATA_DIR);

            return DEFAULT_DATA_DIR;
        }


        public void Save()
        {

            String fileName = NotifyEmail.getBaseFolder() + guid + ".xml";
            String fileXML = NotifyEmail.toXml(this);

            com.claytablet.util.FileUtil.WriteStringToFile(fileName, fileXML);

        }

        public void Delete()
        {

            String fileName = NotifyEmail.getBaseFolder() + guid + ".xml";

            if (System.IO.File.Exists(fileName))
                System.IO.File.Delete(fileName);

        }


        public static List<NotifyEmail> ListAllNotifyEmails()
        {
            List<NotifyEmail> notifyEmailList = new List<NotifyEmail>();
            String DEFAULT_DATA_DIR = NotifyEmail.getBaseFolder();
            String[] exts = { "xml" };
            List<String> files = com.claytablet.util.FileUtil.ListFiles(DEFAULT_DATA_DIR, exts);
            foreach (String file in files)
            {
                try
                {
                    NotifyEmail newNotifyEmail = NotifyEmail.fromXml(com.claytablet.util.FileUtil.ReadStringFromFile(file));
                    notifyEmailList.Add(newNotifyEmail);
                }
                catch (Exception e)
                {
                    //
                }
            }

            return notifyEmailList;
        }


        public static NotifyEmail fromXml(String xml)
        {

            // deserialize the account
            return (NotifyEmail)getXStream().FromXml(xml);
        }

        public static String toXml(NotifyEmail notifyEmail)
        {

            // serilize the object to xml and return it
            return getXStream().ToXml(notifyEmail);
        }

        private static XStream getXStream()
        {
            XStream xstream = new XStream();
            return xstream;
        }


    }
}
