using System;
using System.Collections.Generic;
using System.Text; 
using Xstream.Core;

namespace ClayTablet.CT.Net.WordBee
{
    public class ProviderProjectFile
    {

        //This is a sample mapping file, you can modify to provide your own mapping (depending on the TMS)
         
        private String ctProjectId;
	    private String ctAssetId; 
	    private String ctAssetTaskId; 
	    private String fileExt;
        private EventType receivedEventType; 
        private String ctFilename = null;
        private String sourceCttLanguageCode = null;
        private String targetCttLanguageCode = null;

        private String orgFilename = null;  
        private String sourceFilePath = null;
        private String targetFilePath = null;  
	   
        private String wbfilename = null;
        private String wbfilename_full = null;
        private String wbDocumentId = null;
        private String wbTaskId = null;

        private String wbParserDomain = null;
        private String wbParserConfig = null;
        private String wbWorkflow = "ProjectDefault";
        private String wbFileVersion = null;
        private bool   wbNeedAssignSupplier = false;
        private String wbSupplierCompanyID = null;
        private String wbSupplierPersonID = null; 
        private String wbPickupType = "WorkflowCompleted";  //�WorkflowCompleted� or �LabelFlagged�
        private String wbCustomLabelDBCode = null; 

        private DateTime receivedDate;
        private DateTime uploadedDate;
        private DateTime startTransDate;
        private DateTime assignSupplierDate;
        private DateTime sendbackDate;

        private bool isUploaded = false; 
        private bool isTransStarted = false;
        private bool isSupplierAssigned = false;

        private bool isTransProcessDone = false;
        private bool isTransFileBuild = false;
        private bool isTargetFileDownloaded = false;

        private bool isSentback = false;
        private bool isApproved = false;

        private String cmsJobName = null; 

        public String CmsJobName
        {
            get { return this.cmsJobName; }
            set { this.cmsJobName = value; }
        }

        private String lastError = "";

        public String LastError
        {
            get { return this.lastError; }
            set { this.lastError = value; }
        } 

        public bool IsTargetFileDownloaded
        {
            get { return this.isTargetFileDownloaded; }
            set { this.isTargetFileDownloaded = value; }
        }

        public bool IsTransProcessDone
        {
            get { return this.isTransProcessDone; }
            set { this.isTransProcessDone = value; }
        }

        public bool IsTransFileBuild
        {
            get { return this.isTransFileBuild; }
            set { this.isTransFileBuild = value; }
        }

        public String WordbeeCustomLabelDBCode
        {
            get { return this.wbCustomLabelDBCode; }
            set { this.wbCustomLabelDBCode = value; }
        }

        public String WordbeePickupType
        {
            get { return this.wbPickupType; }
            set { this.wbPickupType = value; }
        }

        public String WordbeeSupplierPersonID
        {
            get { return this.wbSupplierPersonID; }
            set { this.wbSupplierPersonID = value; }
        }

        public String WordbeeSupplierCompanyID
        {
            get { return this.wbSupplierCompanyID; }
            set { this.wbSupplierCompanyID = value; }
        }

        public bool WordbeeNeedAssignSupplier
        {
            get { return this.wbNeedAssignSupplier; }
            set { this.wbNeedAssignSupplier = value; }
        }

        public String WordbeeFileVersion
        {
            get { return this.wbFileVersion; }
            set { this.wbFileVersion = value; }
        }

        public String WordbeeWorkflow
        {
            get { return this.wbWorkflow; }
            set { this.wbWorkflow = value; }
        }

        //file format, xml, ms words etc
        public String WordbeeParserConfig
        {
            get { return this.wbParserConfig; }
            set { this.wbParserConfig = value; }
        }

        //content type, software, parts
        public String WordbeeParserDomain
        {
            get { return this.wbParserDomain; }
            set { this.wbParserDomain = value; }
        }

        public bool IsSupplierAssigned
        {
            get { return this.isSupplierAssigned; }
            set { this.isSupplierAssigned = value; }
        }

        public bool IsTransStarted
        {
            get { return this.isTransStarted; }
            set { this.isTransStarted = value; }
        }
         

        public DateTime AssignSupplierDate
        {
            get { return this.assignSupplierDate; }
            set { this.assignSupplierDate = value; }
        }
 

        public DateTime ReceivedDate
        {
            get { return this.receivedDate; }
            set { this.receivedDate = value; }
        }

        public DateTime UploadedDate
        {
            get { return this.uploadedDate; }
            set { this.uploadedDate = value; }
        }

        public DateTime StartTransDate
        {
            get { return this.startTransDate; }
            set { this.startTransDate = value; }
        }  

        public DateTime SendbackDate
        {
            get { return this.sendbackDate; }
            set { this.sendbackDate = value; }
        } 

        public String WordbeeDocumentId
        {
            get { return this.wbDocumentId; }
            set { this.wbDocumentId = value; }
        }

        public String WordbeeTaskId
        {
            get { return this.wbTaskId; }
            set { this.wbTaskId = value; }
        }
        

        public String WordbeeFileName
        {
            get { return this.wbfilename; }
            set { this.wbfilename = value; }
        }

        public String WordbeeFullFileName
        {
            get
            {
                if (this.wbfilename_full != null)                  
                   return this.wbfilename_full; 
                else
                    return this.wbfilename;
            }
            set { this.wbfilename_full = value; }
        } 

        public String OriginalFilename
        {
            get { return this.orgFilename; }
            set { this.orgFilename = value; }
        } 
 
	    public EventType ReceivedEventType
        {
            get { return this.receivedEventType; }
            set { this.receivedEventType = value; }
        }

        public String CTAssetTaskId
        {
            get { return this.ctAssetTaskId; }
            set { this.ctAssetTaskId = value; }
        }

        public String CTAssetId
        {
            get { return this.ctAssetId; }
            set { this.ctAssetId = value; }
        }

        public String CTProjectId
        {
            get { return this.ctProjectId; }
            set { this.ctProjectId = value; }
        }

        public String FileExt
        {
            get { return this.fileExt; }
            set { this.fileExt = value; }
        }

        public String CTFileName
        {
            get { return this.ctFilename; }
            set { this.ctFilename = value; }
        } 

        public String SourceFilePath
        {
            get { return this.sourceFilePath; }
            set { this.sourceFilePath = value; }
        }

        public String TargetFilePath
        {
            get { return this.targetFilePath; }
            set { this.targetFilePath = value; }
        }

        public String SourceCttLanguageCode
        {
            get { return this.sourceCttLanguageCode; }
            set { this.sourceCttLanguageCode = value; }
        }

        public String TargetCttLanguageCode
        {
            get { return this.targetCttLanguageCode; }
            set { this.targetCttLanguageCode = value; }
        } 

        public bool IsUploaded
        {
            get { return this.isUploaded; }
            set { this.isUploaded = value; }
        }

        public bool IsSentback
        {
            get { return this.isSentback; }
            set { this.isSentback = value; }
        }

        public bool IsApproved
        {
            get { return this.isApproved; }
            set { this.isApproved = value; }
        } 

        public static ProviderProjectFile fromXml(String xml)
        { 
		    // deserialize the account
            return (ProviderProjectFile)getXStream().FromXml(xml);
	    }

        public static String toXml(ProviderProjectFile providerProjectFile)
        {
     
		    // serilize the object to xml and return it
            return getXStream().ToXml(providerProjectFile);
	    }
    	
	    private static XStream getXStream() {

            XStream xstream = new XStream();
		    return xstream;
	    } 
    }
     
}
