using System;
using System.Collections.Generic;
using System.Text; 
using Xstream.Core;

namespace ClayTablet.CT.Net.WordBee
{
    public class ProviderProject
    { 
	    private String ctProjectId;
        private String ctProjectName;
        private String ctProjectDecription;
        private int ctTotalAssetTasks = 0;
        private int ctReceivedAssetTasks = 0;
        private String ctProducerId;
        private String ctProjectDeadline = null;
        private String ctSourceLanguage = null;
        private List<String> ctTargetLanguages = new List<string>(); 
        
        private String wbProjectReference = "";
        private String wbTemplateProjectId = null;
        private String wbTemplateProjectName = null;
        private String wbProjectDeadline = "";
        private int wbProjectType = 2; // default Codyt project
        private String wbInternalComments = "";
        private String wbInstructions  = "";
        private int wbProjectId = 0; 
        private bool isWBProjectCreated = false;
        private String lastError = "";
        
        private DateTime receivedDate;
        private DateTime wbPrjCreateDate;
        private String wbAutoGenProjectName = null;

        private String cmsJobName = null;

        private int checkProjectNotExistNum = 0;

        public int CheckedProjectNotExistNum
        {
            get { return this.checkProjectNotExistNum; }
            set { this.checkProjectNotExistNum = value; }
        }

        public String CmsJobName
        {
            get { return this.cmsJobName; }
            set { this.cmsJobName = value; }
        }

        public String LastError
        {
            get { return this.lastError; }
            set { this.lastError = value; }
        }

        public String WordbeeAutoGenProjectName
        {
            get { return this.wbAutoGenProjectName; }
            set { this.wbAutoGenProjectName = value; }
        }

        public String WordbeeTemplateProjectId
        {
            get { return this.wbTemplateProjectId; }
            set { this.wbTemplateProjectId = value; }
        }

        public String WordbeeTemplateProjectName
        {
            get { return this.wbTemplateProjectName; }
            set { this.wbTemplateProjectName = value; }
        }
          
        public String WordbeeProjectDeadline
        {
            get { return this.wbProjectDeadline; }
            set { this.wbProjectDeadline = value; }
        }

        public int WordbeeProjectType
        {
            get { return this.wbProjectType; }
            set { this.wbProjectType = value; }
        }


        public String WordbeeInternalComments
        {
            get { return this.wbInternalComments; }
            set { this.wbInternalComments = value; }
        }

        public String WordbeeInstructions
        {
            get { return this.wbInstructions; }
            set { this.wbInstructions = value; }
        }

        public String CTSourceLanguage
        {
            get { return this.ctSourceLanguage; }
            set { this.ctSourceLanguage = value; }
        }

        public List<String> CTTargetLanguages
        {
            get { return this.ctTargetLanguages; }
            set { this.ctTargetLanguages = value; }
        }

        public void addCTTargetLanguage(String ctTargetLangauge)
        {
            bool isExisting = false;
            foreach (String tar in ctTargetLanguages)
            {
                if (tar.Equals(ctTargetLangauge))
                {
                    isExisting = true;
                    break;
                }
            }

            if (!isExisting)
            {
                ctTargetLanguages.Add(ctTargetLangauge);
            }
        }

        public void ReceivedNewAssetTask()
        {
            ctReceivedAssetTasks++;
        }
        public String CTProjectDeadline
        {
            get { return this.ctProjectDeadline; }
            set { this.ctProjectDeadline = value; }
        }

        public DateTime ReceivedDate
        {
            get { return this.receivedDate; }
            set { this.receivedDate = value; }
        }

        public DateTime WordbeeProjectCreationDate
        {
            get { return this.wbPrjCreateDate; }
            set { this.wbPrjCreateDate = value; }
        }

        public String CTProducerId
        {
            get { return this.ctProducerId; }
            set { this.ctProducerId = value; }
        }

        public int CTTotalAssetTasks
        {
            get { return this.ctTotalAssetTasks; }
            set { this.ctTotalAssetTasks = value; }
        }

        public int CTReceivedAssetTasks
        {
            get { return this.ctReceivedAssetTasks; }
            set { this.ctReceivedAssetTasks = value; }
        }

        public int WordbeeProjectId
        {
            get { return this.wbProjectId; }
            set { this.wbProjectId = value; }
        }

        public String WordbeeProjectReference
        {
            get { return this.wbProjectReference; }
            set { this.wbProjectReference = value; }
        } 

        public String CTProjectId
        {
            get { return this.ctProjectId; }
            set { this.ctProjectId = value; }
        }

        public String CTProjectName
        {
            get { return this.ctProjectName; }
            set { this.ctProjectName = value; }
        }

        public String CTProjectDecription
        {
            get { return this.ctProjectDecription; }
            set { this.ctProjectDecription = value; }
        }

        public bool isWordbeeProjectCreated
        {
            get { return this.isWBProjectCreated; }
            set { this.isWBProjectCreated = value; }
        }

        public static ProviderProject fromXml(String xml)
        { 
		    // deserialize the account
            return (ProviderProject)getXStream().FromXml(xml);
	    }

        public static String toXml(ProviderProject providerProject)
        {
     
		    // serilize the object to xml and return it
            return getXStream().ToXml(providerProject);
	    }
    	
	    private static XStream getXStream() {

		    XStream xstream = new XStream();  

		    return xstream;
	    } 
    }

    public enum EventType
    {
        NewTranslation = 0x01,
        UpdateTM = 0x02,
        NeedTranslationUpdate = 0x04,
        TranslationFix = 0x08,
        CancelTranslation = 0x16
    }
}
